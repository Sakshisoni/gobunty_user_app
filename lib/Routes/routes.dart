import 'package:gobunty/Auth/login_navigator.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/ListItems/support_page.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/account_page.dart';
import 'package:gobunty/Chat/UI/chat_page.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/home.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/search.dart';
import 'package:gobunty/HomeOrderAccount/home_order_account.dart';
import 'package:gobunty/HomeOrderAccount/Order/UI/order_page.dart';
import 'package:gobunty/Maps/UI/location_page.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/ListItems/about_us_page.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/ListItems/saved_addresses_page.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/ListItems/tnc_page.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/order_placed_map.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:gobunty/Pages/items.dart';
import 'package:gobunty/Pages/view_cart.dart';
import 'package:gobunty/Pages/order_placed.dart';
import 'package:gobunty/Pages/payment_method.dart';

class PageRoutes {
  static const String locationPage = 'location_page';
  static const String homeOrderAccountPage = 'home_order_account';
  static const String homePage = 'home_page';
  static const String accountPage = 'account_page';
  static const String orderPage = 'order_page';
  static const String items = 'items';
  static const String tncPage = 'tnc_page';
  static const String aboutUsPage = 'about_us_page';
  static const String savedAddressesPage = 'saved_addresses_page';
  static const String supportPage = 'support_page';
  static const String loginNavigator = 'login_navigator';
  static const String orderMapPage = 'order_map_page';
  static const String chatPage = 'chat_page';
  static const String viewCart = 'view_cart';
  static const String orderPlaced = 'order_placed';
  static const String paymentMethod = 'payment_method';
  static const String searchPage = 'search_page';

  Map<String, WidgetBuilder> routes() {
    return {
      locationPage: (context) => LocationPage(),
      homeOrderAccountPage: (context) => HomeOrderAccount(),
      homePage: (context) => HomePage(),
      orderPage: (context) => OrderPage(),
      accountPage: (context) => AccountPage(),
      tncPage: (context) => TncPage(),
      aboutUsPage: (context) => AboutUsPage(),
      savedAddressesPage: (context) => SavedAddressesPage(),
      supportPage: (context) => SupportPage(),
      loginNavigator: (context) => LoginNavigator(),
      orderMapPage: (context) => OrderMapPage(),
      chatPage: (context) => ChatPage(),
      items: (context) => ItemsPage(),
      viewCart: (context) => ViewCart(add:"Home"),
      paymentMethod: (context) => PaymentPage(),
      orderPlaced: (context) => OrderPlaced(),
      searchPage: (context) => SearchPage(),
    };
  }
}
