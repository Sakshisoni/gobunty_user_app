import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:gobunty/Components/FoodColors.dart';
import 'package:gobunty/Components/FoodConstant.dart';
import 'package:gobunty/Components/FoodWidget.dart';
import 'package:gobunty/Components/entry_field.dart';
import 'package:gobunty/Components/promo.dart';
import 'package:gobunty/Maps/UI/change.dart';
import 'package:gobunty/Maps/UI/location_page.dart';
import 'package:gobunty/Modals/cartModal.dart';
import 'package:gobunty/Modals/hservice.dart';
import 'package:gobunty/Modals/promo_code.dart';
import 'package:gobunty/Pages/payment_method.dart';
import 'package:flutter/material.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:gobunty/Components/bottom_bar.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:flutter/rendering.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:shimmer/shimmer.dart';
import 'package:toast/toast.dart';
import 'package:gobunty/HomeOrderAccount/home_order_account.dart';
import 'package:gobunty/Pages/newitem.dart';

List<DropdownMenuItem<String>> listDrop = [];

void loadData() {
  listDrop = [];
  listDrop.add(DropdownMenuItem(
    child: Text('1 kg'),
    value: 'A',
  ));
  listDrop.add(DropdownMenuItem(
    child: Text('500 g'),
    value: 'B',
  ));
  listDrop.add(DropdownMenuItem(
    child: Text('250 g'),
    value: 'C',
  ));
}

class ViewCart extends StatefulWidget {
  String add, promoid, title;
  bool value;
  ViewCart({this.add, this.promoid, this.title, this.value});
  @override
  _ViewCartState createState() => _ViewCartState(add, promoid, title, value);
}

enum AddressType {
  Home,
  Office,
  Other,
}
AddressType selectedAddress = AddressType.Other;

class _ViewCartState extends State<ViewCart> {
  String add;
  String Status = "no";
  int _itemCount = 0;
  int _itemCount1 = 0;
  int _itemCount2 = 0;
  int count;
  List<String> list = [];
  List<cartModal> list1 = [];
  String total1 = "0", title, promoid;
  String WalletAmount = "0";
  bool value;
  bool chan = false, check = false;
  String fee = "0", tips = "0", gtotal = "0";
  TextEditingController controller = TextEditingController();
  TextEditingController tip = TextEditingController();
  String address1 = "", aid = "";
  _ViewCartState(this.add, this.promoid, this.title, this.value);
  ProgressDialog pr;
  String price1;
  String Servicetax = "0";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pr = new ProgressDialog(context, showLogs: true, isDismissible: false);
    pr.style(message: 'Please wait...');
    getCart();
    fetch();
    if (value == true) {
      value = true;
      pr.show();
      // getData();
      total();
    }
    Walleet();
  }

  Future<Map> total() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
    };
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/cart/total_praise",
        body: data1);

    Map data2 = json.decode(response.body);
    print(data2);

    if (mounted) {
      setState(() {
        pr.hide();
        total1 = data2['total_price'].toString();
        price1 = data2['total_price'].toString();

        Price = "100";

        Servicetax = ((double.parse(total1)*2)/100).toString();

        if(double.parse(total1) > 300 && double.parse(Price) > 50){

        String  Amountmain = (double.parse(total1) + double.parse(fee)+double.parse(Servicetax) + double.parse(tips)  ).toStringAsFixed(2);
        // String  Amountmain = (double.parse(total1) + double.parse(fee) +  double.parse(tips)  ).toStringAsFixed(2);
          gtotal = (double.parse(Amountmain)-50).toStringAsFixed(2);
          Status = "yes";
          print("ifamountttt");
        }
        else{
          gtotal = (double.parse(total1) + double.parse(fee) + double.parse(Servicetax) + double.parse(tips)).toStringAsFixed(2);
          // gtotal = (double.parse(total1)+ double.parse(fee) +double.parse(Percent)+ double.parse(tips)).toStringAsFixed(2);
          print("elseamount");
          Status = "no";
        }

      });

      //list.add("1 kg");

    }
    return data2;
  }

  //Wallet Apiiiii
  String Price;
  Walleet() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var user_id = sharedPreferences.getString('uid');
    Map data = {
      'user_id':user_id,
    };

    print(data);

    // pr.show();
    var jsonResponse = null;
    var response = await http.post('https://gobunty.com/api/User/wallet_list',);

    if(response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
        // pr.hide();
        print(jsonResponse);
        if (jsonResponse ['msg'].toString().contains("Success")) {
          for (var data in jsonResponse['user_data']) {
            setState(() {
              Price = jsonResponse['user_data']['price'];
            });
          }
        }
      }

      setState(() {

      });
      print(jsonResponse);
    } else {
      // pr.hide().whenComplete(() =>
      Toast.show("No Amount In your Wallet", context);
      print('Something went wrong');
    }
  }


  Future<Map> applyTotal() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'promocode_id': promoid,
      'amount':price1,
    };
    var jsonResponse = null;
    var response = await http
        .post("https://gobunty.com/api/Product/promocode_amount", body: data1);

    Map data2 = json.decode(response.body);
    print(data2);
    if (response.statusCode == 200) {
      if (mounted) {
        setState(() {
          pr.hide();
          total1 = data2['newprice'].toString();
          gtotal = (double.parse(total1) + double.parse(tips) + double.parse(fee)).toStringAsFixed(2).toString();
        });

        //list.add("1 kg");

      }
    } else if (response.statusCode == 404) {
      setState(() {
        check = true;
      });

      Toast.show("No Shop Available", context);
    } else {
      Toast.show("Connection Error", context);
    }
    return data2;
  }

  Future<Map> getCart() async {
    String ip;
    int k;
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
    };
    var jsonResponse = null;
    var response = await http
        .post("https://gobunty.com/api/cart/fetch_cartproduct", body: data1);

    Map data2 = json.decode(response.body);
    print(data2);
    if (response.statusCode == 200) {
      check = true;
      if (data2['status'].toString().contains("true")) {
        for (var data in data2['Product_data']) {
          if (mounted) {
            setState(() {
              pr.hide().whenComplete(() => list1.add(new cartModal(
                  data['id'],
                  data['name'],
                  data['market_id'],
                  data['price'],
                  data['quantity'],
                  data['unit'],
                  "subcat",
                  data['image'])));
              if (list.length < data2['Product_data'].length) {
                list.add("0");
              }

              vid = data['vendor_id'];
              fee=data2['delivery_fee'];
            });
          }
          //list.add("1 kg");

        }
      } else {}
    } else if (response.statusCode == 404) {
      setState(() {
        check = true;
      });

      Toast.show("No Items Available", context);
    } else {
      Toast.show("Connection Error", context);
    }
    print(data2);
    return data2;
  }

  Future<Map> updateCart(String pid, String qty, String price) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'product_id': pid,
      'qty': qty,
      'price': price,
    };
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/cart/update_cart",
        body: data1);

    Map data = json.decode(response.body);
    if (data['msg'].toString().contains("Success")) {
      pr.hide();
      Toast.show("updated", context);
    } else {
      pr.hide();
      Toast.show("something went wrong", context);
    }
    return data;
  }

  Future<Map> removeCart(String pid, int i) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'product_id': pid,
    };
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/cart/remove_cart",
        body: data1);

    Map data = json.decode(response.body);
    if (data['msg'].toString().contains("Success")) {
      pr.hide();
      setState(() {
        list.removeAt(i);

        list1.clear();
        getCart();
        value = false;
      });
    } else {
      pr.hide();
      Toast.show("something went wrong", context);
    }
    return data;
  }

  Future<Map> fetch() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'type': add,
    };
    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/User_Delivery/fetch_address",
        body: data1);

    Map<String, dynamic> map = json.decode(response.body);
    var data2 = map["address_data"];

    print(data2);
    if (data2 != null) {
      if (mounted) {
        setState(() {
          address1 = data2['address'];
          aid = data2['id'];
          /* listadr.add(new AdrModal(
              data['id'], data['address'], "", data['type'], data['latitude'],
              data['longitude']));*/
        });
      }
    } else {
      Toast.show("No Address Available", context);
    }

    print(address1);
  }

  void choiceAction(String choice) {
    if (choice == Constants.Settings) {
      setState(() {
        add = "Office";

        fetch();
      });
    } else if (choice == Constants.Subscribe) {
      setState(() {
        add = "Home";
        fetch();
      });
    } else if (choice == Constants.SignOut) {
      setState(() {
        add = "Other";
        fetch();
      });
    }
  }

  bool check2 = false;
  String selected = "1 kg";
  @override
  Widget build(BuildContext context) {
    loadData();
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Confirm Order', style: Theme.of(context).textTheme.bodyText1),
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            color: kMainColor,
            onPressed: () =>
                // Navigator.pop(context)
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomeOrderAccount(),
                    )),
        ),
      ),

        body: Stack(
        children: <Widget>[
        // SingleChildScrollView(
        // padding: EdgeInsets.only(bottom: 120),
        // child: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        // children: <Widget>[
          ListView(
            children: <Widget>[
              value == true
                  ? SizedBox()
                  : Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Card(
                            elevation: 3,
                            child: Container(
                              color: Colors.white,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 20.0,
                                    right: 20.0,
                                    top: 13.0,
                                    bottom: 13.0),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.location_on,
                                          color: Color(0xffc4c8c1),
                                          size: 13.3,
                                        ),
                                        SizedBox(
                                          width: 11.0,
                                        ),
                                        Text('Deliver to ',
                                            style: Theme.of(context)
                                                .textTheme
                                                .caption
                                                .copyWith(
                                                    color: kDisabledColor,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                        Text(add,
                                            style: Theme.of(context)
                                                .textTheme
                                                .caption
                                                .copyWith(
                                                    color: kMainColor,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                        // Spacer(),
                                        InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        LocationPage(
                                                            bool1: false)));
                                          },
                                          child: Text('Edit Location',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .caption
                                                  .copyWith(
                                                      color: kMainColor,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                        ),
                                        PopupMenuButton(
                                          onSelected: choiceAction,
                                          itemBuilder: (BuildContext context) {
                                            return Constants.choices
                                                .map((String choice) {
                                              return PopupMenuItem(
                                                value: choice,
                                                child: Text(choice),
                                              );
                                            }).toList();
                                          },
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(address1,
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption
                                            .copyWith(
                                                fontSize: 11.7,
                                                color: Color(0xffb7b7b7)))
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
              value == true
                  ? SizedBox()
                  : Container(
                      padding: EdgeInsets.all(10.0),
                      color: kCardBackgroundColor,
                      child: Text('Cart Items',
                          style: Theme.of(context).textTheme.headline6.copyWith(
                              color: Color(0xff616161), letterSpacing: 0.67)),
                    ),
              value == true
                  ? SizedBox()
                  : check == true
                      ?
                       Container(
                          child: SizedBox(
                            height: 140 * list1.length.toDouble(),
                            child: list1.length < 1
                                ? Center(
                                    child: Text(
                                      "No Items Found",
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                : new ListView.builder(
                                    itemCount: list1.length,
                                    shrinkWrap: true,
                                    scrollDirection: Axis.vertical,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder: (context, i) {
                                      return cartOrderItemListTile(
                                          context,
                                          i,
                                          list1[i].name,
                                          list1[i].price,
                                          list1[i].url,
                                          _itemCount);
                                    },
                                  ),
                          ),
                        )
                      : Container(
                          height: 500,
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[300],
                            highlightColor: Colors.grey[100],
                            enabled: true,
                            child: ListView.builder(
                              itemBuilder: (_, __) => Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: 48.0,
                                      height: 48.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 8.0),
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            width: double.infinity,
                                            height: 8.0,
                                            color: Colors.white,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 2.0),
                                          ),
                                          Container(
                                            width: double.infinity,
                                            height: 8.0,
                                            color: Colors.white,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 2.0),
                                          ),
                                          Container(
                                            width: 40.0,
                                            height: 8.0,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              itemCount: 6,
                            ),
                          )),
              Divider(
                color: kCardBackgroundColor,
                thickness: 6.7,
              ),
              value == false
                  ?

              BottomBar(
                text:  "Place Order",
                onTap: () {
                  setState(() {
                    if (list1.length > 0) {
                      value = true;
                      pr.show();
                      // getData();
                      total();
                    }
                  });
                },
              )

              // FlatButton(
              //         child: Text(
              //           "Place Order",
              //           // color: Colors.blueAccent,
              //           // textColor: Colors.white,
              //           style: Theme.of(context).textTheme.caption.copyWith(
              //               color: kMainColor, fontWeight: FontWeight.bold),
              //         ),
              //         textTheme: ButtonTextTheme.accent,
              //         onPressed: () {
              //           setState(() {
              //             if (list1.length > 0) {
              //               value = true;
              //               pr.show();
              //               // getData();
              //               total();
              //             }
              //           });
              //         },
              //       )
                  : SizedBox(),
              value != true
                  ? Divider(
                      color: kCardBackgroundColor,
                      thickness: 6.7,
                    )
                  :
                    Padding(
                        padding: EdgeInsets.  all(10),
                        child :Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        FlatButton(
                          child: new Text(
                            "Back",
                            style: Theme.of(context).textTheme.caption.copyWith(
                                color: kMainColor, fontWeight: FontWeight.bold),
                          ),
                          textTheme: ButtonTextTheme.accent,
                          onPressed: () {
                            setState(() {
                              if (list1.length > 0) {
                                value = false;
                              }
                            });
                          },
                        ),
                        /* Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: EntryField(
                            hint:
                                'Any instruction? E.g Carry package carefully',
                            image: 'images/custom/ic_instruction.png',
                            border: InputBorder.none,
                          ),
                        ),*/
                        Center(
                       child : Container(
                          padding: EdgeInsets.all(10.0),
                          color: kCardBackgroundColor,
                          child: Text('Delivery Tip',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(
                                      color: Color(0xff616161),
                                      letterSpacing: 0.67)),
                        ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                                left: 10, right: 10, top: 5, bottom: 5),
                            child: Card(
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          tips = "5";
                                          total();
                                        });
                                      },
                                      child: Container(
                                        width: width * 0.18,
                                        height: width * 0.08,
                                        decoration: boxDecoration(
                                            color: Colors.red,
                                            radius: spacing_control),
                                        alignment: Alignment.center,
                                        child: text("\₹ 5",
                                            textColor: Colors.blue,
                                            isCentered: true),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          tips = "10";
                                          total();
                                        });
                                      },
                                      child: Container(
                                        width: width * 0.18,
                                        height: width * 0.08,
                                        decoration: boxDecoration(
                                            color: Colors.red,
                                            radius: spacing_control),
                                        alignment: Alignment.center,
                                        child: text("\₹ 10",
                                            textColor: Colors.blue,
                                            isCentered: true),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          tips = "20";
                                          total();
                                        });
                                      },
                                      child: Container(
                                        width: width * 0.18,
                                        height: width * 0.08,
                                        decoration: boxDecoration(
                                            color: Colors.red,
                                            radius: spacing_control),
                                        alignment: Alignment.center,
                                        child: text("\₹ 20",
                                            textColor: Colors.blue,
                                            isCentered: true),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )),
                        Divider(
                          color: kCardBackgroundColor,
                          thickness: 6.7,
                        ),
                        Container(
                          width: double.infinity,
                          padding: EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 20.0),
                          child: Text('PAYMENT INFO',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(color: kDisabledColor)),
                          color: Colors.white,
                        ),
                        Container(
                          color: Colors.white,
                          padding: EdgeInsets.symmetric(
                              vertical: 4.0, horizontal: 20.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Sub Total',
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                Text(
                                  '\₹ ' + total1,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                              ]),
                        ),
                        Divider(
                          color: kCardBackgroundColor,
                          thickness: 1.0,
                        ),
                        Container(
                          color: Colors.white,
                          padding: EdgeInsets.symmetric(
                              vertical: 4.0, horizontal: 20.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Delivery Fee",
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                Text(
                                  '\₹ ' + fee,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                              ]),
                        ), Divider(
                          color: kCardBackgroundColor,
                          thickness: 1.0,
                        ),
                        Container(
                          color: Colors.white,
                          padding: EdgeInsets.symmetric(
                              vertical: 4.0, horizontal: 20.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Service Tax",
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                Text(
                                "\₹ "+ Servicetax,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                              ]),
                        ),
                        Divider(
                          color: kCardBackgroundColor,
                          thickness: 1.0,
                        ),
                        Container(
                          color: Colors.white,
                          padding: EdgeInsets.symmetric(
                              vertical: 4.0, horizontal: 20.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Delivery Tip',
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                Text(
                                  '\₹ ' + tips,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                              ]),
                        ),
                        Divider(
                          color: kCardBackgroundColor,
                          thickness: 1.0,
                        ),
                // fvvdr
                // if(double.parse(total1) > 300 && double.parse(Price) > 50){
                        double.parse(total1) > 300 && double.parse(Price) > 50?Container(
                          color: Colors.white,
                          padding: EdgeInsets.symmetric(
                              vertical: 4.0, horizontal: 20.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Wallet Amount',
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                Text(
                                  '- ' + '\₹ 50 ',
                                  style: Theme.of(context).textTheme.caption,
                                ),
                              ]),
                        ):SizedBox(),
                        Divider(
                          color: kCardBackgroundColor,
                          thickness: 1.0,
                        ),
                        Container(
                          color: Colors.white,
                          padding: EdgeInsets.symmetric(
                              vertical: 4.0, horizontal: 20.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Amount to Pay',
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption
                                      .copyWith(fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  '\₹ ' + gtotal,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                              ]),
                        ),
                        Divider(
                          color: kCardBackgroundColor,
                          thickness: 3.0,
                        ),
                     Center(
                       child : Container(
                          padding: EdgeInsets.all(10.0),
                          color: kCardBackgroundColor,
                          child: Text('PromoCode',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(
                                      color: Color(0xff616161),
                                      letterSpacing: 0.67)),
                        ),
                      ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 10, right: 10, top: 5, bottom: 5),
                          child: Card(
                            elevation: 3,
                            child: ClipRRect(
                              child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 2,
                                        child: text(title, isCentered: true),
                                      ),
                                      Expanded(
                                          flex: 1,
                                          child: GestureDetector(
                                            onTap: () {
                                              Navigator.pop(context);
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          FoodCoupon(
                                                              vid: vid)));
                                              // _showSingleChoiceDialog(context);
                                            },
                                            child: text("Promo Code",
                                                textColor: food_colorAccent,
                                                isCentered: true),
                                          ))
                                    ],
                                  )),
                            ),
                          ),
                        ),

                       FlatButton(
                          child: new Text(
                            "Apply",
                            style: Theme.of(context).textTheme.caption.copyWith(
                                color: kMainColor, fontWeight: FontWeight.bold),
                          ),
                          textTheme: ButtonTextTheme.accent,
                          onPressed: () {
                            setState(() {
                              if(promoid!="1") {
                                pr.show();
                                applyTotal();
                              }
                              else
                                {
                                  Toast.show("Please Select Promo Code", context);
                                }
                            });
                          },
                        ),
                        Container(
                          height: 132.0,
                          color: kCardBackgroundColor,
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 40)),
                      ],
                    )
              )
            ],
          ),
          value != true
              ? Divider(
                  color: kCardBackgroundColor,
                  thickness: 6.7,
                )
              : Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        color: Colors.white,
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 13.0, bottom: 13.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.location_on,
                                    color: Color(0xffc4c8c1),
                                    size: 13.3,
                                  ),
                                  SizedBox(
                                    width: 11.0,
                                  ),
                                  Text('Deliver to ',
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption
                                          .copyWith(
                                              color: kDisabledColor,
                                              fontWeight: FontWeight.bold)),
                                  Text(add,
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption
                                          .copyWith(
                                              color: kMainColor,
                                              fontWeight: FontWeight.bold)),
                                  Spacer(),
                                  Spacer(),
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  LocationPage(bool1: false)));
                                    },
                                    child: Text('Edit Location',
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption
                                            .copyWith(
                                                color: kMainColor,
                                                fontWeight: FontWeight.bold)),
                                  ),
                                  PopupMenuButton(
                                    onSelected: choiceAction,
                                    itemBuilder: (BuildContext context) {
                                      return Constants.choices
                                          .map((String choice) {
                                        return PopupMenuItem(
                                          value: choice,
                                          child: Text(choice),
                                        );
                                      }).toList();
                                    },
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 13.0,
                              ),
                              Text(address1,
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption
                                      .copyWith(
                                          fontSize: 11.7,
                                          color: Color(0xffb7b7b7)))
                            ],
                          ),
                        ),
                      ),
                      BottomBar(
                        text: "Pay \₹ " + gtotal,
                        onTap: () {
                          if (gtotal != "0" && address1 != "") {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PaymentPage(
                                          money: gtotal, aid: aid,promoid:promoid,Status:Status)));
                          } else {
                            Toast.show(
                                "Please Fill Address From Setlocation on Home",
                                context);
                          }
                        },
                      ),
                    ],
                  ),
                ),
                      // ),
                  ],
               ),
           // )
        );
    //    ],
    //
    //   ),
    // );
    // );
  }

  String selectedValue;
  String vid;

  String id;

  Widget cartOrderItemListTile(BuildContext context, int i, String title,
      String price, String url, int itemCount) {
    var width = MediaQuery.of(context).size.width;
    return Column(
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.only(left: 7.0, top: 13.3),
            child: ListTile(
              // contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
              leading: new CircleAvatar(
                backgroundImage: new NetworkImage(
                  url,
                ),
              ),
              title: Text(
                title + " (" + price + " Rs)",
                style: Theme.of(context)
                    .textTheme
                    .subtitle2
                    .copyWith(color: kMainTextColor),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 15.0, bottom: 14.2),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        child: FlatButton(
                          child: new Text(
                            "Remove",
                            style: Theme.of(context).textTheme.caption.copyWith(
                                color: kMainColor, fontWeight: FontWeight.bold),
                          ),
                          textTheme: ButtonTextTheme.accent,
                          onPressed: () {
                            setState(() {
                              pr.show();
                              removeCart(list1[i].id, i);
                            });
                          },
                        ),
                      ),
                      Spacer(),
                      Container(
                        height: width * 0.08,
                        alignment: Alignment.center,
                        width: width * 0.23,
                        decoration: boxDecoration(
                            color: food_textColorPrimary,
                            radius: spacing_control),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: width * 0.08,
                              height: width * 0.08,
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.only(
                                      bottomLeft:
                                          Radius.circular(spacing_control),
                                      topLeft:
                                          Radius.circular(spacing_control))),
                              child: IconButton(
                                icon: Icon(Icons.remove,
                                    color: food_white, size: 10),
                                onPressed: () {
                                  setState(() {
                                    if (int.parse(list1[i].quatity) > 1) {
                                      count = int.parse(list1[i].quatity) - 1;
                                      list1[i].quatity = count.toString();

                                      updateCart(list1[i].id, list1[i].quatity,
                                          list1[i].price);
                                      itemCount--;
                                    }
                                    value = false;
                                  });
                                },
                              ),
                            ),
                            text(list1[i].quatity),
                            Container(
                              width: width * 0.08,
                              height: width * 0.08,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.only(
                                      bottomRight:
                                          Radius.circular(spacing_control),
                                      topRight:
                                          Radius.circular(spacing_control))),
                              child: IconButton(
                                icon: Icon(Icons.add,
                                    color: food_white, size: 10),
                                onPressed: () {
                                  setState(() {
                                    count = int.parse(list1[i].quatity) + 1;
                                    list1[i].quatity = count.toString();
                                    updateCart(list1[i].id, list1[i].quatity,
                                        list1[i].price);
                                    itemCount++;
                                    value = false;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ]),
              ),
            ))
      ],
    );
  }
}




class Constants {
  static const String Subscribe = 'Home';
  static const String Settings = 'Office';
  static const String SignOut = 'Other';

  static const List<String> choices = <String>[Subscribe, Settings, SignOut];
}
