import 'dart:convert';


import 'package:cached_network_image/cached_network_image.dart';
import 'package:gobunty/Components/custom_appbar.dart';
import 'package:gobunty/Components/search_bar.dart';
import 'package:gobunty/Modals/cartModal.dart';
import 'package:gobunty/Pages/view_cart.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:gobunty/Themes/style.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

List<String> list = ['1 kg', '500 g', '250 g'];

// ignore: must_be_immutable
class ItemsPage extends StatefulWidget {
  String id;
  String model,addr,fee;
  ItemsPage({this.id, this.model,this.addr,this.fee});

  @override
  _ItemsPageState createState() => _ItemsPageState(id: id, model: model,addr:addr,fee:fee);
}

class _ItemsPageState extends State<ItemsPage> {
  int itemCount = 0;
  String model;
  String id,addr,fee;
  String text = "Add";
  List<String> list = [];
  List<String> list1=[];
  List<String> listto=[];
  String selected="1 kg";
  _ItemsPageState({this.id, this.model,this.addr,this.fee});
  ProgressDialog pr;

  final List<Tab> tabs = <Tab>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
    getCart();
    getsub();
  }
  Future<Map> getsub() async {
    SharedPreferences pref=await SharedPreferences.getInstance();
    Map data1 = {
      'vendor_id': id,
    };
    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/Product/subcategory_list",
        body: data1);

    Map data = json.decode(response.body);
    if(data['status'].toString().contains("true"))
    {
      if(list.length<data['vendor_data'].length) {
        for (int i = 0; i < data['vendor_data'].length; i++) {
          setState(() {
            tabs.add(new Tab(text: data['vendor_data'][i]['name'],));
          });

        }
        getCart();
      }}
    else
    {

      showDialog(
          context: context,
          builder: (BuildContext context) {
        return mAlertItem2;
      },
    );
      // Toast.show("Clear Previous Cart", context);
    }
    print(data);
    return data;
  }


//Alert Dialog

  AlertDialog mAlertItem2 = AlertDialog(
    // backgroundColor: appStore.scaffoldBackground,
    title: Text("Replace Cart item?"),
    // title: Text("Confirmation", style: boldTextStyle(color: appStore.textPrimaryColor)),
    content: Text(
      "Your cart contains dishes from another Shop.Do you want to discard the selection and add this shop item.",
      // style: secondaryTextStyle(color: kMainColor),
    ),
    actions: [
      FlatButton(
        child: Text(
          "Yes",
          // style: primaryTextStyle(color: appColorPrimary),
        ),
        onPressed: () {
          // Navigator.of().pop();
        },
      ),
      FlatButton(
        child: Text("No",
          // style: primaryTextStyle(color: appColorPrimary)
        ),
        onPressed: () {
          // Navigator.of().pop();
        },
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true,isDismissible: false);
    pr.style(message: 'Please wait...');
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80.0),
          child: CustomAppBar(
            leading: new IconButton(
                icon: new Icon(Icons.arrow_back),
                color: kMainColor,
                onPressed: () => Navigator.of(context).pop()),
            titleWidget: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(model,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(color: kMainTextColor)),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.location_on,
                        color: kIconColor,
                        size: 10,
                      ),
                      SizedBox(width: 10.0),
                      Text('',
                          style: Theme.of(context).textTheme.overline),

                      Flexible(child:
                      Text(addr,
                          style: Theme.of(context).textTheme.overline),),
                      Spacer(),
                      Icon(
                        Icons.access_time,
                        color: kIconColor,
                        size: 10,
                      ),
                      SizedBox(width: 10.0),
                      Text('20 MINS',
                          style: Theme.of(context).textTheme.overline),
                      SizedBox(width: 20.0),
                    ],
                  ),
                ],
              ),
            ),
            bottom: PreferredSize(
              /*preferredSize: Size.fromHeight(0.0),*/
              child: Column(
                children: <Widget>[
                  //CustomSearchBar(hint: 'Search item or store'),
                 /* TabBar(
                    tabs: tabs,
                    isScrollable: true,
                    labelColor: kMainColor,
                    unselectedLabelColor: kLightTextColor,
                    indicatorPadding: EdgeInsets.symmetric(horizontal: 24.0),
                  ),*/
                  Divider(
                    color: kCardBackgroundColor,
                    thickness: 8.0,
                  )
                ],
              ),
            ),
          ),
        ),
        body:  Stack(
              children: <Widget>[
                FutureBuilder(
                  future: getData(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    // print(snapshot.data);
                    if (snapshot.data == null) {
                      return Container(
                        child: Center(
                          child: SpinKitWave(
                            color: Color(0xFF017BFF),
                            size: 50.0,
                          ),
                        ),
                      );
                    } else {
                      //print(snapshot.data);
                      return /*snapshot.data['status'].toString().contains("false")? Center(
                        child: Text(
                          "No Product Found",
                          textAlign: TextAlign.center,
                        ),
                      ):*/ Padding(padding: EdgeInsets.only(bottom: 75.0),
                        child: ListView.builder(
                        itemCount: snapshot.data['msg'] != "Record not Found"
                            ? snapshot.data["Product_data"].length
                            : 0,
                        itemBuilder: (BuildContext context, int index) {

                            print(listto);
                            print(list1);
                          return Stack(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 10.0, top: 10.0, right: 10.0),
                                    child:CircleAvatar(
                                      backgroundImage: CachedNetworkImageProvider(snapshot.data["Product_data"][index]
                                      ["image"]),
                                      radius: 40,
                                    ),
                                  ),
                                  Flexible(child:
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[

                                      Text(
                                          snapshot.data["Product_data"][index]
                                              ["name"].toString().replaceAll("\n", ""),
                                          style: bottomNavigationTextStyle
                                              .copyWith(fontSize: 15),),

                                      Text(
                                        snapshot.data["Product_data"][index]
                                        ["description"]..toString().replaceAll("\n", ""),
                                        style: bottomNavigationTextStyle
                                            .copyWith(fontSize: 12),),
                                      Text(
                                          '\₹ ' +
                                              snapshot.data["Product_data"][index]
                                                  ["price"],
                                          style: Theme.of(context)
                                              .textTheme

                                              .caption),
                                      snapshot.data['status'].toString().contains("true")?
                                      Text(
                                          list[index].toString().contains("Remove")?"Added":""
                                              ,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption):SizedBox(),
                                      SizedBox(
                                        height: 20.0,
                                      ),
                                    ],
                                  ),),
                                ],
                              ),
                              snapshot.data['status'].toString().contains("true")?
                              Positioned(
                                  right: 20.0,
                                  bottom: 14,
                                  child: Container(
                                    height: 30.0,
                                    child: FlatButton(
                                      child: new Text(
                                        list1[index]!=null?list1[index]:text,
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption
                                            .copyWith(
                                                color: kMainColor,
                                                fontWeight: FontWeight.bold),
                                      ),
                                      textTheme:ButtonTextTheme.accent,
                                      onPressed: () {
                                        setState(() {


                                          if(list1[index].contains("Add"))
                                            {
                                              pr.show();
                                              addCart(snapshot.data['Product_data'][index]['id'],index);
                                            }
                                          else{
                                                  pr.show();
                                            removeCart(snapshot.data['Product_data'][index]['id'],index);
                                          }

                                        });
                                      },
                                    ),
                                  ),
                              ):SizedBox(),
                              Divider(
                                color: Colors.black12,
                                height: 1.0,
                                thickness: 1.0,
                              )
                            ],
                          );
                        },
                        ),);

                    }
                  },
                ),

                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          'images/icons/ic_cart wt.png',
                          height: 19.0,
                          width: 18.3,
                        ),
                        SizedBox(width: 20.7),
                        Text(
                          itemCount.toString() + " items",
                          style: bottomBarTextStyle.copyWith(
                              fontSize: 15, fontWeight: FontWeight.w500),
                        ),
                        Spacer(),
                        FlatButton(
                          color: Colors.white,
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ViewCart(add:"Home")
                          ,
                            ),
                          );},
                          child: Text(
                            'View Cart',
                            style: Theme.of(context).textTheme.caption.copyWith(
                                color: kMainColor, fontWeight: FontWeight.bold),
                          ),
                          textTheme: ButtonTextTheme.accent,
                          disabledColor: Colors.white,
                        ),
                      ],
                    ),
                    color: kMainColor,
                    height: 60.0,
                  ),
                ),
              ],
            ),


      ),
    );
  }

  Future<Map> getData() async {
    SharedPreferences pref=await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'vendor_id': id,
    };


    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/product/product_list",
        body: data1);

    Map data = json.decode(response.body);
    if(data['status'].toString().contains("true"))
      {
    if(list.length<data['Product_data'].length) {
      for (int i = 0; i < data['Product_data'].length; i++) {
        listto.add(data['Product_data'][i]['id']);
        list.add("1 kg");
        list1.add("Add");
      }
      getCart();
    }}
    else
      {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return mAlertItem2;
          },
        );
        // Toast.show("Clear Previous Cart", context);
      }
  print(data);
    return data;
  }
  Future<Map> addCart(String pid,int i) async {
    SharedPreferences pref=await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'product_id':pid,
    };
    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/cart/addtocart",
        body: data1);

    Map data = json.decode(response.body);

    print(data);
    if(data['msg'].toString().contains("Success"))
      {
        pr.hide();
        setState(() {
          itemCount=int.parse(data['product_count']);
          list1[i]="Remove";

          Toast.show("Item Added To Cart",context);
        });

      }
    else
      {
        pr.hide();
        Toast.show("Something Went Wrong", context);
      }
    return data;
  }
  Future<Map> removeCart(String pid,int i) async {
    SharedPreferences pref=await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'product_id':pid,
    };
    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/cart/remove_cart",
        body: data1);

    Map data = json.decode(response.body);
    if(data['msg'].toString().contains("Success"))
    {
      pr.hide();
      setState(() {
        itemCount=int.parse(data['product_count']);
        list1[i]="Add";
        Toast.show("Item Remove from Cart",context);
      });

    }
    else
    {
      pr.hide();
      Toast.show("Something Went Wrong", context);
    }
    return data;
  }
  Future<Map> getCart() async {
    String ip;
    int k;
    SharedPreferences pref=await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
    };
    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/cart/fetch_cartproduct",
        body: data1);

    Map data = json.decode(response.body);
    print(data['status']);
    if(data['status'].toString().contains("true")) {
      for (int i = 0; i < data['Product_data'].length; i++) {
        setState(() {
          ip = data['Product_data'][i]['id'];
          k = listto.indexOf(ip);
            if(list1.length!=null) {
              list1[k] = "Remove";
            }
        });
        //list.add("1 kg");
      }
      setState(() {
        itemCount=data['Product_data'].length;
      });
    }
    return data;
  }
}

class BottomSheetWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          height: 80.7,
          color: kCardBackgroundColor,
          padding: EdgeInsets.all(10.0),
          child: ListTile(
            title: Text('Fresh Red Onions',
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(fontSize: 15, fontWeight: FontWeight.w500)),
            subtitle: Text('Vegetables',
                style:
                    Theme.of(context).textTheme.caption.copyWith(fontSize: 15)),
            trailing: FlatButton(
              color: Colors.white,
              onPressed: () {/*...*/},
              child: Text(
                'Add',
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(color: kMainColor, fontWeight: FontWeight.bold),
              ),
              textTheme: ButtonTextTheme.accent,
              disabledColor: Colors.white,
            ),
          ),
        ),
        CheckboxGroup(
          labelStyle:
              Theme.of(context).textTheme.caption.copyWith(fontSize: 16.7),
          labels: list,
        ),
      ],
    );
  }

}
