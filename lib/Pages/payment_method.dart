import 'dart:convert';
import 'dart:math';

import 'package:flutter/rendering.dart';
import 'package:gobunty/Components/list_tile.dart';
import 'package:gobunty/Pages/order_placed.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class PaymentPage extends StatelessWidget {
  String money, aid,promoid,Status;
  PaymentPage({this.money, this.aid,this.promoid,this.Status});

  @override
  Widget build(BuildContext context) {
    return Payment(money: money, aid: aid,promoid:promoid,Status:Status);
  }
}

class Payment extends StatefulWidget {
  String money, aid,promoid,Status;
  Payment({this.money, this.aid,this.promoid,this.Status});
  @override
  PaymentState createState() => PaymentState(money: money, aid: aid,promoid:promoid,Status:Status);
}

class PaymentState extends State<Payment> {
  String money, aid,promoid,Status;
  PaymentState({this.money, this.aid,this.promoid,this.Status});
  Razorpay _razorpay;
  String orderid;
  FlutterLocalNotificationsPlugin fltrNotification;
  String _selectedParam;
  String task;
  int val = 1;
  ProgressDialog pr;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _razorpay = new Razorpay();

    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
//    getorder();
    var androidInitilize = new AndroidInitializationSettings('app_icon');
    var iOSinitilize = new IOSInitializationSettings();
    var initilizationsSettings = new InitializationSettings(
        android: androidInitilize, iOS: iOSinitilize);
    fltrNotification = new FlutterLocalNotificationsPlugin();
    fltrNotification.initialize(initilizationsSettings,
        onSelectNotification: notificationSelected);
  }

  Future notificationSelected(String payload) async {

  }
  Future _showNotification() async {
    var androidDetails = new AndroidNotificationDetails(
        "Channel ID", "Go Bunty", "Delivery App",
        importance: Importance.max);
    var iSODetails = new IOSNotificationDetails();
    var generalNotificationDetails =
        new NotificationDetails(android: androidDetails, iOS: iSODetails);

    await fltrNotification.show(0, "Orders", "Your Order dispatched shortly",
        generalNotificationDetails);
  }

  void payment(String orderid1, String paymentid, String method, String status,
      String price) async {
    SharedPreferences pref = await SharedPreferences.getInstance();

    Map data = {
      'price': price,
      'status': status,
      'method': method,
      'order_id': orderid1,
      'user_id': pref.getString('uid'),
      'payment_id': paymentid,
      'delivery_fee': "50",
      'promo_id':promoid,
      'delivery_address_id': aid,
      'price':Status
    };
    print(data);
    var response=await http.post("https://gobunty.com/api/Payment/add_payment", body: data);

    if (response.statusCode == 200) {
      String responeBody = response.body;
      var jsonBody = json.decode(responeBody);

      var data = jsonBody['msg'];
      print(data);
      if (data.toString().contains("Success")) {
        setState(() {
          _showNotification();
          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => OrderPlaced(),
              ));
        });

      }
    }
    else if (response.statusCode == 404) {

      Toast.show("No Shop Available", context);
    }
    else {
      Toast.show("Connection Error", context);
    }
    print(response.statusCode);
    print(response.body);
  }

  static const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  void getorder() async {
    String username = 'rzp_live_uwn5S3pU0iME9D';
    String password = 'd1pn4kdVAzghXXTGCCCo3KNe';
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));
    double newmoney=double.parse(money.toString())*100;
    int nw=newmoney.toInt();
    print(nw);
    Map data = {
      "amount":nw.toString(),
      "currency": "INR",
      "receipt": "receipt_" + getRandomString(5)
    }; // as per my experience the receipt doesn't play any role in helping you generate a certain pattern in your Order ID!!

    var headers = {"content-type": "application/json"};

    var res = await http.post('https://api.razorpay.com/v1/orders',
        headers: <String, String>{'authorization': basicAuth}, body: data);
    if (res.statusCode == 200) {
      Map data2 = json.decode(res.body);
      setState(() {
        orderid = data2['id'];
      });
      openCheckout();
      Toast.show(orderid, context);
    }
    else
      {
        print(res.body);
        print(res.statusCode);
      }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  void openCheckout() async {
    // String receipt = "order_" + getRandomString(15);
    // int price1 = int.parse(money.toString());
    RegExp regex = RegExp(r"([.]*0)(?!.*\d)");
    double money1 = double.parse(money.toString());
    double tMoney = money1 * 100;
    String stringMoney = tMoney.toString().replaceAll(regex, '');
    double totalMoney = double.parse(stringMoney.toString());

    // print(money1);
    // print(tMoney);
    // print(stringMoney);
    // print(totalMoney);

    var options = {
      "key": "rzp_live_uwn5S3pU0iME9D",
      // // "amount": totalMoney,
      // "amount": tMoney,
      "name": "Go Bunty",
      "currency": "INR",
      "order_id": orderid,
      "timeout": "300",
    };

    try {
      if (orderid.isNotEmpty) {
        setState(() {
          _razorpay.open(options);
        });
      }
    } catch (e) {
      print(e.toString());
      Toast.show(e.toString(), context);
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    // Do something when payment succeeds
    Toast.show("Success" + response.paymentId, context);
    payment(response.orderId, response.paymentId, "Online", "true", money);
    // Navigator.pop(context);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    // Do something when payment fails
    Toast.show(
        "Failure" + response.code.toString() + "" + response.message.toString(),
        context);
    //  Navigator.pop(context);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    // Do something when an external wallet is selected
    Toast.show("Success" + response.walletName, context);
    payment(orderid, response.walletName, "Online", "true", money);
    // Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(64.0),
        child: AppBar(
          automaticallyImplyLeading: true,
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              color: kMainColor,
              onPressed: () => Navigator.of(context).pop()),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Select Payment Method',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(color: kMainTextColor),
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                'Amount to Pay \₹ ' + money,
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    .copyWith(color: kDisabledColor),
              ),
            ],
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            color: kCardBackgroundColor,
            child: Text(
              'Online',
              style: Theme.of(context).textTheme.caption.copyWith(
                  color: kDisabledColor,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 0.67),
            ),
          ),
          BuildListTile(
            image: 'images/payment/payment_cod.png',
            text: 'Go Bunty Online Payment',
            onTap: () {
              getorder();

            },
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            color: kCardBackgroundColor,
            child: Text(
              'CASH',
              style: Theme.of(context).textTheme.caption.copyWith(
                  color: kDisabledColor,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 0.67),
            ),
          ),
          BuildListTile(
            image: 'images/payment/payment_cod.png',
            text: 'Cash on Delivery',
            onTap: () {
              if(double.parse(money) < 500){
                String orderid1 = "order_" + getRandomString(15);
                String paymentid = "pay_" + getRandomString(15);
                payment(orderid1, paymentid, "COD", "true", money);
              }
              else{
                Toast.show("Above Order 500 Pay Online", context);
              }

            },
          ),
          Expanded(
            child: Container(
              color: kCardBackgroundColor,
            ),
          )
        ],
      ),
    );
  }
}
