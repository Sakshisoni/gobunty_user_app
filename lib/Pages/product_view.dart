
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gobunty/Modals/product_cart.dart';
import 'package:gobunty/Modals/product_model.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class ProductView extends StatefulWidget {
String id;
  ProductView(this.id);
  int colorVal;

  _HomeTopTabsState createState() => _HomeTopTabsState();
}

class _HomeTopTabsState extends State<ProductView> with SingleTickerProviderStateMixin{
  TabController _tabController;
  int itemCount = 0;
  String model;
  String id, addr, fee;
  String text1 = "Add";
  List<String> list = [];
  List<Pcart> list1 = [];
  List<Product> product = [];
  String selected = "1 kg";

  ProgressDialog pr;
  int i = 0;
  String uid;
  final List<Tab> tabs = <Tab>[];
  @override
  void initState() {
    super.initState();
    getSub();


  }
  void _handleTabSelection() {
    setState(() {
      widget.colorVal=0xffff5722;
    });
  }
  void getSub() async {

    Map data1 = {
      'vendor_id': id,
    };


    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/Product/subcategory_list",
        body: data1);

    tabs.clear();
    Map data = json.decode(response.body);
    if(data['status'].toString().contains("true"))
    {

      for (var v in data['vendor_data']) {
        setState(() {
          print(v['name']);
          tabs.add(new Tab(text:v['name'],));
          print( tabs.length);
        });
        _tabController = new TabController(vsync: this, length: tabs.length);
        _tabController.addListener(_handleTabSelection);
      }

    }
    else
    {
      Toast.show("Clear Previous Cart", context);
    }

    // getData();

    print(data);

  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length:6,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            controller: _tabController,
            isScrollable: true,
            indicatorWeight: 4.0,
            indicatorColor:Color(0xffff5722),
            unselectedLabelColor: Colors.grey,
            tabs:tabs,
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: <Widget>[

          ],
        ),
      ),
    );
  }
}