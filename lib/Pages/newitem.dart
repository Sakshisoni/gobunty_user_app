import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gobunty/Components/FoodColors.dart';
import 'package:gobunty/Components/FoodConstant.dart';
import 'package:gobunty/Components/FoodWidget.dart';
import 'package:gobunty/Modals/product_cart.dart';
import 'package:gobunty/Modals/product_model.dart';
import 'package:gobunty/Pages/product_view.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:toast/toast.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:http/http.dart' as http;

class FoodDescription extends StatefulWidget {
  static String tag = '/FoodDescription';
  String id;
  String model, addr, fee;

  FoodDescription({this.id, this.model, this.addr, this.fee});
  @override
  FoodDescriptionState createState() =>
      FoodDescriptionState(id, model, addr, fee);
}

class FoodDescriptionState extends State<FoodDescription> with SingleTickerProviderStateMixin {
  int itemCount = 0;
  String model;
  String id, addr, fee;
  String text1 = "Add";
  List<String> list = [];
  List<String> subid = [];
  List<Pcart> list1 = [];
  List<Product> product = [];
  String selected = "1 kg";
  double totalprice = 0.0;
  bool isSwitchedFT = false;
  int _value = 1;

  FoodDescriptionState(this.id, this.model, this.addr, this.fee);
  ProgressDialog pr;
  int i = 0;
  String uid;
  TabController tabController;
  final List<Tab> tabs = <Tab>[];
  bool status=false;
  bool check=false;

  @override
  void initState() {
    super.initState();
    fetch();
    getSub();
    if(tabs.length>0) {
      tabController = TabController(vsync: this, length: tabs.length);
      tabController.addListener(() {
        if(tabController.indexIsChanging) {
          setState(() {
            product.clear();
            status=false;
            getData(tabController.index);
          });
        }
      });
    }
  }
  var textPrimaryColorGlobal = textPrimaryColor;
  var textSecondaryColorGlobal = textSecondaryColor;

  TextStyle primaryTextStyle({
    int size = 16,
    Color color,
    FontWeight weight = FontWeight.normal,
    String fontFamily,
    double letterSpacing,
  }) {
    return TextStyle(
      fontSize: size.toDouble(),
      color: color ?? textPrimaryColorGlobal,
      fontWeight: weight,
      fontFamily: fontFamily,
      letterSpacing: letterSpacing,
    );
  }

  TextStyle secondaryTextStyle({
    int size = 14,
    Color color,
    FontWeight weight = FontWeight.normal,
    String fontFamily,
    double letterSpacing,
  }) {
    return TextStyle(
      fontSize: size.toDouble(),
      color: color ?? textSecondaryColorGlobal,
      fontWeight: weight,
      fontFamily: fontFamily,
      letterSpacing: letterSpacing,
    );
  }
  Future<Map> fetch() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'vendor_id': id,
    };
    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/vendor/vendor_profile",
        body: data1);

    Map<String, dynamic> map = json.decode(response.body);
    var data2 = map["vendor_data"];

    print(data2);
    if(response.statusCode==200) {

      if (data2 != null) {
        if (mounted) {
          setState(() {
            check=true;
            model = data2[0]['name'].toString();
            addr = data2[0]['address'].toString();
            fee = data2[0]['image'].toString();
          });
        }
      } else {
        Toast.show("No Shop Available", context);
      }
    }
    else if (response.statusCode == 404) {
      setState(() {
        status=true;
      });

      Toast.show("No Available", context);
    }
    else {
      Toast.show("Connection Error", context);
    }


  }
  @override
  void dispose() {
    super.dispose();
  }

  bool visibility = false;
  var count = 1;

  void _changed() {
    setState(() {
      visibility = !visibility;
    });
  }
  void getSub() async {

    Map data1 = {
      'vendor_id': id,
    };
    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/Product/subcategory_list",
        body: data1);

    tabs.clear();
    Map data = json.decode(response.body);
    if (response.statusCode == 200) {
    if(data['status'].toString().contains("true"))
    {

        for (var v in data['vendor_data']) {
          setState(() {
            print(v['name']);
            tabs.add(new Tab(text:v['name'],));
            subid.add(v['subcategory_id']);
            print( tabs.length);
          });

        }

      }
    else
    {
      if(data['msg']!="Success")
        {
            Toast.show("Currently Unavailable", context);
        }
      else
        {
          // showDialog(
          //   context: context,
          //   builder: (BuildContext context) {
          //     return Dialog(
          //       shape: RoundedRectangleBorder(
          //         borderRadius: BorderRadius.circular(16),
          //       ),
          //       elevation: 0.0,
          //       backgroundColor: Colors.transparent,
          //       child: Container(
          //           decoration: new BoxDecoration(
          //             color: Colors.white,
          //             shape: BoxShape.rectangle,
          //             borderRadius: BorderRadius.circular(16),
          //             boxShadow: [
          //               BoxShadow(
          //                 color: Colors.black26,
          //                 blurRadius: 10.0,
          //                 offset: const Offset(0.0, 10.0),
          //               ),
          //             ],
          //           ),
          //           width: MediaQuery
          //               .of(context)
          //               .size
          //               .width,
          //           child: Column(
          //             mainAxisSize: MainAxisSize.min, // To make the card compact
          //             children: <Widget>[
          //               GestureDetector(
          //                 onTap: () {
          //                   // getData(tabController.index);
          //                   Navigator.pop(context);
          //                 },
          //                 child: Container(padding: EdgeInsets.all(16),
          //                     alignment: Alignment.centerRight,
          //                     child: Icon(Icons.close, color: kMainColor)),
          //               ),
          //               Text("Replace cart item?",style: TextStyle(fontSize: 16.7,
          //               color: Colors.black)),
          //               // , textColor: Colors.green, fontFamily: fontBold, fontSize: textSizeLarge),
          //               SizedBox(height: 24),
          //               // Image.asset(
          //               //   "",
          //               //   color: Colors.green,
          //               //   width: 95,
          //               //   height: 95,
          //               // ),
          //               // SizedBox(height: 24),
          //
          //               Padding(
          //                 padding: const EdgeInsets.only(left: 16, right: 16),
          //
          //                 child: Text('Your cart contains item from another Shop.Do you want to discard the selection and add this shop item.',style: TextStyle(fontSize: 12.7,
          //                     color: Colors.black)),
          //                 // fontSize: textSizeMedium, maxLine: 2, isCentered: true),
          //               ),
          //               SizedBox(height: 30),
          //               Container(
          //                   margin: EdgeInsets.all(16),
          //                   padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
          //                   decoration: new BoxDecoration(
          //                     color: kMainColor,
          //                     shape: BoxShape.rectangle,
          //                     borderRadius: BorderRadius.circular(24),
          //                   ),
          //                   alignment: Alignment.center,
          //                   child:
          //                   GestureDetector(
          //                     onTap: () {
          //                       ClearCartData();
          //                       getData(tabController.index);
          //                       Navigator.pop(context);
          //                     },
          //                     child: new Text("Ok", style: TextStyle(fontSize: 12.7,
          //                         color: Colors.white),),
          //                   )
          //
          //                 // textColor: t1_white, fontFamily: fontMedium, fontSize: textSizeNormal),
          //               )
          //             ],
          //           )
          //       ),
          //     );
          //   });
          //
          //   );
          Toast.show("Clear Previous Cart", context);
        }
      }
    }
    else if (response.statusCode == 404) {
    Toast.show("No Category Available", context);
    }
    else {
    Toast.show("Connection Error", context);
    }
    getData(0);
    setState(() {
      tabController=TabController(vsync: this,length:tabs.length);

      tabController.addListener(() {
        setState(() {
          status=false;
          product.clear();
          getData(tabController.index);
        });

      });
    });
    print(data);

  }
  //CartDataClear Api
  Future<void> ClearCartData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String UserId = pref.getString('uid');
    print(UserId);
    // pr.show();
    Map data = {
      'user_id': UserId,
    };
    print(data);

    var jsonResponse = null;
    var response = await http.post(
        'https://gobunty.com/api/Cart/remove_cart_data', body: data);

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      Toast.show("Success", context);
      setState(() {});
      print(jsonResponse);
    }
    else {
      Toast.show("No item Available", context);
      // pr.hide().whenComplete(() => Toast.show("Something Went Wrong", context));
      print('Something went wrong');
    }
  }


  // AlertDialog mAlertItem2 = AlertDialog(
  //   // backgroundColor: appStore.scaffoldBackground,
  //   title: Text("Replace Cart item?"),
  //   content: Text(
  //     "Your cart contains item from another Shop.Do you want to discard the selection and add this shop item.",
  //   ),
  //   actions: [
  //     FlatButton(
  //       child: Text(
  //         "Yes",
  //       ),
  //       onPressed: () {
  //         ClearCartData();
  //         // Navigator.of(context).pop();
  //       },
  //     ),
  //     FlatButton(
  //       child: Text("No",
  //       ),
  //       onPressed: () {
  //       },
  //     ),
  //   ],
  // );



  @override
  Widget build(BuildContext context) {
    double expandHeight = MediaQuery.of(context).size.height * 0.33;
    var width = MediaQuery.of(context).size.width;

    Widget mHeading(var value) {
      return Container(
          margin: EdgeInsets.fromLTRB(spacing_standard_new,
              spacing_standard_new, spacing_standard_new, 0),
          child: text(value, fontFamily: fontMedium, textAllCaps: true));
    }
    var mView = Container(
        height: 0.5,
        width: width,
        color: food_view_color,
        margin: EdgeInsets.only(
            top: spacing_standard_new, bottom: spacing_standard_new));

    Widget mVegOption(var value, var iconColor) {
      return Row(
        children: <Widget>[
          Image.asset("images/cart.png",
              color: iconColor, width: 18, height: 18),
          SizedBox(width: spacing_standard),
          text(value),
        ],
      );
    }

    return SafeArea(
      child: Scaffold(
        backgroundColor: food_app_background,
        body:check==true? NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: expandHeight,
                floating: true,
                forceElevated: innerBoxIsScrolled,
                pinned: true,
                titleSpacing: 0,
                backgroundColor:
                    innerBoxIsScrolled ? kMainColor : food_colorPrimary,
                actionsIconTheme: IconThemeData(opacity: 0.0),
                title: Container(
                  height: 60,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[],
                        ),
                      ],
                    ),
                  ),
                ),
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                    height: expandHeight,
                    child: CachedNetworkImage(
                        imageUrl: fee, height: expandHeight, fit: BoxFit.fill),
                  ),
                ),
              ),
            ];
          },
          body: Stack(
            children: <Widget>[
              SingleChildScrollView(
                padding: EdgeInsets.only(bottom: 100),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: width,
                      padding: EdgeInsets.all(spacing_standard_new),
                      decoration: boxDecoration(showShadow: true, radius: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          text(model,
                              fontFamily: fontMedium, fontSize: textSizeMedium),
                          mTotalRating("Rating"),
                          SizedBox(height: spacing_standard),
                          Row(
                            children: <Widget>[
                              Container(
                                decoration: gradientBoxDecoration(
                                    gradientColor1: food_color_blue_gradient1,
                                    gradientColor2: food_color_blue_gradient2),

                                padding: EdgeInsets.fromLTRB(
                                    spacing_standard, 0, spacing_standard, 0),

                                margin: EdgeInsets.only(right: spacing_middle),
                                child: text("Offer",
                                    textColor: food_white,
                                    fontSize: textSizeSmall),
                              ),
                              text("Save 15% on each night",
                                  textColor: food_textColorSecondary)
                            ],
                          ), SizedBox(height: 10.0),
                          mView,
                          Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children:<Widget>[
                                Container(
                                  child: Text('veg/Non-veg',style: TextStyle(color:kDisabledColor,fontSize:15),),
                                ),
                                Container(
                                  child: Switch(
                                    value: isSwitchedFT,
                                    onChanged: (bool value) {
                                      setState(() {
                                        isSwitchedFT = value;
                                        // saveSwitchState(value);
                                        print('Saved state is $isSwitchedFT');
                                        //switch works
                                      });
                                      print(isSwitchedFT);
                                    },
                                    activeTrackColor:kLightTextColor,
                                    activeColor: Colors.green,
                                  ),
                                ),
                              ]

                          ),

                          //  Row(
                          //   children: <Widget>[
                          //     Expanded(
                          //         child: mVegOption(
                          //             "Veg Only", food_color_green),
                          //         flex: 1),
                          //     Expanded(
                          //         child: mVegOption(
                          //             "Non-veg Only", food_color_red),
                          //         flex: 2),
                          //   ],
                          // )
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        margin: EdgeInsets.only(top: spacing_standard),
                        padding: EdgeInsets.all(spacing_standard_new),
                        decoration: boxDecoration(showShadow: true, radius: 0),
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: spacing_middle),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  text(addr, fontFamily: fontMedium),
                                  text("Est. delivery time 30 min")
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: spacing_standard),
                    SingleChildScrollView(
                        child:
                      Container(
                        decoration: boxDecoration(showShadow: true, radius: 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            mHeading("Menu"),
                            tabs.length>0?TabBar(
                              tabs: tabs,
                              controller: tabController,
                              isScrollable: true,
                              labelColor: kMainColor,
                              unselectedLabelColor: kLightTextColor,
                              indicatorPadding: EdgeInsets.symmetric(horizontal: 10.0),
                            ):SizedBox(),
                            SizedBox(height: spacing_standard_new),
                            tabs.length>0?Padding(
                          padding: const EdgeInsets.symmetric(vertical: 0),
                          child: Container(
                            height: 142* double.parse(product.length.toString()) ,
                            width: MediaQuery.of(context).size.width - 2 * spacing_standard,
                              child: TabBarView(
                                controller: tabController,
                                children: tabs.map((Tab tab){
                                  return Stack(
                                    children: <Widget>[
                                     status==true?Padding(
                                        padding: EdgeInsets.only(bottom: 10.0),
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          physics: NeverScrollableScrollPhysics(),
                                          scrollDirection: Axis.vertical,
                                          itemCount:
                                          product.length > 0 ? product.length : 0,
                                          itemBuilder: (BuildContext context, int index) {
                                            return product.length>0 ?Container(
                                                margin: EdgeInsets.only(
                                                    right: spacing_standard_new,
                                                    left: spacing_standard_new,
                                                    bottom: spacing_standard_new),
                                                child: Column(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment.spaceBetween,
                                                      //crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        ClipRRect(
                                                          borderRadius: BorderRadius.all(
                                                              Radius.circular(
                                                                  spacing_middle)),
                                                          child: CachedNetworkImage(
                                                              imageUrl:
                                                              product[index].image,
                                                              width: width * 0.2,
                                                              height: width * 0.2,
                                                              fit: BoxFit.fill),
                                                        ),
                                                        SizedBox(width: spacing_middle),
                                                        Expanded(
                                                          child: Column(
                                                            crossAxisAlignment:
                                                            CrossAxisAlignment.start,
                                                            mainAxisAlignment:
                                                            MainAxisAlignment.start,
                                                            mainAxisSize:
                                                            MainAxisSize.min,
                                                            children: <Widget>[
                                                              RichText(
                                                                text: TextSpan(
                                                                  children: [
                                                                    TextSpan(
                                                                        text: product[index].name.toString().replaceAll("\n", ""),
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                            textSizeMedium,
                                                                            color:
                                                                            food_textColorPrimary)),
                                                                  ],
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                height: 5,
                                                              ),
                                                             RichText(
                                                                text: TextSpan(
                                                                  children: [
                                                                    TextSpan(
                                                                        text: product[index].status == "0" ? "Currently Unavailable":"Available",
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                            textSizeMedium,
                                                                            color:
                                                                            food_textColorSecondary)),
                                                                  ],
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                height: 0,
                                                              ),

                                                              // DropdownButton<String>(
                                                              //   isExpanded: true,
                                                              //   value: categoryId,
                                                              //   iconSize: 30.0,
                                                              //   style: TextStyle(
                                                              //       color: kMainColor,
                                                              //       fontSize: 16.0,),
                                                              //   hint: Text('Quantity'),
                                                              //   onChanged:(String newValue){
                                                              //     setState(() {
                                                              //       categoryId = newValue;
                                                              //       // productCategoryget();
                                                              //       print(categoryId);
                                                              //     });
                                                              //   },
                                                              //   items: categoryList?.map((item) {
                                                              //     return new  DropdownMenuItem(
                                                              //       child: Text(item['name']),
                                                              //       value: item['id'].toString(),
                                                              //     );
                                                              //   })?.toList()??[],
                                                              // ),
                                                            // ),]
                                                            //   ),

                                                              product[index].status == "1"? Row(
                                                                mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                                children: <Widget>[
                                                                  text('\₹ ' +
                                                                      product[index]
                                                                          .price),

                                                               StrikeThroughWidget(
                                                                  child :  text('\₹ ' +
                                                                      product[index]
                                                                          .disprice),
                                                                  ),

                                                                  product[index].add!=""?Visibility(
                                                                    visible: product[
                                                                    index]
                                                                        .add
                                                                        .toString()
                                                                        .contains(
                                                                        "Add")
                                                                        ? false
                                                                        : true,
                                                                    child: Container(
                                                                      height:
                                                                      width * 0.08,
                                                                      alignment: Alignment
                                                                          .center,
                                                                      width: width * 0.23,
                                                                      decoration: boxDecoration(
                                                                          color:
                                                                          food_textColorPrimary,
                                                                          radius:
                                                                          spacing_control),
                                                                      child: Row(
                                                                        mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceBetween,
                                                                        children: <
                                                                            Widget>[
                                                                          Container(
                                                                            width: width *
                                                                                0.08,
                                                                            height:
                                                                            width *
                                                                                0.08,
                                                                            decoration: BoxDecoration(
                                                                                color: Colors
                                                                                    .red,
                                                                                borderRadius: BorderRadius.only(
                                                                                    bottomLeft: Radius.circular(
                                                                                        spacing_control),
                                                                                    topLeft:
                                                                                    Radius.circular(spacing_control))),
                                                                            child: IconButton(icon: Icon(
                                                                                Icons.remove,
                                                                                color: food_white,
                                                                                size: 10),
                                                                              onPressed: () {
                                                                                setState(() {
                                                                                  if (int.parse(product[index].add) > 1) {
                                                                                    count = int.parse(product[index].add) - 1;
                                                                                    product[index].add = count.toString();
                                                                                    updateCart(product[index].id, product[index].add, product[index].price);
                                                                                    itemCount--;
                                                                                    totalprice -= double.parse(product[index].price.toString());

                                                                                    print("new1");
                                                                                  } else {
                                                                                    itemCount--;
                                                                                    totalprice -= double.parse(product[index].price.toString());

                                                                                    product[index].add = "Add";
                                                                                    removeCart(product[index].id, index, product[index].add);
                                                                                    print("new2");
                                                                                  }
                                                                                });
                                                                              },
                                                                            ),
                                                                          ),
                                                                          text(product[
                                                                          index]
                                                                              .add),
                                                                          Container(
                                                                            width: width *
                                                                                0.08,
                                                                            height:
                                                                            width *
                                                                                0.08,
                                                                            alignment:
                                                                            Alignment
                                                                                .center,
                                                                            decoration: BoxDecoration(
                                                                                color: Colors
                                                                                    .blue,
                                                                                borderRadius: BorderRadius.only(
                                                                                    bottomRight: Radius.circular(
                                                                                        spacing_control),
                                                                                    topRight:
                                                                                    Radius.circular(spacing_control))),
                                                                            child:
                                                                            IconButton(
                                                                              icon: Icon(
                                                                                  Icons
                                                                                      .add,
                                                                                  color:
                                                                                  food_white,
                                                                                  size:
                                                                                  10),
                                                                              onPressed:
                                                                                  () {
                                                                                setState(
                                                                                        () {
                                                                                      count = int.parse(product[index].add) + 1;

                                                                                      product[index].add = count.toString();

                                                                                      updateCart(
                                                                                          product[index].id,
                                                                                          product[index].add,
                                                                                          product[index].price);
                                                                                      itemCount++;
                                                                                      totalprice += double.parse(product[index].price.toString());

                                                                                      // totalprice++;
                                                                                      print("new");
                                                                                    });
                                                                              },
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    replacement:
                                                                    GestureDetector(
                                                                      onTap: () {
                                                                        setState(() {

                                                                          product[index].add = "1";
                                                                          list1.add(new Pcart(product[index].id, product[index].price, product[index].add, uid));
                                                                          addCart(product[index].id, i, product[index].add,product[index].price);
                                                                          itemCount++;
                                                                          totalprice += double.parse(product[index].price.toString());
                                                                          }
                                                                        );
                                                                      },
                                                                      child: Container(
                                                                        width: width * 0.22,
                                                                        height: width * 0.08,
                                                                        decoration: boxDecoration(
                                                                            color:
                                                                            Colors.red,
                                                                            radius:
                                                                            spacing_control),
                                                                        alignment:
                                                                        Alignment
                                                                            .center,
                                                                        child: text(
                                                                            product[index]
                                                                                .add,
                                                                            textColor: Colors.blue,
                                                                            isCentered:
                                                                            true),
                                                                      ),
                                                                    ),
                                                                  ):SizedBox(),
                                                                ],
                                                              ):SizedBox(),
                                                            ],
                                                          ),
                                                        ),

                                                        // Quantitybtn()
                                                      ],
                                                    ),
                                                    mView,
                                                  ],
                                                )):Shimmer.fromColors(
                                                baseColor: Colors.grey[300],
                                                highlightColor: Colors.grey[100],
                                                enabled: true,
                                                child:Row(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      width: 48.0,
                                                      height: 48.0,
                                                      color: Colors.white,
                                                    ),
                                                    const Padding(
                                                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                                                    ),
                                                    Expanded(
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: <Widget>[
                                                          Container(
                                                            width: double.infinity,
                                                            height: 8.0,
                                                            color: Colors.white,
                                                          ),
                                                          const Padding(
                                                            padding: EdgeInsets.symmetric(vertical: 2.0),
                                                          ),
                                                          Container(
                                                            width: double.infinity,
                                                            height: 8.0,
                                                            color: Colors.white,
                                                          ),
                                                          const Padding(
                                                            padding: EdgeInsets.symmetric(vertical: 2.0),
                                                          ),
                                                          Container(
                                                            width: 40.0,
                                                            height: 8.0,
                                                            color: Colors.white,
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                            );
                                          },
                                        ),
                                      ):Center(child: CircularProgressIndicator(),),
                                    ],
                                  );
                                }).toList(),

                              ),
                            ),):SizedBox(),

                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  //height: width * 0.38,
                  child: Column(
                    children: <Widget>[
                      mBottom(
                          context,
                          food_color_green_gradient1,
                          food_color_green_gradient2,
                          "View Cart",
                          totalprice.toString(),
                          itemCount.toString()),
                    ],
                  ),
                ),
              )
            ],
          ),
        ) :Center(child: CircularProgressIndicator(),),
      ),)
       ;
  }

  Future<Map> addCart(String pid, int i, String add, String price) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'product_id': pid,
      'quantity': add,
      'price':price,
    };
    var jsonResponse = null;
    var response =
        await http.post("https://gobunty.com/api/cart/addtocart", body: data1);

    Map data = json.decode(response.body);

    print(data);
    if (data['msg'].toString().contains("Success")) {
      setState(() {

      });
    } else {
      pr.hide();
      Toast.show("Something Went Wrong", context);
    }
    return data;
  }

  Future<Map> updateCart(String pid, String qty, String price) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'product_id': pid,
      'qty': qty,
      'price': price,
    };

    var response = await http.post("https://gobunty.com/api/cart/update_cart",
        body: data1);

    Map data = json.decode(response.body);
    if (data['msg'].toString().contains("Success")) {
      Toast.show("updated", context);
    } else {
      Toast.show("something went wrong", context);
    }
    return data;
  }

  Future<Map> removeCart(String pid, int i, String add) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'product_id': pid,
    };

    var response = await http.post("https://gobunty.com/api/cart/remove_cart",
        body: data1);

    Map data = json.decode(response.body);
    if (data['msg'].toString().contains("Success")) {
      setState(() {});
    } else {
      pr.hide();
      Toast.show("Something Went Wrong", context);
    }
    return data;
  }

  Future<Map> getCart() async {
    String ip;
    int k;
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
    };
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/cart/fetch_cartproduct", body: data1);

    Map data = json.decode(response.body);
    print(data['status']);
    if (data['status'].toString().contains("true")) {
      for (int i = 0; i < data['Product_data'].length; i++) {
        setState(() {});
        //list.add("1 kg");
      }
      setState(() {
        itemCount = data['Product_data'].length;
      });
    }
    return data;
  }

  int itemcount=0;
  Future<Map> getData(int index) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    print(subid);
    uid = pref.getString("uid");
    Map data1 = {
      'user_id': pref.getString("uid"),
      'vendor_id': id,
      'subcategory_id':subid[index],
    };
    var jsonResponse = null;
    var response = await http
        .post("https://gobunty.com/api/product/product_list", body: data1);
    product.clear();
    Map data = json.decode(response.body);
     if (response.statusCode == 200) {

    if (data['status'].toString().contains("true")) {
      for (var v in data["Product_data"]) {
        setState(() {
          product.add(new Product(
              v['id'],
              v['name'],
              v['description'],
              v['image'],
              v['unit'],
              v['price'],
              v['discount_price'],
              v['add'],
              v['status']             //status 1 = product available, status 0 == product unavailable
              ));
          if (v['add'] != "Add") {
          }
        });
      }
      setState(() {
        status=true;
        itemCount = int.parse(data['count'].toString());
        totalprice = double.parse(data['price'].toString());

       //Add priceeeeee
        });

    } else {
      if(data['msg']!="Success")
        {

        }
      else {
        for (var v in data["Product_data"]) {
          setState(() {
            product.add(new Product(
                v['id'],
                v['name'],
                v['description'],
                v['image'],
                v['unit'],
                v['price'],
                v['discount_price'],
                v['status'],             //status 1 = product available, status 0 == product unavailable
                ""));
            if (v['add'] != "Add") {
              /*list1.add(new Pcart(
                v['id'], v['price'], v['add'], pref.getString("uid")));*/

            }
          });
        }
        setState(() {
          status=true;
          itemCount = int.parse(data['count'].toString());
        });

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                elevation: 0.0,
                backgroundColor: Colors.transparent,
                child: Container(
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(16),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 10.0,
                          offset: const Offset(0.0, 10.0),
                        ),
                      ],
                    ),
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    child: Column(
                      mainAxisSize: MainAxisSize.min, // To make the card compact
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            // getData(tabController.index);
                            Navigator.pop(context);
                          },
                          child: Container(padding: EdgeInsets.all(16),
                              alignment: Alignment.centerRight,
                              child: Icon(Icons.close, color: kMainColor)),
                        ),
                        Text("Replace cart item?",style: TextStyle(fontSize: 16.7,
                            color: Colors.black)),
                        // , textColor: Colors.green, fontFamily: fontBold, fontSize: textSizeLarge),
                        SizedBox(height: 24),
                        // Image.asset(
                        //   "",
                        //   color: Colors.green,
                        //   width: 95,
                        //   height: 95,
                        // ),
                        // SizedBox(height: 24),

                        Padding(
                          padding: const EdgeInsets.only(left: 16, right: 16),

                          child: Text('Your cart contains item from another Shop.Do you want to discard the selection and add this shop item.',style: TextStyle(fontSize: 12.7,
                            color: Colors.black)),

                        ),

                        SizedBox(height: 30),
                        Container(
                            margin: EdgeInsets.all(16),
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                            decoration: new BoxDecoration(
                              color: kMainColor,
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(24),
                            ),
                            alignment: Alignment.center,
                            child:
                            GestureDetector(
                              onTap: () {
                                ClearCartData();
                                getData(tabController.index);
                                Navigator.pop(context);
                              },
                              child: new Text("Ok" , style: TextStyle(fontSize: 12.7,
                                color: Colors.white),

                              )

                          // textColor: t1_white, fontFamily: fontMedium, fontSize: textSizeNormal),
                           )
                        ),
                      ],
                    )
                ),
              );
            });
        // showDialog(
        //     context: context,
        //     builder: (BuildContext context) => CustomDialog(),
        //     // builder: (BuildContext context) {
        //     //   return mAlertItem2;
        //     // },
        //   );

        // Toast.show("Clear Previous Cart", context);
         }
       }
     }
    else if (response.statusCode == 404) {
    Toast.show("No Product Available", context);
    }
    else {
    Toast.show("Connection Error", context);
    }
    print(data);
    print(product);
    print(response.statusCode);
    return data;
  }

  ///Quantity Api
  List categoryList;
  String categoryId;

  Categoryget() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String UserId = pref.getString('uid');
    print (UserId);
    // pr.show();
    Map data = {
      'vendor_id': UserId,
    };
    print(data);

    var jsonResponse = null;
    var response = await http.post('https://gobunty.com/api/Product/all_category_data',body : data);

    if(response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      setState(() {
        categoryList = jsonResponse['categories_data'];
        // categoryId =  jsonResponse['categories_data']['id'];
      }
      );
      setState(() {});
      print(jsonResponse);
    }
    else {
      Toast.show("No Category Available", context);
      // pr.hide().whenComplete(() => Toast.show("Something Went Wrong", context));
      print('Something went wrong');
    }
  }


}

class StrikeThroughWidget extends StatelessWidget {
  final Widget _child;

  StrikeThroughWidget({Key key, @required Widget child})
      : this._child = child,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _child,
      padding: EdgeInsets.symmetric(horizontal: 8), // this line is optional to make strikethrough effect outside a text
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('images/mark.png'), fit: BoxFit.fitWidth),
      ),
    );
  }
}

// class CustomDialog extends StatefulWidget {
//
//   @override
//   _CustomDialogState createState() => _CustomDialogState();
// }
//
// class _CustomDialogState extends State<CustomDialog> {
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   Widget text(var text,
//       {var fontSize = textSizeLargeMedium,
//         textColor =t1_textColorSecondary,
//         var fontFamily = fontRegular,
//         var isCentered = false,
//         var maxLine = 1,
//         var latterSpacing = 0.5}) {
//     return Text(
//       text,
//       textAlign: isCentered ? TextAlign.center : TextAlign.start,
//       maxLines: maxLine,
//       style: TextStyle(
//           fontFamily: fontFamily,
//           fontSize: fontSize,
//           color: textColor,
//           height: 1.5,
//           letterSpacing: latterSpacing),
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Dialog(
//       shape: RoundedRectangleBorder(
//         borderRadius: BorderRadius.circular(16),
//       ),
//       elevation: 0.0,
//       backgroundColor: Colors.transparent,
//       child: dialogContent(context),
//     );
//   }
//
//   dialogContent(BuildContext context) {
//     return Container(
//         decoration: new BoxDecoration(
//           color: Colors.white,
//           shape: BoxShape.rectangle,
//           borderRadius: BorderRadius.circular(16),
//           boxShadow: [
//             BoxShadow(
//               color: Colors.black26,
//               blurRadius: 10.0,
//               offset: const Offset(0.0, 10.0),
//             ),
//           ],
//         ),
//         width: MediaQuery
//             .of(context)
//             .size
//             .width,
//         child: Column(
//           mainAxisSize: MainAxisSize.min, // To make the card compact
//           children: <Widget>[
//             GestureDetector(
//               onTap: () {
//                 Navigator.pop(context);
//               },
//               child: Container(padding: EdgeInsets.all(16),
//                   alignment: Alignment.centerRight,
//                   child: Icon(Icons.close, color: kMainColor)),
//             ),
//             Text("Replace cart item?"),
//             // , textColor: Colors.green, fontFamily: fontBold, fontSize: textSizeLarge),
//             SizedBox(height: 24),
//             // Image.asset(
//             //   "",
//             //   color: Colors.green,
//             //   width: 95,
//             //   height: 95,
//             // ),
//             // SizedBox(height: 24),
//
//             Padding(
//               padding: const EdgeInsets.only(left: 16, right: 16),
//
//               child: Text('Your cart contains item from another Shop.Do you want to discard the selection and add this shop item.'),
//               // fontSize: textSizeMedium, maxLine: 2, isCentered: true),
//             ),
//             SizedBox(height: 30),
//             Container(
//                 margin: EdgeInsets.all(16),
//                 padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
//                 decoration: new BoxDecoration(
//                   color: kMainColor,
//                   shape: BoxShape.rectangle,
//                   borderRadius: BorderRadius.circular(24),
//                 ),
//                 alignment: Alignment.center,
//                 child:
//                 GestureDetector(
//                   onTap: () {
//                     ClearCartData();
//                     // getData(0);
//                     Navigator.pop(context);
//                   },
//                   child: new Text("Ok"),
//                 )
//
//               // textColor: t1_white, fontFamily: fontMedium, fontSize: textSizeNormal),
//             )
//           ],
//         )
//     );
//   }
//
//   ///CartDataClear Api
//   Future<void> ClearCartData() async {
//     SharedPreferences pref = await SharedPreferences.getInstance();
//     String UserId = pref.getString('uid');
//     print(UserId);
//     // pr.show();
//     Map data = {
//       'user_id': UserId,
//     };
//     print(data);
//
//     var jsonResponse = null;
//     var response = await http.post(
//         'https://gobunty.com/api/Cart/remove_cart_data', body: data);
//
//     if (response.statusCode == 200) {
//       jsonResponse = json.decode(response.body);
//       Toast.show("Success", context);
//       setState(() {});
//       print(jsonResponse);
//     }
//     else {
//       Toast.show("No item Available", context);
//       // pr.hide().whenComplete(() => Toast.show("Something Went Wrong", context));
//       print('Something went wrong');
//     }
//   }


// }
