import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class TncPage extends StatelessWidget {
  ProgressDialog pr;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kMainColor,
        titleSpacing: 0.0,
        title: Text('Terms & Conditions',
            style: TextStyle(fontSize: 16.7,
          color: Colors.white
          ),),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(48.0),
                color: kCardBackgroundColor,
                child: Image(
                  image:
                      AssetImage("images/logos/logo_user.png"), //gobunty logo
                  height: 130.0,
                  width: 99.7,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 28.0, horizontal: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Terms of use',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(
                      "\nThis Terms and Conditions Agreement sets forth the standards of use of the Food Delivery service, (“GoBunty Pvt Ltd”) Online Service. By using the www.GoBunty.com website and GoBunty app, you (the ”User”) agree to these terms and conditions. If you do not agree to the terms and conditions of this Agreement, you should immediately cease all usage of this website. We reserve the right, at any time, to modify, alter, or update the terms and conditions of this agreement without prior notice. Modifications shall become effective immediately upon being posted at www.GoBunty.com website. Your continued use of the Service after amendments are posted constitutes acknowledgement and acceptance of the Agreement and its modifications. Except as provided in this paragraph, this Agreement may not be amended.",
                       style: Theme.of(context).textTheme.caption,
                    ),
                    Text(
                      '\nServices',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(
                      "\n2.1 We are a delivery service provider providing you an online platform to (a) order food,Groceries  online from the list of restaurants, home food makers & Groceries Store  available on the Services."
                        "\n2.2 We do not own, sell, resell, furnish, provide, prepare, manage and/or control the Vendors or the related services provided in connection thereof."
                    "\n2.3 Our responsibilities are limited to: (i) facilitating the availability of the Services; and (ii) serving as the limited agent of each Vendor for the purpose of accepting payments from you for your online food orders and delivering your order.",
                       style: Theme.of(context).textTheme.caption,
                    ),
                    Text(
                      '\nRegistration',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(
                        "\n3.1 By signing up to our website you are agreeing to the terms and conditions of sale which apply to all transactions on www.Grabet.net"
                    "S\nigning up to the service means we must have the following information:"
                    "\n• Your address, including the postcode."
                    "\n• Your home telephone number or mobile telephone number. style: Theme.of(context).textTheme.caption,",
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
