import 'dart:convert';

import 'package:gobunty/Components/bottom_bar.dart';
import 'package:gobunty/Maps/UI/location_page.dart';
import 'package:gobunty/Modals/AdrModal.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:gobunty/Themes/style.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Address{
  final String icon;
  final String addressType;
  final String address;

  Address(this.icon, this.addressType, this.address);
}
class SavedAddressesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kCardBackgroundColor,
        appBar: AppBar(
          backgroundColor: kMainColor,
          titleSpacing: 0.0,
          title: Text(
            'Saved Addresses',
            style: TextStyle(fontSize: 16.7,
          color: Colors.white,
          ),
          ),
        ),
        body: SavedAddresses());
  }
}

class SavedAddresses extends StatefulWidget {
  @override
  _SavedAddressesState createState() => _SavedAddressesState();
}

class _SavedAddressesState extends State<SavedAddresses> {
  final List<Address> listOfAddressTypes = [
    Address('images/address/ic_homeblk.png', 'Home', 'New York, America'),
    Address('images/address/ic_officeblk.png', 'Office', 'Paris, France'),
    Address('images/address/ic_otherblk.png', 'Other', 'New Delhi, India'),
  ];
  List<AdrModal> list=[];
  ProgressDialog pr;
  //AddressBloc _addressBloc;
  @override
  void initState() {
    super.initState();
    fetchlocation();
//    _addressBloc = BlocProvider.of<AddressBloc>(context);
//    _addressBloc.add(FetchAddressesEvent());
  }
  Future<void> fetchlocation() async
  {
    SharedPreferences pref= await SharedPreferences.getInstance();
    Map data = {

      'user_id': pref.getString('uid'),

    };
    var response = await http.post("https://gobunty.com/api/user_Delivery/fetch_delivery_address",body:data);
    if (response.statusCode == 200) {
      String responeBody = response.body;
      Map<String, dynamic> map = json.decode(response.body);
      List<dynamic> data1 = map["address_data"];


print(map);
    if(map!=null) {
      for (var data in data1) {
        if (mounted) {
          setState(() {
            list.add(new AdrModal(
                data['id'], data['address'], "", data['phone'], data['latitude'],
                data['longitude']));
          });

          pref.setString("address", list[0].address);
        }
      }
    }
    }

    else {
      print('Something went wrong');
    }
  }


  @override
  Widget build(BuildContext context) {
    return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                      child: ListView.builder(
                          itemCount: list.length,
                          itemBuilder: (BuildContext context, int index) {
                            return list.length!=null?Column(
                              children: <Widget>[
                                Divider(
                                  height: 6.0,
                                  color: kCardBackgroundColor,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 20.0, horizontal: 6.0),
                                  color: Colors.white,
                                  child: ListTile(
                                    leading:
                                              CircleAvatar(
                                              radius: 30,
                                              backgroundColor: kCardBackgroundColor,
                                              child: ImageIcon(
                                              AssetImage(listOfAddressTypes[index].icon),
                                              color: kMainColor,
                                              size: 28,
                                              ),
                                              ),
                                    title: Padding(
                                      padding: EdgeInsets.only(
                                          left: 8.0, bottom: 8.0),
                                      child: Text(
                                        list[index].type,
                                        style: listTitleTextStyle,
                                      ),
                                    ),
                                    subtitle: Padding(
                                      padding: EdgeInsets.only(left: 8.0),
                                      child: Text(
                                        list[index].address,
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption
                                            .copyWith(fontSize: 11.7),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ):Center(child: Text("No Address Not Found",
                            style: TextStyle(fontSize: 20.0,fontFamily: "OpenSans",fontWeight: FontWeight.bold),));
                          })),
              BottomBar(
                color: Colors.white,
                text: ' Edit Address',
                textColor: kMainColor,
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LocationPage(bool1:true),
                      ));
                },
              ),
            ],
          );
        }
  }
