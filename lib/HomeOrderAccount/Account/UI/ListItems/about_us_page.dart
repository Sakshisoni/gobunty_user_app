import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class AboutUsPage extends StatelessWidget {
  ProgressDialog pr;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kMainColor,
        titleSpacing: 0.0,
        title: Text('About Us' , style: TextStyle(fontSize: 16.7,
          color: Colors.white
          ),
        ),
      ), 
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(48.0),
                color: kCardBackgroundColor,
                child: Image(
                  image:
                      AssetImage("images/logos/logo_user.png"), //gobunty logo
                  height: 130.0,
                  width: 99.7,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 28.0, horizontal: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'About Go Bunty',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(
                      "\nGoBunty.com, the easiest, simple and fun way to order food, groceries, vegetables & Home Services is here. Choose from a wide variety of restaurants across Andhra Pradesh from any place you're in. Without any haste of memorizing restaurant names, menus or their contact details. No more calls on hold, busy tones, wrong deliveries due to miscommunication. With us adding smallest detail to your order, for example \"I want it shallow fried not deep\" becomes very easy. We have a fully automated model with no or very less human intervention guaranteeing a pleasant ordering experience.",
                      style: Theme.of(context).textTheme.caption,
                    ),
                    Text(
                      '\nOur Vision',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(
                      "\nPocket GoBunty with the GoBunty mobile App. Download our free mobile app and order food from 100+ restaurants & Grocery Store in Anantapuram. Connect to hundreds of restaurants and order from a variety of cuisines and food through our mobile app. Get exciting offers, view food images, add comments to your orders, earn Easy Points, place advance order and what not."
                        "\n\nAn app as good as our site. Go ahead pocket it!",
                       style: Theme.of(context).textTheme.caption,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
