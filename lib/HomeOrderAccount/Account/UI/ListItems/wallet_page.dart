import 'package:gobunty/Themes/style.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

class WalletPage extends StatefulWidget {
  @override
  _WalletPageState createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kMainColor,
        title: Text('Wallet',
             style: TextStyle(color: Colors.white),),
        titleSpacing: 0.0,
      ),
      body: Wallet(),
    );
  }
}

class Wallet extends StatefulWidget {
  @override
  _WalletState createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Walleet();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Price == null ? Center(child: CircularProgressIndicator()):ListView(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(20.0),
          child: Row(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      'AVAILABLE BALANCE',
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline6
                          .copyWith(
                          letterSpacing: 0.67,
                          color: kHintColor,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Text(
                    "₹ "+ Price,
                    style: listTitleTextStyle.copyWith(
                        fontSize: 25.0, color: kMainColor, letterSpacing: 0.18),
                  ),
                ],
              ),
              SizedBox(
                width: 40.0,
              ),
            ],
          ),
        ),
        Container(
          height: 80.0,

          padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
          color: kCardBackgroundColor,
          child: Text(
            'This Wallet Amount 50 rs Dedect purching Food Order Minimum 300. Validity in 60 Days',
            style: Theme
                .of(context)
                .textTheme
                .subtitle2
                .copyWith(
                color: kLightTextColor,
                fontWeight: FontWeight.w500,
                letterSpacing: 0.08),
          ),
        ),

      ],
    );
  }
  String Price;
  Walleet() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var user_id = sharedPreferences.getString('uid');
    Map data = {
      'user_id':user_id,
    };

    print(data);

    // pr.show();
    var jsonResponse = null;
    var response = await http.post('https://gobunty.com/api/User/wallet_list',body:data);
    print(response.statusCode);
    if(response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
        // pr.hide();
        print(jsonResponse);

        if (jsonResponse ['msg'].toString().contains("Success")) {
          for (var data in jsonResponse['user_data']) {
            setState(() {
              Price = data['price'];
            });

            print(Price);
          }
        }
      }

      setState(() {

      });
      print(jsonResponse);
    } else {
      // pr.hide().whenComplete(() =>
          Toast.show("No Amount In your Wallet", context);
      print('Something went wrong');
    }
  }
}
