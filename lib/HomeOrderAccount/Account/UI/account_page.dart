import 'dart:convert';

import 'package:gobunty/Auth/MobileNumber/UI/mobile_input.dart';
import 'package:gobunty/Auth/MobileNumber/UI/phone_number.dart';
import 'package:gobunty/Auth/login_navigator.dart';
import 'package:gobunty/Components/list_tile.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/ListItems/about_us_page.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/ListItems/saved_addresses_page.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/ListItems/tnc_page.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/ListItems/wallet_page.dart';
import 'package:gobunty/HomeOrderAccount/Account/UI/ListItems/ReferEarn.dart';
import 'package:gobunty/Modals/UserModal.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

List<UserModal> _user = [];

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Account', style: Theme.of(context).textTheme.bodyText1),
        centerTitle: true,
      ),
      body: Account(),
    );
  }
}

class Account extends StatefulWidget {
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  String number;
  //AccountBloc _accountBloc;
  String name = "", mobile = "", email = "";
  @override
  void initState() {
    super.initState();

    getUserDetails();

    // _accountBloc = BlocProvider.of<AccountBloc>(context);
  }

  getlogout() async {
    SharedPreferences pref = await SharedPreferences.getInstance();

    /*   Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => LoginNavigator(),
        ));*/
    if (mounted) {
      setState(() {
        pref.clear();
        Navigator.pop(context);
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PhoneNumber(),
            ));
      });
    }
  }

  Future<void> getUserDetails() async {
    SharedPreferences pref = await SharedPreferences.getInstance();

    Map data2 = {
      'mobile_no': pref.getString('token'),
    };
    var response =
        await http.post("https://gobunty.com/api/user/user_list", body: data2);
    if (response.statusCode == 200) {
      Map<String, dynamic> map = json.decode(response.body);
      var data1 = map["user_data"];
      //var data = jsonBody['user_data'];
      print(data1['mobile']);
      if (mounted) {
        setState(() {
          name = data1['name'];
          mobile = data1['mobile'];
          email = data1['email'];
        });
      }

      print(name);
    } else {
      print('Something went wrong');
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        //getUserDetails(),
        //getUserDetails(),
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('\n' + name, style: Theme.of(context).textTheme.bodyText1),
              Text('\n' + mobile,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(color: Color(0xff9a9a9a))),
              SizedBox(
                height: 5.0,
              ),
              Text(email,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(color: Color(0xff9a9a9a))),
            ],
          ),
        ),
        Divider(
          color: kCardBackgroundColor,
          thickness: 8.0,
        ),
        AddressTile(),
        BuildListTile(
            image: 'images/wallet.png',
            text: 'Wallet',
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>WalletPage(),
                  ));
            }),
        BuildListTile(
            image: 'images/account/ic_menu_tncact.png',
            text: 'Terms & Conditions',
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TncPage(),
                ))),
        BuildListTile(
            image: 'images/account/ic_menu_supportact.png',
            text: 'Support',
            onTap: () {}),
        BuildListTile(
          image: 'images/account/ic_menu_aboutact.png',
          text: 'About us',
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AboutUsPage(),
              )),
        ), BuildListTile(
          image: 'images/account/refer.png',
          text: 'Refer & Earn',
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => QIBusReferEarn(),
              )),
        ),
        Divider(
          color: Colors.black,
          thickness: 2.0,
        ),
        BuildListTile(
          image: 'images/account/ic_menu_logoutact.png',
          text: 'Logout',
          onTap: () {
            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('Logging out'),
                    content: Text('Are you sure?'),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('No'),
                        textColor: kMainColor,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: kTransparentColor)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      FlatButton(
                          child: Text('Yes'),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: kTransparentColor)),
                          textColor: kMainColor,
                          onPressed: () {
                            getlogout();
                            Phoenix.rebirth(context);
                          })
                    ],
                  );
                });
          },
        ),
        BuildListTile(
          image: 'images/star.png',
          text: 'Rate us on the Play Store',
          onTap: () async {
            // await launch("playstore.com");
          },
        ),
      ],
    );
  }
}

class AddressTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BuildListTile(
        image: 'images/account/ic_menu_addressact.png',
        text: 'Saved Addresses',
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SavedAddressesPage(),
              ));
        });
  }
}
