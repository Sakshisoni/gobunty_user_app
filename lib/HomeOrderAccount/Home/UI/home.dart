import 'dart:convert';

import 'package:geocoder/geocoder.dart';
import 'package:gobunty/Components/FoodColors.dart';
import 'package:gobunty/Components/card_content.dart';
import 'package:gobunty/Components/custom_appbar.dart';
import 'package:gobunty/Components/reusable_card.dart';
import 'package:gobunty/Components/search_bar.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/Stores/stores.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/custom_delivery.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/search.dart';
import 'package:gobunty/Maps/UI/location_page.dart';
import 'package:gobunty/Modals/AdrModal.dart';
import 'package:gobunty/Pages/view_cart.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:gobunty/Themes/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:marquee/marquee.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/Stores/addItem.dart';
import 'package:location/location.dart';
// import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:http/http.dart' as http;
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Home();
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String value = 'Set Location';
  List<String> add=["Set Location"];
  List<AdrModal> list=[];
  ProgressDialog pr;
  Location location = new Location();
  PermissionStatus _permission;
  String address="";

  LocationData locationData;

  getinfo() async
  {
    var whatsappUrl ="whatsapp://send?phone=919381117607&text=hi";

    await launch(whatsappUrl);
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

      _getLocationPermission();
      fetchlocation();


  }
  void fetchlocation() async
  {
    add.clear();
    add.add("Set Location");
    SharedPreferences pref= await SharedPreferences.getInstance();
    Map data = {

      'user_id': pref.getString('uid'),

    };
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/user_Delivery/fetch_delivery_address",body:data);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.toString());

      print(jsonResponse);
      Map<String, dynamic> map = json.decode(response.body);
      List<dynamic> data1 = jsonResponse["address_data"];
      if(jsonResponse==null) {}
      else {
        if(jsonResponse['msg']=="Success")
          {
            for(var value1 in data1) {
              if (mounted) {
                setState(() {
                  add.add(value1['phone'].toString());
                  if (value1['mobile']
                      .toString()=="1") {
                    value = "Home";
                  }
                });
              }
            }
        }

      }
      }
     else {
      print('Something went wrong');
    }
     pr.hide();
  }
  void getstreet(String lat, String lon) async {
    final coordinates = new Coordinates(
        double.parse(lat.toString()), double.parse(lon.toString()));
    var addresses =
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;

    setState(() {
      address = "${first.addressLine}";
    });
  }

  void _getLocationPermission() async {
    bool enable;
    enable = await location.serviceEnabled();
    if (!enable) {
      enable = await location.requestService();
      if (!enable) {
        return;
      }
    }
    _permission = await location.hasPermission();
    if (_permission == PermissionStatus.DENIED) {
      _permission = await location.requestPermission();
      if (_permission != PermissionStatus.GRANTED) {
        return;
      }
    }
    locationData = await location.getLocation();
    print(locationData.toString());
    setState(() {
      getstreet(
          locationData.latitude.toString(), locationData.longitude.toString());
    });

    /* try {
      location.requestPermission();
    } on Exception catch (_) {
      print('There was a problem allowing location access');
    }*/
  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true,isDismissible: false);
    pr.style(message: 'Please wait...');

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(126.0),
        child: CustomAppBar(
          leading: Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Icon(
              Icons.location_on,
              color: kMainColor,
            ),
          ),
          titleWidget: DropdownButton(
            value: value,
            icon: Icon(
              Icons.keyboard_arrow_down,
              color: kMainColor,
            ),
            iconSize: 24.0,
            elevation: 16,
            style: inputTextStyle.copyWith(fontWeight: FontWeight.bold),
            underline: Container(
              height: 0,
            ),

            items: add.map((String address) {
              return DropdownMenuItem<String>(
                value: address,
                child: Text(
                  address,
                  overflow: TextOverflow.ellipsis,
                  style:
                  TextStyle(fontWeight: FontWeight.w400,
                    fontFamily: 'OpenSans',fontSize: 16.0 ),
                ),
              );
            }).toList(),
            onChanged: (String newValue) {
              setState(() {
                value = newValue;

              });
              if (value == 'Set Location')


              Navigator.push(
                  context,
                  MaterialPageRoute(
                  builder: (context) => LocationPage(bool1:false)));
            },
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 6.0),
              child: IconButton(
                icon: ImageIcon(
                  AssetImage('images/notification.png'),
                  color: kMainColor,
                ),
                onPressed: () {
                  Toast.show("Not Available",context);
                }
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 6.0),
              child: IconButton(
                icon: ImageIcon(
                  AssetImage('images/icons/ic_cart blk.png'),
                  color: kMainColor,
                ),
                onPressed:()
                  {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ViewCart(add:"Home",promoid: "1",title: "Select Code",value: false)));

                  }
              ),
            ),
          ],
          hint: 'Search item or store',
          onTap: (){

            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SearchPage()));
          },
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                automaticallyImplyLeading: false,
                title: Container(
                  /*child: ListView(
                    children: <Widget>[
                      Card(
                        elevation: 5,
                        child: Container(
                          child: Text("Praveen555"),
                        ),
                      )
                    ],
                  ),*/
                  margin: EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 7,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white70,
                    borderRadius: BorderRadius.all(Radius.circular(22)),
                  ),
                ),
                backgroundColor: Colors.white,
                expandedHeight: 180,
                flexibleSpace: FlexibleSpaceBar(
                  background: new SizedBox(
                    height: 70,
                    width: double.infinity,
                    child: Carousel(
                      boxFit: BoxFit.fill,
                      images: [
                        AssetImage('images/logos/foods.png'),
                        AssetImage('images/logos/grocery.png'),
                        AssetImage('images/logos/home_service.png'),
                        AssetImage('images/logos/vegetables.png'),
                        // AssetImage('images/logos/.jpg'),
                      ],
                      dotBgColor: Colors.transparent,
                      animationDuration: Duration(milliseconds: 700),
                    ),
                  ),
                ),
              )
            ];
          },
          body: Column(
            children: <Widget>[
              Container(
                height: 40,
                color: food_textColorPrimary,
                child: Marquee(
                text: 'Fruits,Vegetables and Grocery needs Next day delivery on your order.',
                style: TextStyle(fontWeight: FontWeight.w300,fontFamily: "OpenSans",color: Colors.white),
                scrollAxis: Axis.horizontal,
                crossAxisAlignment: CrossAxisAlignment.center,
                blankSpace: 20.0,
                velocity: 100.0,
                numberOfRounds: 10,
                pauseAfterRound: Duration(seconds: 1),
                showFadingOnlyWhenScrolling: true,
                fadingEdgeStartFraction: 0.1,
                fadingEdgeEndFraction: 0.1,
                startPadding: 10.0,
                accelerationDuration: Duration(seconds: 1),
                accelerationCurve: Curves.linear,
                decelerationDuration: Duration(milliseconds: 300),
                decelerationCurve: Curves.easeOut,
              ),),
              Padding(
                padding: EdgeInsets.only(top: 16.0, left: 24.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Mana Anantapuram",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12.0,
                        fontWeight: FontWeight.w800,
                        fontFamily: 'OpenSans',
                      ),
                      // style: Theme.of(context).textTheme.bodyText1 .copyWith(fontWeight: FontWeight.normal),
                    ),
                    SizedBox(
                      width: 3.0,
                    ),
                    Text(
                      "City App",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12.0,
                        fontWeight: FontWeight.w800,
                        fontFamily: 'OpenSans',
                      ),
                      // style: Theme.of(context)
                      //     .textTheme
                      //     .bodyText1
                      //     .copyWith(fontWeight: FontWeight.normal),
                    ),
                  ],
                ),

              ),
               buildGrid(),
            ],
          ),
        ),
      ),
    );

  }

  Expanded buildGrid() {

    return Expanded(
      child: Container(
        color: Colors.white70,
        margin: EdgeInsets.all(7.0),
        child:SingleChildScrollView(
          child:Column(
            children: <Widget>[
            Padding(padding: EdgeInsets.all(10.0),child:RaisedButton.icon(
                color: kMainColor,
                disabledColor: kMainColor,
                onPressed: (){
                  getinfo();
                },
                label: Text("Chat with Bunty",
                  style: TextStyle(fontSize: 12.0,color: Colors.white),
                ),
                icon: Icon(Icons.message,
                color: Colors.white,),
              ),),
              GridView.count(
          crossAxisCount: 3,
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
          controller: ScrollController(keepScrollOffset: false),
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          children: <Widget>[

            Card(
              color: Colors.white,
              shadowColor: kWhiteColor,
              elevation: 3,
              child: InkWell(
                onTap: (){
                 // Toast.show("Currently Not Available",context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                      builder: (context) => StoresPage('Food & Meals', "Food Delivery")));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('images/maincategory/food.png'),
                      height: 48.0,
                      width: 48.0,
                    ),
                    Padding(padding: EdgeInsets.all(3.0)),
                    Text("FOOD",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 10.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans',
                      ),
                    )
                  ],
                ),
              ),
            ),
               Card(
                color: Colors.white,
                shadowColor: kWhiteColor,
                elevation: 3,
                child: InkWell(
                 onTap: (){
                  //Toast.show("Currently Not Available",context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                      builder: (context) => StoresPage('Grocery', "Groceries")));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('images/maincategory/grocery.png'),
                      height: 48.0,
                      width: 48.0,
                    ),
                    Padding(padding: EdgeInsets.all(3.0)),
                    Text("GROCERY",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 10.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans',
                      ),
                    )
                  ],
                ),
              ),
            ),
              Card(
              color: Colors.white,
              shadowColor: kWhiteColor,
              elevation: 3,
              child: InkWell(
                onTap: (){
                  //Toast.show("Currently Not Available",context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => StoresPage('Vegetables & Fruits', 'Vegetables')));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('images/maincategory/vegetable.png'),
                      height: 48.0,
                      width: 48.0,
                    ),
                    Padding(padding: EdgeInsets.all(3.0)),
                    Text("VEGETABLES",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 10.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans',
                      ),
                    )
                  ],
                ),
              ),
            ),
              Card(
              color: Colors.white,
              shadowColor: kWhiteColor,
              elevation: 3,
              child: InkWell(
                onTap: (){
                  // Toast.show("Currently Not Available",context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddItem()));
                          // builder: (context) => AddItem('Pharmacy', "Medicines")));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('images/maincategory/medicine.png'),
                      height: 48.0,
                      width: 48.0,
                    ),
                    Padding(padding: EdgeInsets.all(3.0)),
                    Text("PHARMACY",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 10.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans',
                      ),
                    )
                  ],
                ),
              ),
            ),
              Card(
              color: Colors.white,
              shadowColor: kWhiteColor,
              elevation: 3,
              child: InkWell(
                onTap: (){


                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => StoresPage('Crackers', "Crackers")
                      ));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('images/maincategory/cracker.png'),
                      height: 48.0,
                      width: 48.0,
                    ),
                    Padding(padding: EdgeInsets.all(3.0)),
                    Text("CRACKERS",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 10.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans',
                      ),
                    )
                  ],
                ),
              ),
            ),
                Card(
                color: Colors.white,
                shadowColor: kWhiteColor,
                elevation: 3,
                child: InkWell(
                onTap: (){
                  /*Scaffold.of(context)
                      .showSnackBar(SnackBar(
                    content: Text("Currently Not Availble"),
                  ));*/
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CustomDeliveryPage(
                              'Custom Delivery', 'Home Services')));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('images/maincategory/house.png'),
                      height: 48.0,
                      width: 48.0,
                    ),
                    Padding(padding: EdgeInsets.all(3.0)),
                    Center(
                      child: Text("Home Service",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 10.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'OpenSans',
                        ),
                    ),),
                  ],
                ),
              ),
            ),
            Card(
              color: Colors.white,
              shadowColor: kWhiteColor,
              elevation: 3,
              child: InkWell(
                onTap: (){
                  /*Scaffold.of(context)
                      .showSnackBar(SnackBar(
                    content: Text("Currently Not Availble"),
                  ));*/
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CustomDeliveryPage(
                              'Car Rental', 'Car Rental')));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('images/maincategory/car-service.png'),
                      height: 48.0,
                      width: 48.0,
                    ),
                    Padding(padding: EdgeInsets.all(3.0)),
                    Center(
                      child: Text("Car Rental",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 10.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'OpenSans',
                        ),
                      ),),
                  ],
                ),
              ),
            ),

          ],
        ),
              Divider(
                color: Colors.white,
                thickness: 3.0,
              ),
              Column(
                children: <Widget>[
                  Card(
                    color: Colors.white,
                    shadowColor: kWhiteColor,
                    margin: EdgeInsets.only(left: 12.0, right: 12.0, top: 10.0),
                    elevation: 2,
                    child: Container(
                      height: 80.0,
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                                top: 16.0,
                                left: 16.0,
                                right: 16.0,
                                bottom: 16.0),
                            child: Container(
                              height: 100.0,
                              width: 70.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                  image: DecorationImage(
                                    fit: BoxFit.scaleDown,
                                    image: AssetImage("images/cube.png"),
                                  )),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.all(10.0),
                              child: Center(
                                child: Text(
                                  "Get things delivered to your doorstep instantly",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: "OpenSans",
                                      fontWeight: FontWeight.w700,
                                      color: Colors.black,
                                      fontSize: 11.0),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Card(
                    color: Colors.white,
                    shadowColor: kWhiteColor,
                    margin: EdgeInsets.only(left: 12.0, right: 12.0, top: 10.0),
                    elevation: 2,
                    child: Container(
                      height: 80.0,
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                                top: 16.0,
                                left: 16.0,
                                right: 16.0,
                                bottom: 16.0),
                            child: Container(
                              height: 100.0,
                              width: 70.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                  image: DecorationImage(
                                    fit: BoxFit.scaleDown,
                                    image: AssetImage("images/home.png"),
                                  )),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.all(10.0),
                              child: Center(
                                child: Text(
                                  "Delivery from anywhere in city",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "OpenSans",
                                      color: Colors.black,
                                      fontSize: 11.0),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Card(
                    color: Colors.white,
                    shadowColor: kWhiteColor,
                    margin: EdgeInsets.only(left: 12.0, right: 12.0, top: 10.0),
                    elevation: 2,
                    child: Container(
                      height: 80.0,
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                                top: 16.0,
                                left: 16.0,
                                right: 16.0,
                                bottom: 16.0),
                            child: Container(
                              height: 100.0,
                              width: 70.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                  image: DecorationImage(
                                    fit: BoxFit.scaleDown,
                                    image: AssetImage("images/cart.png"),
                                  )),
                              ),
                          ),
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.all(10.0),
                              child: Center(
                                child: Text(
                                  "No minimum order value on your purchase",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      color: Colors.black,
                                      fontSize: 11.0),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(bottom: 20.0))
      ],),),
      ),
    );

    Container(
      child: ListView(
        children: <Widget>[
          Card(
            elevation: 5,
            child: Text("PRaveen"),
          )
        ],
      ),
    );
  }
}



