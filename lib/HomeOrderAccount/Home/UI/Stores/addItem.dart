import 'dart:convert';
import 'package:gobunty/Components/bottom_bar.dart';
import 'package:gobunty/Components/entry_field.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class AddItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Pharmacy', style: Theme.of(context).textTheme.bodyText1),
        titleSpacing: 0.0,
        centerTitle: true,

          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {getinfo();},
                  child: Image.asset(
                              "images/whatsapp.png",
                              height: 25,
                              width: 25,
                            ),
                )
            ),
      //     actions: <Widget>[
      // Padding(
      // padding: EdgeInsets.only(right: 20.0),
      //       InkWell(
      //         onTap:  () {
      //             getinfo();
      //             },
      //         child: Image.asset(
      //           "images/whatsapp.png",
      //           height: 25,
      //           width: 25,
      //         ),
      //       )


            // IconButton(
            //   icon: Icon(
            //     Icons.message,
            //     color: Colors.green,
            //   ),
            //   onPressed: () {
            //           getinfo();
            //   },
            // )

          ]

      ),
      body: Add(),
    );
  }
}


class ImageDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        width: 200,
        height: 220,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // Card(
              //   elevation: 3,
              //   shape: RoundedRectangleBorder(
              //     borderRadius: BorderRadius.circular(70.0),
              //   ),
              //   child:
                Padding(
                  padding: EdgeInsets.all(6.0),
                  child: Image.asset(
                    // "images/badge.png",
                    "images/thank_you.png",
                    height: 70,
                    width: 70,
                  ),
                ),
              // ),
              // Padding(
              //   padding: EdgeInsets.symmetric(horizontal: 20,vertical: 5),
              //   child: Text(
              //     "Thank You for Request.",
              //     style: TextStyle(
              //         fontSize: 18, color: kMainColor, fontFamily: "OpenSans"),
              //   ),
              // ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                child: Text(
                  "Our customer relations manager will get in touch with you shortly.",
                  style: TextStyle(
                      fontSize: 14, color: Colors.orange, fontFamily: "OpenSans"),
                ),
              )
            ]),
      ),
    );
  }
}



getinfo() async
{
  var whatsappUrl ="whatsapp://send?phone=919381117607&text=hi";

  await launch(whatsappUrl);
}

class Add extends StatefulWidget {
  @override
  _AddState createState() => _AddState();
}

class _AddState extends State<Add> {

  final TextEditingController item_name_controller = TextEditingController();
  final TextEditingController item_detail_controller = TextEditingController();
  final TextEditingController item_actual_price_controller = TextEditingController();
  final TextEditingController item_discount_price_controller = TextEditingController();
  final TextEditingController item_original_price_controller = TextEditingController();
  final TextEditingController item_quantity_controller = TextEditingController();
  ProgressDialog pr;
  bool _isLoading=false;

  int _value = 1;
  File imageFile;
  String base64Image;
  File tmpFile;
  String errMessage = 'Error Uploading Image';
  String status = '';

  setStatus(String message) {
    setState(() {
      status = message;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    item_name_controller.dispose();
    item_detail_controller.dispose();
    item_actual_price_controller.dispose();
    item_discount_price_controller.dispose();
    item_original_price_controller.dispose();
    item_quantity_controller.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  Future<void> _showChoiceDialog(BuildContext context){
    _openGallery(BuildContext context) async{
      var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
      this.setState(() {
        imageFile = picture;
      });

      Navigator.of(context).pop();
    }
    _openCamera(BuildContext context) async{
      var picture = await ImagePicker.pickImage(source: ImageSource.camera);
      this.setState(() {
        imageFile = picture;

      });
      Navigator.of(context).pop();
    }
    return showDialog(context: context,builder:(BuildContext context)
    {
      return AlertDialog(
        title: Text("Make a Choice!!!",  style: Theme.of(context)
            .textTheme
            .caption
            .copyWith(color: kMainColor)),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              GestureDetector(
                child: Text("Gallery", style: Theme.of(context).textTheme.headline6.copyWith(
                    fontWeight: FontWeight.w500,
                    letterSpacing: 0.67,
                    fontSize: 15.0,
                    color: kMainTextColor),),
                onTap: () {
                  _openGallery(context);
                },
              ),
              Padding(padding: EdgeInsets.all(10.0),),
              GestureDetector(
                child: Text("Camera" ,style: Theme.of(context).textTheme.headline6.copyWith(
                    fontWeight: FontWeight.w500,
                    letterSpacing: 0.67,
                    fontSize: 15.0,
                    color: kMainTextColor),),
                    onTap: () {
                  _openCamera(context);
                },
              )
            ],
          ),
        ),
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true,isDismissible: false);
    pr.style(message: 'Please wait...');

    return Stack(
      // Container(
      // child: Column(
      //   crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        // Container(
        //   padding: EdgeInsets.all(48.0),
        //   color: kCardBackgroundColor,
        //   child: Image(
        //     image:
        //     AssetImage("images/img2.jpg"), //gobunty logo
        //     height: 130.0,
        //     width: 400.7,
        //   ),
        // ),
        // Image.asset(
        //   "images/img1.jpg",
        //   height: 500,
        //   width: 500,
        // ),
        ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: ClampingScrollPhysics(),
        itemCount: 1,
        itemBuilder: (context, i) {
           return
          Padding(
              padding: EdgeInsets.only(left: 20.0,top: 10.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Upload Prescriptions ",
                        style: Theme.of(context).textTheme.headline6.copyWith(
                            fontWeight: FontWeight.w300,
                            letterSpacing: 0.67,
                            color: textPrimaryColor),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                        _showChoiceDialog(context);
                        // print("Container was tapped");
                        },
                        child:
                        ClipRRect(
                          borderRadius: BorderRadius.all(
                              Radius.circular(
                                  15.0)),
                          child:Container(
                            height: 80.0,
                            width: 80.0,
                            child: imageFile == null ?  Image.asset('images/medical_file.png') : Image.file(imageFile),

                          ),
                          // child: Image.asset(
                          //     // imageFile == null ?  Image.asset('images/photo.png') : Image.file(imageFile),
                          //     "images/medical_file.png",
                          //     width: 80,
                          //     height: 80,
                          //     fit: BoxFit.cover),
                        ),

                        // child:Container(
                        //   height: 80.0,
                        //   width: 80.0,
                        //   child: imageFile == null ?  Image.asset('images/photo.png') : Image.file(imageFile),
                        //
                        // ),
                    ),

                    ],
                  ), SizedBox(height: 10.0),
                  // Divider(
                  //   color: kCardBackgroundColor,
                  //   thickness: 8.0,
                  // ),

                ],
              ),
            );


        }),
        Align(
          alignment: Alignment.bottomCenter,
          child: BottomBar(
            text: "Upload",
            onTap: (){
              if (isValidPhoneNumber(imageFile)) {
                pr.show();
                AddPrescripitionApi();
              }
              else {
                Toast.show("First Add Image", context);
                pr.hide();
              }
            },
          ),
        )
      ],
      // ),
      // ),
    );
  }

  bool isValidPhoneNumber(File imageFile) {

    if (imageFile == null) {
      return false;
    }
    return true;
  }



String UserId;
AddPrescripitionApi() async {

  if (imageFile == null) return;
  String base64Image = base64Encode(imageFile.readAsBytesSync());
  String fileName = imageFile.path.split("/").last;

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  UserId = sharedPreferences.getString('uid');
  print(UserId);
  // print(category/_id);
  Map data = {
    'img_name': fileName,
    'image' : base64Image,
    'user_id' : UserId,
  };
  print(data);

  var jsonResponse = null;
  var response = await http.post('https://gobunty.com/api/User/add_pharmacy', body: data);

  print (response.statusCode);

  if(response.statusCode == 200) {
    jsonResponse = json.decode(response.body);
    if(jsonResponse != null) {
      print(jsonResponse);
      // sharedPreferences.setString("token", phone);
      if(jsonResponse['msg']=="Success") {
        Navigator.pop(context);
        Toast.show(jsonResponse['msg'],context);
        showDialog(context: context, builder: (_) => ImageDialog());
        // Navigator.pop(context);
      }
      // else{
      //   Toast.show(jsonResponse['msg'],context);
      // }
      else{
        Toast.show(jsonResponse['msg'],context);
        pr.hide().whenComplete(() =>
        Navigator.pop(context),
        );
      }
    }
  }
  else {
    setState(() {
      _isLoading = false;
    });
    print(response.body);
  }
 }
}
// String UserId;
// AddPrescripitionApi() async {
//
//   if (imageFile == null) return;
//
//   String base64Image = base64Encode(imageFile.readAsBytesSync());
//
//   String fileName = imageFile.path.split("/").last;
//
//
//   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//
//   UserId = sharedPreferences.getString('uid');
//   print(UserId);
//   // print(category/_id);
//   Map data = {
//     'img_name': fileName,
//     'image' : base64Image,
//     'user_id' : UserId,
//   };
//   print(data);
//
//   var jsonResponse = null;
//   var response = await http.post('https://gobunty.com/api/User/add_pharmacy', body: data);
//
//   print (response.statusCode);
//
//   if(response.statusCode == 200) {
//     jsonResponse = json.decode(response.body);
//     if(jsonResponse != null) {
//       print(jsonResponse);
//       // sharedPreferences.setString("token", phone);
//       if(jsonResponse['msg']!="Success") {
//
//         Navigator.pop(context);
//         Toast.show(jsonResponse['msg'],context);
//         // showDialog(context: context, builder: (_) => ImageDialog());
//         // Navigator.pop(context);
//       }else{
//         Toast.show(jsonResponse['msg'],context);
//       }
//       {
//         pr.hide().whenComplete(() =>
//         Navigator.pop(context),
//         );
//       }
//     }
//   }
//   else {
//     setState(() {
//       _isLoading = false;
//     });
//     print(response.body);
//   }
//  }
// }