import 'dart:convert';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:gobunty/Components/FoodColors.dart';
import 'package:gobunty/Components/FoodConstant.dart';
import 'package:gobunty/Components/FoodWidget.dart';
import 'package:gobunty/Components/custom_appbar.dart';
import 'package:gobunty/Modals/store_model.dart';
import 'package:gobunty/Pages/items.dart';
import 'package:gobunty/Pages/newitem.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:toast/toast.dart';

class StoresPage extends StatelessWidget {
  final String pageTitle;
  final String text;

  StoresPage(this.pageTitle, this.text);
  @override
  Widget build(BuildContext context) {
    return Stores(pageTitle, text);
  }
}

class Stores extends StatefulWidget {
  final String pageTitle;
  final String text1;
  Stores(this.pageTitle, this.text1);
  @override
  StoresState createState() => StoresState(pageTitle, text1);
}

class StoresState extends State<Stores> {
  final String pageTitle;
  final String text1;
  String lat, lon, address = "", dis = "";
  StoresState(this.pageTitle, this.text1);
  bool check1 = false;
  final int noOfStores = 28;
  bool status=false;
  ProgressDialog pr;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getlocation();
    getData();
  }

  void _getlocation() async {
    final geopos = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      lat = '${geopos.latitude}';
      lon = '${geopos.longitude}';
    });
    double fat = double.parse(lat);
    double fon = double.parse(lon);

    // print("${first.featureName} : ${first.addressLine}");
  }

  void getstreet(String lat, String lon) async {
    final coordinates = new Coordinates(
        double.parse(lat.toString()), double.parse(lon.toString()));
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;

    setState(() {
      address = "${first.addressLine}";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(pageTitle, style: Theme.of(context).textTheme.bodyText1),
        titleSpacing: 0.0,
        centerTitle: true,
      ),
      body:
      SingleChildScrollView(child:
       Container(
        decoration: boxDecoration(showShadow: true, radius: 0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              projectWidget(context),
            ]),
      ),)
    );
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }
  BuildContext context;
  Widget projectWidget(BuildContext context) {
    setState(() => this.context = context);
    var width = MediaQuery.of(context).size.width;
    return status?
      Padding(
      padding: EdgeInsets.all(0),
      child:store.length > 0 ? ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: store.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FoodDescription(
                        id: store[index].id,
                        model: store[index].name,
                        addr: store[index].address,
                        fee: store[index].image),
                  ),
                );
              },
               child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            ClipRRect(
                              child: CachedNetworkImage(
                                imageUrl: store[index].image,
                                width: width / 3.5,
                                height: width / 3.2,
                                fit: BoxFit.fill,
                              ),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(left: 10.0,top: 10),

                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        text(store[index].name,
                                            textColor: food_textColorPrimary,
                                            fontSize: textSizeSMedium,
                                            fontFamily: fontBold),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        text("Order Online",
                                            fontSize: textSizeSmall,
                                            textColor: food_color_green,
                                            fontFamily: fontBold),

                                        SizedBox(
                                          height: 4,
                                        ),
                                        text(store[index].range + " km",
                                            fontSize: textSizeSmall,
                                            textColor: food_icon_color),

                                        SizedBox(
                                          height: 4,
                                        ),
                                    Row(
                                         children: <Widget>[
                                        // mDot(),
                                           Icon(
                                             Icons.lock_clock,
                                             color: Colors.orange,
                                             size: 15.0,
                                           ),
                                      SizedBox(width: 5.0),

                                        text("45 mins",
                                            fontSize: textSizeSmall,
                                            textColor: food_icon_color,

                                        ),
                                        ]
                                    ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                    // Row(
                                    //   children: <Widget>[
                                    //     // mDot(),
                                    //     Icon(
                                    //       Icons.location_on_outlined,
                                    //       color: Colors.orange,
                                    //       size: 15.0,
                                    //     ),
                                    //     SizedBox(width: 5.0),


                                        text(store[index].address,
                                            fontSize: textSizeSmall,
                                            maxLine: 3),
                                        SizedBox(
                                          height: 4,
                                        ),

                                      // ]
                                    // ),

                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        divider()
                      ],
                    )
               ),
             //  child: Container(
             //    margin: EdgeInsets.only(bottom: spacing_standard_new),
             //    decoration: boxDecoration(radius: 0, showShadow: true),
             //    child: Column(
             // crossAxisAlignment: CrossAxisAlignment.start,
             //      children: <Widget>[
             //        CachedNetworkImage(
             //            imageUrl: store[index].image,
             //            width: width,
             //            height: 180,
             //            fit: BoxFit.fill),
              //       Container(
              //         padding: EdgeInsets.fromLTRB(spacing_standard_new,
              //             spacing_standard_new, spacing_standard_new, 0),
              //         child: Column(
              //           crossAxisAlignment: CrossAxisAlignment.start,
              //           children: <Widget>[
              //             text(store[index].name,
              //                 fontSize: textSizeSMedium, fontFamily: fontMedium),
              //             SizedBox(height: spacing_control),
              //             Row(children: <Widget>[
              //               mRating("4"),
              //               SizedBox(width: spacing_standard),
              //               Flexible(
              //                 child: text(
              //                     store[index].address.toString().replaceAll("\n", ""),
              //                     textColor: food_textColorSecondary),
              //               ),
              //             ]),
              //             SizedBox(height: spacing_control),
              //             Row(
              //               children: <Widget>[
              //                 mDot(),
              //                 SizedBox(width: spacing_standard),
              //                 text("Shop", textColor: food_textColorSecondary),
              //                 SizedBox(width: spacing_standard),
              //                 mDot(),
              //                 SizedBox(width: spacing_standard),
              //                 text(store[index].range + " kms",
              //                     textColor: food_textColorSecondary),
              //               ],
              //             ),
              //             Container(
              //                 margin: EdgeInsets.only(top: spacing_standard_new),
              //                 height: 0.5,
              //                 color: food_view_color,
              //                 width: width),
              //           ],
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
            );
          }):Center(child: Text("No Shop Found",style: TextStyle(fontFamily: "OpenSans",fontSize: 20,color: Colors.black),),),
    ):Container(
        height: 400,
        child:  Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      enabled: true,
      child: ListView.builder(
        itemBuilder: (_, __) => Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 48.0,
                height: 48.0,
                color: Colors.white,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      height: 8.0,
                      color: Colors.white,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 2.0),
                    ),
                    Container(
                      width: double.infinity,
                      height: 8.0,
                      color: Colors.white,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 2.0),
                    ),
                    Container(
                      width: 40.0,
                      height: 8.0,
                      color: Colors.white,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        itemCount: 6,
      ),
    ));
  }

  Widget divider() {
    return Divider(
      height: 0.5,
      color: kLightTextColor,
    );
  }

  List<StoreModel> store = [];
  Future<Map> getData() async {
    Map data1 = {
      'category_name': text1,
    };
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/vendor/vendor_list",
        body: data1);
    Map data;
    if (response.statusCode == 200) {
      status=true;
      data = json.decode(response.body);
      for (var v in data['vendor_data']) {
        setState(() {
          store.add(new StoreModel(
              v['id'],
              v['name'],
              v['description'],
              v['address'],
              v['longitude'],
              v['latitude'],
              v['phone'],
              v['mobile'],
              v['delivery_fee'],
              v['available_for_delivery'],
              v['image'],
              v['delivery_range']));
        });

      }
    }
    else if (response.statusCode == 404) {
      setState(() {
        status=true;
      });

      Toast.show("No Shop Available", context);
    }
    else {
      Toast.show("Connection Error", context);
    }
    print(data);

  }
}
