import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:gobunty/Components/bottom_bar.dart';
import 'package:gobunty/Components/entry_field.dart';
import 'package:gobunty/Modals/hservice.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:gobunty/Maps/UI/location_page.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:location/location.dart';


const kGoogleApiKey = "AIzaSyC14egQ8aT1JC702KV9VCQdhWYtXBOS2a8";

// to get places detail (lat/lng)
GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class CustomDeliveryPage extends StatelessWidget {
  final String pageTitle, text;

  CustomDeliveryPage(this.pageTitle, this.text);

  @override
  Widget build(BuildContext context) {
    return CustomDelivery(pageTitle,text);
  }
}

class CustomDelivery extends StatefulWidget {
  final String pageTitle,text;

  CustomDelivery(this.pageTitle,this.text);

  @override
  _CustomDeliveryState createState() => _CustomDeliveryState(pageTitle,text);
}

class _CustomDeliveryState extends State<CustomDelivery> {
  // CustomDeliveryBloc _deliveryBloc;
  TextEditingController pickUpController = TextEditingController();
  TextEditingController dropController = TextEditingController();
  TextEditingController valuesController = TextEditingController();
  TextEditingController instructionController = TextEditingController();
  String selectedValue;
  List<hservice> sortFilter = [];
  String pageTitle,text;
  String add;
  String address1 = "", aid = "";
  TextEditingController _addressController = TextEditingController();
  TextEditingController _mobileController = TextEditingController();

  String Address;
  double lat,lng;

  _CustomDeliveryState(this.pageTitle,this.text);
  @override
  void initState() {
    super.initState();
    getData();
    getValue();
    fetch();
    // _deliveryBloc = BlocProvider.of<CustomDeliveryBloc>(context);
  }
  var textPrimaryColorGlobal = textPrimaryColor;
  var shadowColorGlobal = Colors.black12;
  TextStyle boldTextStyle({
    int size = 16,
    Color color,
    FontWeight weight = FontWeight.bold,
    String fontFamily,
    double letterSpacing,
  }) {
    return TextStyle(
      fontSize: size.toDouble(),
      color: color ?? textPrimaryColorGlobal,
      fontWeight: weight,
      fontFamily: fontFamily,
      letterSpacing: letterSpacing,
    );
  }

  BoxDecoration boxDecoration({double radius = 2, Color color = Colors.transparent, Color bgColor = appWhite, var showShadow = false}) {
    return BoxDecoration(
      color: bgColor,
      boxShadow: showShadow ? [BoxShadow(color: shadowColorGlobal, blurRadius: 5, spreadRadius: 1)] : [BoxShadow(color: Colors.transparent)],
      border: Border.all(color: color),
      borderRadius: BorderRadius.all(Radius.circular(radius)),
    );
  }

  TextStyle primaryTextStyle({
    int size = 16,
    Color color,
    FontWeight weight = FontWeight.normal,
    String fontFamily,
    double letterSpacing,
  }) {
    return TextStyle(
      fontSize: size.toDouble(),
      color: color ?? textPrimaryColorGlobal,
      fontWeight: weight,
      fontFamily: fontFamily,
      letterSpacing: letterSpacing,
    );
  }
  String mno = "";
  // String address = "";

  void getValue() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      mno = pref.getString("mobile");
      _mobileController.text = mno;
      // address = pref.getString("address");
      print(mno);
      // print(address);
    });
    // print(address);
    print(mno);
  }

  @override
  void dispose() {
    pickUpController.dispose();
    dropController.dispose();
    valuesController.dispose();
    instructionController.dispose();
    super.dispose();
  }

  ScrollController controller = ScrollController();
  Future<Map> getData() async {
    Map data1 = {
      'category_name': text,
    };
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/user/service_list",
        body: data1);
    Map data;
    if (response.statusCode == 200) {
      data = json.decode(response.body);
      for (var val in data['user_data']) {
        setState(() {
          sortFilter.add(new hservice(val['id'], val['name'], val['image']));
          selectedValue = sortFilter[0].name;
          id= sortFilter[0].id;
        });
      }
    } else {
      Toast.show("Connection Error", context);
    }
    return data;
  }
//Bottom Sheet
  mFormBottomSheet(BuildContext aContext) {
    showModalBottomSheet(
      backgroundColor: Colors.white,
      context: aContext,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Padding(
            padding: MediaQuery.of(context).viewInsets,
         child : Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // Card(
              //   elevation: 3,
              //   child: Container(
              //     color: Colors.white,
              //     child: Padding(
              //       padding: EdgeInsets.only(
              //           left: 20.0,
              //           right: 20.0,
              //           top: 13.0,
              //           bottom: 13.0),
              //       child: Column(
              //         crossAxisAlignment:
              //         CrossAxisAlignment.start,
              //         children: <Widget>[
              //           Row(
              //             children: <Widget>[
              //               Icon(
              //                 Icons.location_on,
              //                 color: Color(0xffc4c8c1),
              //                 size: 13.3,
              //               ),
              //               SizedBox(
              //                 width: 11.0,
              //               ),
              //               Text('Deliver to ',
              //                   style: Theme.of(context)
              //                       .textTheme
              //                       .caption
              //                       .copyWith(
              //                       color: kDisabledColor,
              //                       fontWeight:
              //                       FontWeight.bold)),
              //               Text(add==null?add = "Home":add,
              //                   style: Theme.of(context)
              //                       .textTheme
              //                       .caption
              //                       .copyWith(
              //                       color: kMainColor,
              //                       fontWeight:
              //                       FontWeight.bold)),
              //               // Spacer(),
              //               InkWell(
              //                 onTap: () {
              //                   Navigator.push(
              //                       context,
              //                       MaterialPageRoute(
              //                           builder: (context) =>
              //                               LocationPage(
              //                                   bool1: false)));
              //                 },
              //                 child: Text('Edit Location',
              //                     style: Theme.of(context)
              //                         .textTheme
              //                         .caption
              //                         .copyWith(
              //                         color: kMainColor,
              //                         fontWeight:
              //                         FontWeight.bold)),
              //               ),
              //               PopupMenuButton(
              //                 onSelected: choiceAction,
              //                 itemBuilder: (BuildContext context) {
              //                   return Constants.choices
              //                       .map((String choice) {
              //                     return PopupMenuItem(
              //                       value: choice,
              //                       child: Text(choice),
              //                     );
              //                   }).toList();
              //                 },
              //               ),
              //             ],
              //           ),
              //           SizedBox(
              //             height: 5.0,
              //           ),
              //           Text(address1,
              //               style: Theme.of(context)
              //                   .textTheme
              //                   .caption
              //                   .copyWith(
              //                   fontSize: 11.7,
              //                   color: Color(0xffb7b7b7)))
              //         ],
              //       ),
              //     ),
              //   ),
              // ),
              // Divider().paddingOnly(top: 16, bottom: 16),
              Text(
                "Address",
              ),
              (SizedBox (height:10) ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: EntryField(
                  controller: _addressController,
                  hint: 'Address',
                  readOnly: true,
                  onTap: _handlePressButton,
                ),
              ),
              (SizedBox (height:16) ),
              Text(
                "Mobile Number",
              ),(SizedBox (height:10) ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: EntryField(
                  controller: _mobileController,
                  hint: 'mobile no',
                ),
              ),
              // Text(
              //   mno,
              //   style: TextStyle(
              //       fontSize: 15,
              //       fontFamily: "OpenSans",
              //       color: Colors.black),
              // ),
              (SizedBox (height:16) ),
              GestureDetector(
                onTap: () {
                  addservice(_addressController.text,_mobileController.text);
                  Navigator.pop(context);
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: boxDecoration(bgColor: appColorPrimary, radius: 16),
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                  child: Center(
                    child: Text(
                      "Confirm",
                      style: primaryTextStyle(color: white),
                    ),
                  ),
                ),
              )
            ],
          ),
         ),
        );
      },
    );
  }

  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      onError: onError,
      mode: Mode.overlay,
      language: "en",
      // language: "en",
      components: [Component(Component.country, "in")],
    );

    displayPrediction(p, homeScaffoldKey.currentState);
  }

  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);

      lat = detail.result.geometry.location.lat ;
      lng = detail.result.geometry.location.lng ;

      print(lat);
      print(lng);
      // print(detail.result.geometry.location);
      print(p.description);
      Address = p.description;

      setState(() {
        _addressController.text = p.description;


        print(_addressController.text);
      });

      // scaffold.showSnackBar(
      //   SnackBar(content: Text("${p.description} - $lat/$lng")),
      //
      // );
    }
  }

  void onError(PlacesAutocompleteResponse response) {
    Toast.show("errrorrrrrrrrrr", context);
    // homeScaffoldKey.currentState.showSnackBar(
    //   SnackBar(content: Text(response.errorMessage)),
    // );
  }



  Future<Map> fetch() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'user_id': pref.getString("uid"),
      'type': "Home",
    };
    print(data1);
    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/User_Delivery/fetch_address",
        body: data1);

    Map<String, dynamic> map = json.decode(response.body);
    var data2 = map["address_data"];

    print(map);

    print(data2);
    if (data2 != null) {
      if (mounted) {
        setState(() {
          address1 = data2['address'];
          aid = data2['id'];
          lat = double.parse(data2['latitude'].toString());
          lng = double.parse(data2['longitude'].toString());

          // "longitude": "75.9028363",
          // "latitude": "22.7568947",
          /* listadr.add(new AdrModal(
              data['id'], data['address'], "", data['type'], data['latitude'],
              data['longitude']));*/
          setState(() {
            _addressController.text = address1;
          });
        });
      }
    } else {
      Toast.show("No Address Available", context);
    }

    print("addressssss"+address1);
    print("latttt"+lat.toString());
    print("longgg"+lng.toString());
  }

  void choiceAction(String choice) {
    if (choice == Constants.Settings) {
      setState(() {
        add = "Office";

        fetch();
      });
    } else if (choice == Constants.Subscribe) {
      setState(() {
        add = "Home";
        fetch();
      });
    } else if (choice == Constants.SignOut) {
      setState(() {
        add = "Other";
        fetch();
      });
    }
  }


  void addservice(String address,String Mno) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    print(id);
    Map data = {
      'user_id': pref.getString('uid'),
      'service_id': id,
      'address': address,
      'mobile': Mno,
      'lat': lat.toString(),
      'lng': lng.toString(),
    };

    print(data);
    var jsonResponse = null;
    var response = await http
        .post("https://gobunty.com/api/Service/home_service_add", body: data);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.toString());
      Navigator.pop(context);
      showDialog(context: context, builder: (_) => ImageDialog());
    } else {
      print('Something went wrong');
    }
    print(response.body);
    print(response.statusCode);
  }

  String id;
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return MaterialApp(
      theme: ThemeData(highlightColor: kBarColor),
      home: Scaffold(
          backgroundColor: kCardBackgroundColor,
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              pageTitle,
              style: Theme.of(context).textTheme.bodyText1,
            ),
            leading: new IconButton(
                icon: new Icon(Icons.arrow_back),
                color: kMainColor,
                onPressed: () => Navigator.of(context).pop()),
            titleSpacing: 0.0,
          ),
          body: Stack(
              children:<Widget>[
              Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: height - 153,
                  child: sortFilter.length > 0
                      ? ListView.builder(
                          itemBuilder: (ctx, index) {
                            return GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {
                                selectedValue = sortFilter[index].name;
                                id = sortFilter[index].id;
                                setState(() {});
                              },
                              child: Container(
                                height: 120,
                                color: selectedValue == sortFilter[index].name
                                    ? kTransparentColor
                                    : null,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    color: Colors.white,
                                    elevation: 7,
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(10.0,10.0,30.0,5.0),
                                      // padding: EdgeInsets.symmetric(horizontal: 20),
                                          // elevation: 4,
                                    // child: Theme(
                                    //   data: Theme.of(context).copyWith(
                                    //     unselectedWidgetColor: appStore.textPrimaryColor,
                                    //   ),
                                    //   child: CheckboxListTile(
                                    //     value: isChecked12,
                                    //     checkColor: appStore.appBarColor,
                                    //     secondary: Container(
                                    //       height: 40,
                                    //       width: 40,
                                    //       decoration: BoxDecoration(
                                    //         image: DecorationImage(
                                    //             image: Image.asset(
                                    //               'images/widgets/materialWidgets/mwInputSelectionWidgets/Checkbox/profile.png',
                                    //             ).image),
                                    //         shape: BoxShape.circle,
                                    //       ),
                                    //     ),
                                    //     onChanged: (checked) {
                                    //       setState(() {
                                    //         isChecked12 = checked;
                                    //       });
                                    //     },
                                    //     title: Text(
                                    //       'Checkbox Tile',
                                    //       style: boldTextStyle(size: 18),
                                    //     ),
                                    //     subtitle: Text(
                                    //       'Custom Leading value ',
                                    //       style: secondaryTextStyle(),
                                    //     ),
                                    //   ),
                                    // ),


                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.all(13),
                                          child:
                                          ClipRRect(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(
                                                    15.0)),
                                            child: CachedNetworkImage(
                                                imageUrl:  sortFilter[index].image,
                                                width: 50,
                                                height: 50,
                                                fit: BoxFit.cover),
                                          ),


                                          // Image(
                                          //   image: CachedNetworkImageProvider(
                                          //       sortFilter[index].image),
                                          //   width: 45,
                                          //   height: 45,
                                          // ),
                                        ),
                                       Expanded(
                                          child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,

                                              children: <Widget>[
                                                Flexible(
                                                  child: Text(
                                                    sortFilter[index].name,
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontFamily: "OpenSans",
                                                        color: Colors.black),
                                                  ),
                                                ),
                                              ]),
                                        ),
                                        selectedValue == sortFilter[index].name
                                            ? Icon(Icons.check_box,color: kMainColor,)
                                        // mFormBottomSheet(context)
                                            : Icon(
                                                Icons.check_box_outline_blank,color: kMainColor,),
                                      ],
                                    ),
                                  ),
                                  )
                                ),
                              ),
                            );
                          },
                          itemCount: sortFilter.length,
                        )
                      : Center(
                          child: CircularProgressIndicator(),
                        ),
                ),
                BottomBar(

                    text: "Continue",
                    onTap: () {
                      mFormBottomSheet(context);
                    }),
              ]
          )
          ]
          ),
    ),
    );
  }
}

class ImageDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        width: 200,
        height: 220,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Card(
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(70.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(6.0),
                  child: Image.asset(
                    "images/badge.png",
                    height: 50,
                    width: 50,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                child: Text(
                  "Thank You for Request.",
                  style: TextStyle(
                      fontSize: 18, color: kMainColor, fontFamily: "OpenSans"),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                child: Text(
                  "Our customer relations manager will get in touch with you shortly.",
                  style: TextStyle(
                      fontSize: 12, color: Colors.orange, fontFamily: "OpenSans"),
                ),
              )
            ]),
      ),
    );
  }
}

class Constants {
  static const String Subscribe = 'Home';
  static const String Settings = 'Office';
  static const String SignOut = 'Other';

  static const List<String> choices = <String>[Subscribe, Settings, SignOut];
}

/*
//bottom sheets that pops up on select package type field
class ModalBottomWidget extends StatelessWidget {
  final List<String> list = <String>[
    'Paper & Documents',
    'Flowers & Chocolates',
    'Sports & Toys item',
    'Clothes & Accessories',
    'Electronic item',
    'Household item',
    'Glass item',
  ];

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          color: kCardBackgroundColor,
          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Select Package Type',
                style: Theme.of(context).textTheme.headline4,
              ),
              RaisedButton(
                child: Text('Done'),
                onPressed: () {
                  //_deliveryBloc.add(ValuesShowEvent());
                  Navigator.pop(context);
                },
                disabledTextColor: kLightTextColor,
                shape: OutlineInputBorder(
                    borderSide: BorderSide(color: kTransparentColor),
                    borderRadius: BorderRadius.circular(30.0)),
              ),
            ],
          ),
        ),
        CheckboxGroup(
          labelStyle: Theme.of(context).textTheme.caption,
          //labels: _deliveryBloc.list,
          padding: EdgeInsets.only(top: 16.0),
          onSelected: (List<String> checked) {}, labels: list,
        ),
      ],
    );
  }
}
*/
