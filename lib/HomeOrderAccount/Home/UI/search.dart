import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/Stores/stores.dart';
import 'package:gobunty/Modals/searchModal.dart';
import 'package:gobunty/Pages/items.dart';
import 'package:gobunty/Pages/newitem.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => new _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController controller = new TextEditingController();
  bool check1=false;
  ProgressDialog pr;
  // Get json result and convert it to model. Then add
  Future<Null> getUserDetails() async {

    var response = await http.post("https://gobunty.com/api/Search/search_data");
    if (response.statusCode == 200) {

      String responeBody = response.body;
      var jsonBody = json.decode(responeBody);
      for (var data in jsonBody['search_data']) {

        if(data['type']!="Product") {
          _userDetails.add(
            new searchModal(
                data['id'],
                data['name'],
                data['image'],
                data['type'],
                data['model_id'],
                data['category_name'],
                data['address'],
                data['delivery_fee']),

          );
        }
        else{
          _userDetails.add(
            new searchModal(
                data['id'],
                data['name'],
                data['image'],
                data['type'],
                data['vendor_id'],
                data['category_name'],
                data['address'],
                data['delivery_fee']),

          );
        }
      }
      setState(() {});
      print(jsonBody);
    } else {
      pr.hide().whenComplete(() => Toast.show("Something Went Wrong", context));
      print('Something went wrong');
    }
  }

  Future<bool> check(String id) async {
    print(id);
    String ip;
    int k;
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data1 = {
      'vendor_id': id,
      'user_id': pref.getString("uid"),
    };
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/check_vendor.php",
        body: data1);

    Map data = json.decode(response.body);
    print(data);

    if (data['msg']=="success") {
      setState(() {
        check1 = true;
      });
    }
print(check1);
    return check1;
  }

  @override
  void initState() {
    super.initState();

    getUserDetails();
  }

  @override
  Widget build(BuildContext context) {

    pr = new ProgressDialog(context, showLogs: true,isDismissible: false);
    pr.style(message: 'Please wait...');
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Search'),
        backgroundColor: kMainColor,
        elevation: 0.0,
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            color: Colors.white,
            onPressed: () {
              Navigator.of(context).pop();
              controller.text = "";
              _userDetails.clear();
              _searchResult.clear();
            }),
      ),
      body: new Column(
        children: <Widget>[
          new Container(
            color: Theme.of(context).primaryColor,
            child: new Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Card(
                child: new ListTile(
                  leading: new Icon(Icons.search),
                  title: new TextField(
                    controller: controller,
                    decoration: new InputDecoration(
                        hintText: 'Search', border: InputBorder.none),
                    onChanged: onSearchTextChanged,
                  ),
                  trailing: new IconButton(
                    icon: new Icon(Icons.cancel),
                    onPressed: () {
                      controller.clear();
                      onSearchTextChanged('');
                    },
                  ),
                ),
              ),
            ),
          ),
          new Expanded(
            child: _searchResult.length != 0 || controller.text.isNotEmpty
                ? new ListView.builder(
                    itemCount: _searchResult.length,
                    itemBuilder: (context, i) {
                      return new Card(
                        child: InkWell(
                          onTap: () {
                            if (_searchResult[i].type != "Vendor") {

                                Navigator.pop(context);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => FoodDescription(
                                        id: _searchResult[i].model,
                                        model: _searchResult[i].cname,
                                        addr: _searchResult[i].addr,
                                        fee: _searchResult[i].url),
                                  ),
                                );

                            } else {

                                Navigator.pop(context);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => FoodDescription(
                                        id: _searchResult[i].id,
                                        model: _searchResult[i].name,
                                        addr: _searchResult[i].addr,
                                        fee: _searchResult[i].url),
                                  ),
                                );
                              }
                          },
                          child: new ListTile(
                            leading: new CircleAvatar(
                              backgroundImage: new CachedNetworkImageProvider(
                                _searchResult[i].url,
                              ),
                            ),
                            title: new Text(_searchResult[i].name),
                          ),
                        ),
                        margin: const EdgeInsets.all(0.0),
                      );
                    },
                  )
                : new ListView.builder(
                    itemCount: _userDetails.length,
                    itemBuilder: (context, index) {
                      return new Card(
                        child: InkWell(
                          onTap: () {
                            if (_userDetails[index].type != "Vendor") {

                                  print(_userDetails[index].model);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FoodDescription(
                                          id: _userDetails[index].model,
                                          model: _userDetails[index].cname,
                                          addr: _userDetails[index].addr,
                                          fee: _userDetails[index].url),
                                    ),
                                  );
                                }

                            else {

                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FoodDescription(
                                          id: _userDetails[index].id,
                                          model: _userDetails[index].name,
                                          addr: _userDetails[index].addr,
                                          fee: _userDetails[index].url),
                                    ),
                                  );
                                }


                            }
                          ,
                          child: new ListTile(
                            leading: new CircleAvatar(
                              backgroundImage: new CachedNetworkImageProvider(
                                _userDetails[index].url,
                              ),
                            ),
                            title: new Text(_userDetails[index].name),
                          ),
                        ),
                        margin: const EdgeInsets.all(0.0),
                      );
                    },
                  ),
          ),
        ],
      ),
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _userDetails.forEach((userDetail) {
      if (userDetail.name.toLowerCase().contains(text.toLowerCase()))
        _searchResult.add(userDetail);
    });

    setState(() {});
  }
}

List<searchModal> _searchResult = [];

List<searchModal> _userDetails = [];
