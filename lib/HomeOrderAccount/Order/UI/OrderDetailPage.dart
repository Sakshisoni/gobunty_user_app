import 'package:gobunty/Components/bottom_bar.dart';
import 'package:gobunty/Components/custom_appbar.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:gobunty/Chat/UI/chat_page.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';
import 'package:gobunty/Components/FoodWidget.dart';
import 'package:gobunty/Modals/NewOrderInfo.dart';

List<NewOrderInfo> new_order_info = [];
class OrderInfo extends StatefulWidget {

  String OrderId;
  OrderInfo({this.OrderId});

  @override
  // _OrderInfoState createState() => _OrderInfoState(Orderid);
  _OrderInfoState createState() => _OrderInfoState(OrderId);
}

class _OrderInfoState extends State<OrderInfo> {
  ProgressDialog pr;
  String OrderId;
  _OrderInfoState(this.OrderId);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    NewOrderInfoApi(OrderId);
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  //
  // Widget text(String text,
  //     {var fontSize = textSizeSmall,
  //       textColor = t1_colorPrimaryDark,
  //       var fontFamily = fontRegular,
  //       var isCentered = false,
  //       var maxLine = 1,
  //       var latterSpacing = 0.25,
  //       var textAllCaps = false,
  //       var isLongText = false}) {
  //
  //   return Text(textAllCaps ? text.toUpperCase() : text,
  //       textAlign: isCentered ? TextAlign.center : TextAlign.start,
  //       maxLines: isLongText ? null : maxLine,
  //       style: TextStyle(fontFamily: fontFamily, fontSize: fontSize, color: textColor, height: 1.5, letterSpacing: latterSpacing));
  // }


  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true,isDismissible: false);
    pr.style(message: 'Please wait...');

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Color(0xff1c437e),
        title: Text(
          'View Order Details',
          style: TextStyle(fontSize: 16.7,
            color: Colors.white,),
        ),
      ),
      body:msg == null ? Center(child: CircularProgressIndicator()): Stack(
      // body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              // Container(
              //   color: Colors.white,
              //   padding: EdgeInsets.symmetric(vertical: 12.0),
              //   child: ListTile(
              //     leading: Image.asset(
              //       'images/user.png',
              //       scale: 2.5,
              //       height: 42.3,
              //       width: 33.7,
              //
              //     ),
              //     // title:text( name,
              //     //     textColor: kMainColor,
              //     //     // fontFamily: fontRegular,
              //     //     textAllCaps: true,
              //     //     // fontSize: textSizeSMedium,
              //     //     maxLine: 2),
              //     // style: Theme.of(context)
              //     //     .textTheme
              //     //     .headline4
              //     //     .copyWith(fontSize: 13.3, letterSpacing: 0.07),
              //     // ),
              //     // subtitle: text(date,
              //     //     textColor:t1_textColorSecondary ,
              //         // fontSize: textVerySmallSize
              //     // ),
              //     // trailing: FittedBox(
              //     //   fit: BoxFit.fill,
              //     //   child: Row(
              //     //     children: <Widget>[
              //     //       IconButton(
              //     //         icon: Icon(
              //     //           Icons.phone,
              //     //           color: kMainColor,
              //     //           size: 18.0,
              //     //         ),
              //     //         onPressed: () => launch("tel://8109319168"),
              //     //       ),
              //     //     ],
              //     //   ),
              //     // ),
              //   ),
              // ),
              // Divider(
              //   color: kCardBackgroundColor,
              //   thickness: 8.0,
              // ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
                child: Text('ITEM(S)',
                    style: Theme.of(context).textTheme.headline6.copyWith(
                        color: kDisabledColor,
                        // fontFamily: fontBold,
                        fontWeight: FontWeight.bold)),
                color: Colors.white,
              ),
              Divider(
                color: kCardBackgroundColor,
                thickness: 8.0,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 6.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          physics: ClampingScrollPhysics(),
                          itemCount: new_order_info.length,
                          itemBuilder: (context, i) {
                         return
                           Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(top:0.0,bottom: 10.0,left: 5.0,right: 10.0),
                                  ),
                                  InkWell(
                                    child:Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                      Text(new_order_info[i].category_name,
                                            style: Theme.of(context).textTheme.headline6.copyWith(
                                                    color: kDisabledColor, fontWeight: FontWeight.bold)),

                                        SizedBox(height: 10.0),
                                        Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            // mainAxisSize: MainAxisSize.max,
                                            // mainAxisAlignment: MainAxisAlignment.end,
                                            children: <Widget>[
                                              Container(
                                                child :
                                                Text(new_order_info[i].product_name!=null?new_order_info[i].product_name:"xyz",
                                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                                  color: kDisabledColor, fontWeight: FontWeight.normal)),
                                              ),

                                              SizedBox(width: 70.0),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 5.0),
                                                child :
                                                // Text('Price : ${new_order_info[i].price}',
                                                //         style: Theme.of(context).textTheme.headline5.copyWith(
                                                //         color: Colors.green[800], fontWeight: FontWeight.normal)),
                                                Text('Price : ${new_order_info[i].price}',
                                                  style: TextStyle(
                                                    color: Colors.green[800],
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w800,
                                                    fontSize: 12.0,
                                                  ),
                                                ),
                                              ),
                                            ]
                                        ),
                                                SizedBox(height: 10),

                                        Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            // mainAxisSize: MainAxisSize.max,
                                            // mainAxisAlignment: MainAxisAlignment.end,
                                            children: <Widget>[
                                              Container(
                                                child :
                                                Text("Delivery Address : ",
                                                  style: Theme.of(context).textTheme.headline6.copyWith(
                                                  color: kDisabledColor, fontWeight: FontWeight.normal)),
                                              ),

                                              SizedBox(width: 0),
                                              Padding(padding: const EdgeInsets.only(right: 0.0), ),
                                              Expanded(
                                                child : Text(new_order_info[i].delivery_address,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    color: Colors.black54,
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w800,
                                                    fontSize: 12.0,

                                                  ),
                                                ),
                                              ),
                                            ]
                                        ),
                                        Divider(
                                          color: kCardBackgroundColor,
                                          thickness: 8.0,
                                        ),

                                        // Container(
                                        //   width: double.infinity,
                                        //   padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
                                        //   child: Text('DELIVERY ADDRESS',
                                        //       style: Theme.of(context).textTheme.headline6.copyWith(
                                        //           color: kDisabledColor,
                                        //           // fontFamily: fontBold,
                                        //           fontWeight: FontWeight.bold)),
                                        //   color: Colors.white,
                                        // ),
                                        // Container(
                                        //   width: double.infinity,
                                        //   padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
                                        //   child:
                                        //   text("new_order_info[i].delivery_address",
                                        //       textColor: kMainColor,
                                        //       // fontFamily: fontMedium,
                                        //       // fontSize: textSizeSmall,
                                        //       maxLine: 2),
                                        //   color: Colors.white,
                                        // ),


                                      ],
                                    ),

                                  ),

                                ]

                            );

                          }),

                    ],
                  ),
                ),
              ),
              Divider(
                color: kCardBackgroundColor,
                thickness: 8.0,
              ),
              // Container(
              //   height: 60,
              //   padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 21.7),
              //   child: Row(
              //     children: <Widget>[
              //       SizedBox(width: 10.0),
              //       Image.asset(
              //         'images/custom/ic_instruction.png',
              //         scale: 2.5,
              //         height: 15.7,
              //         width: 16.3,
              //       ),
              //       SizedBox(width: 18.0),
              //       Text(
              //         "descripition",
              //         style: Theme.of(context)
              //             .textTheme
              //             .caption
              //             .copyWith(fontSize: 11.7),
              //       )
              //     ],
              //   ),
              // ),
              Divider(
                color: kCardBackgroundColor,
                thickness: 8.0,
              ),


              // Container(
              //   width: double.infinity,
              //   padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
              //   child:
              //   Text('PAYMENT INFO',
              //
              //       style: Theme.of(context).textTheme.headline6.copyWith(
              //           color: kDisabledColor, fontWeight: FontWeight.bold)),
              //   color: Colors.white,
              // ),
              // Container(
              //   color: Colors.white,
              //   padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 20.0),
              //   child: Row(
              //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //       children: <Widget>[
              //         text('Item Total Price',
              //             textColor: kMainColor,
              //             // fontFamily: fontMedium,
              //             // fontSize: textSizeSmall,
              //             maxLine: 2),
              //         // Text(
              //         //   'Item Total Price',
              //         //   style: Theme.of(context).textTheme.caption,
              //         // ),
              //         // Icon(Icons.add),
              //         Text(
              //           "totalprice",
              //           style: Theme.of(context).textTheme.caption,
              //         ),
              //       ]),
              // ),
              // Divider(
              //   color: kCardBackgroundColor,
              //   thickness: 1.0,
              // ),
              // Container(
              //   color: Colors.white,
              //   padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 20.0),
              //   child: Row(
              //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //       children: <Widget>[
              //         Text(
              //           'Payment Method',
              //           style: Theme.of(context)
              //               .textTheme
              //               .caption
              //               .copyWith(fontWeight: FontWeight.bold),
              //         ),
              //         Text(
              //           "paymentmode",
              //           style: Theme.of(context).textTheme.caption,
              //         ),
              //       ]),
              // ),
              // SizedBox(
              //   height: 7.0,
              // ),
              // Container(
              //   height: 50.0,
              //   color: kCardBackgroundColor,
              // ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  color: kWhiteColor,
                  padding: EdgeInsets.all(8.0),
                  child: ListTile(
                    leading: CircleAvatar(
                      radius: 22.0,
                      backgroundImage: AssetImage('images/profile.png'),
                    ),
                    title: Text(
                      'George Anderson',
                      style: Theme.of(context).textTheme.headline4.copyWith(
                          fontSize: 15.0, fontWeight: FontWeight.w500),
                    ),
                    subtitle: Text(
                      'Delivery Partner Assigned',
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(fontSize: 11.7, letterSpacing: 0.06),
                    ),
                    trailing: IconButton(
                      icon: Icon(
                        Icons.navigation,
                        color: kMainColor,
                        size: 17.0,
                      ),

                    ),
                  ),
                ),
                // BottomBar(
                //     text: "Ready to Confirm Order",
                //     onTap: () {
                //       pr.show();
                //       ReadyApi(new_order_info[0].id);
                //
                //     })
              ],
            ),
          )
        ],
      ),
    );
  }

  // Api Integrate for product list
  String msg;
  NewOrderInfoApi(String OrderId) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    Map data = {
      'orders_id': OrderId,
    };
    print(data);

    var jsonResponse = null;
    var response = await http.post('https://gobunty.com/api/Payment/order_summary_view', body: data);
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
        print(jsonResponse);
        new_order_info.clear();

        if (jsonResponse ['msg'].toString().contains("Success")) {

          msg = jsonResponse ['msg'].toString();

          for (var data in jsonResponse['user_data']) {
            setState(() {
              new_order_info.add(

                new NewOrderInfo(
                    data['id'],
                    data['product_name'],
                    data['category_name'],
                    data['vendor_name'],
                    data['Vendor_address'],
                    data['delivery_address'],
                    data['price'],
                ),
              );
              print(new_order_info.length);
            });
          }
        }
      }
      setState(() {});
      print(jsonResponse);
    } else {
      pr.hide().whenComplete(() => Toast.show("Something Went Wrong", context));
      print('Something went wrong');
    }
  }
}
