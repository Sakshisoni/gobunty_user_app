import 'dart:convert';

import 'package:gobunty/Modals/ordermodal.dart';
import 'package:flutter/material.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:gobunty/Themes/style.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/order_placed_map.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:gobunty/HomeOrderAccount/Order/UI/OrderDetailPage.dart';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:gobunty/Components/FoodColors.dart';
import 'package:gobunty/Components/FoodConstant.dart';
import 'package:gobunty/Components/FoodWidget.dart';


class OrderPage extends StatefulWidget {
  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> with
    AutomaticKeepAliveClientMixin{
  List<ordermodal> list = [];
  String check;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getdata();
  }
  @override
  bool get wantKeepAlive =>true;
  getdata() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map data = {
      'user_id': pref.getString('uid'),
    };
    print(data);
    var response = await http.post(
        "https://gobunty.com/api/Payment/order_summary",
        body: data);
    if (response.statusCode == 200) {
      String responeBody = response.body;
      var jsonBody = json.decode(responeBody);
      Map<String, dynamic> map = json.decode(response.body);
      List<dynamic> data1 = map["user_data"];
      print(data1);
      if (data1 == null) {
        check="false";
      } else {
        for (var data in data1) {
          if (mounted) {
            setState(() {
              list.add(new ordermodal(
                  data['id'],
                  data['product_count'],
                  data['product name'],
                  data['category_name'],
                  data['vendor_name'],
                  data['Vendor_address'],
                  data['delivery_address'],
                  data['created_at'],
                  data['price'],
                  data['order_key']
                 ));
            });
          }
        }
      }
    } else {
      print('Something went wrong');
    }
  }

  BoxDecoration boxDecoration(
      {double radius = 2,
        Color color = Colors.transparent,
        Color bgColor = white,
        var showShadow = false}) {
    return BoxDecoration(
      color: bgColor,
      boxShadow: showShadow
          ? [BoxShadow(color: shadow_color, blurRadius: 2, spreadRadius: 2)]
          : [BoxShadow(color: Colors.transparent)],
      border: Border.all(color: color),
      borderRadius: BorderRadius.all(Radius.circular(radius)),
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('My Orders', style: Theme.of(context).textTheme.bodyText1),
        centerTitle: true,
      ),
      body:ListView(
          children: <Widget>[
            Container(
              height: 60.0,
              padding: EdgeInsets.symmetric(vertical: 18.7, horizontal: 20.3),
              color: kCardBackgroundColor,
              child: Text(
                'ORDER(S) IN PROCESS',
                style: Theme.of(context).textTheme.headline6.copyWith(
                    color: Color(0xff99a596),
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.67),
              ),
            ),
            //Center(child: Text("Your Order Is Dispatched Shortly If Any  Query Regrading Order Then Chat With Bunty."),),
            Container(
              child: SizedBox(
                height: 550,
                child: check == "false"
                    ? Center(
                        child: Text(
                          "No Items Found",
                          textAlign: TextAlign.center,
                        ),
                      )
                    : new ListView.builder(
                        itemCount: list.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        physics: AlwaysScrollableScrollPhysics(),
                        itemBuilder: (context, i) {
                          return  GestureDetector(
                              onTap: () {
                            // Toast.show("Currently Tracking Not Available", context);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => OrderInfo(OrderId:list[i].id
                                  // pageTitle: 'Vegetables & Fruits',
                                ),
                              ),
                            );
                           /* Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => OrderMapPage(
                                  pageTitle: 'Vegetables & Fruits',
                                ),
                              ),
                            );*/
                          },
                             child : Padding(
                                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                                  child: Container(
                                    decoration: boxDecoration(radius: 10, showShadow: true),
                                    child: Stack(
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.all(16),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  ClipRRect(
                                                  child: Image.asset(
                                                          'images/logo_user.png',
                                                          height: 42.3,
                                                          width: 33.7,
                                                        ),
                                                    borderRadius: BorderRadius.circular(12),
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      padding: EdgeInsets.only(left: 16),
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: <Widget>[
                                                          Row(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment.spaceBetween,
                                                            children: <Widget>[
                                                              text( list[i].cname!=null? list[i].cname:"Food",
                                                                  textColor: kMainTextColor,
                                                                  fontFamily: fontBold,
                                                                  fontSize: textSizeMedium,
                                                                  maxLine: 2),
                                                              text(list[i].time!=null?"₹"+list[i].time:"20 June 2020",
                                                                  fontSize: textSizeSmall,
                                                                textColor: kLightTextColor,),
                                                            ],
                                                          ),SizedBox(height: 10),
                                                          text(list[i].order_key!=null?"Order Key : "+list[i].order_key:"abccc",
                                                              fontSize: textSizeSmall,
                                                              textColor: kLightTextColor,
                                                              fontFamily: fontMedium),
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                ],
                                                mainAxisAlignment: MainAxisAlignment.start,
                                              ),
                                              SizedBox(
                                                height: 16,
                                              ),

                                     Row(
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: 36.0,
                                                    bottom: 6.0,
                                                    top: 12.0,
                                                    right: 12.0),
                                                child: ImageIcon(
                                                  AssetImage(
                                                      'images/custom/ic_pickup_pointact.png'),
                                                  size: 13.3,
                                                  color: kMainColor,
                                                ),
                                              ),

                                              Flexible(child:
                                              text(list[i].vname!=null &&list[i].vadd!=null? list[i].vname+" ("+list[i].vadd+")":"Name",
                                                  fontSize: textSizeSmall,
                                                  maxLine: 2,
                                                  textColor: kLightTextColor),
                                              // Text(
                                              //   list[i].vname!=null &&list[i].vadd!=null? list[i].vname+" ("+list[i].vadd+")":"Name",
                                              //   style: Theme.of(context)
                                              //       .textTheme
                                              //       .caption
                                              //       .copyWith(
                                              //           fontSize: 12.0,
                                              //           letterSpacing: 0.05),
                                              // ),
                                              ),
                                            ],
                                          ),
                                       Row(
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: 36.0,
                                                    bottom: 12.0,
                                                    top: 12.0,
                                                    right: 12.0),
                                                child: ImageIcon(
                                                  AssetImage(
                                                      'images/custom/ic_droppointact.png'),
                                                  size: 13.3,
                                                  color: kMainColor,
                                                ),
                                              ),
                                              Flexible(child:
                                              text(list[i].dadd!=null?  list[i].dadd:"Address",
                                                  fontSize: textSizeSmall,
                                                  maxLine: 2,
                                                  textColor: kLightTextColor),
                                              // Text(
                                              //   list[i].dadd!=null?  list[i].dadd:"Address",
                                              //   style: Theme.of(context)
                                              //       .textTheme
                                              //       .caption
                                              //       .copyWith(
                                              //           fontSize: 12.0,
                                              //           letterSpacing: 0.05),
                                              // ),
                                              ),
                                            ],
                                          ),
                                              // text("t1_samplelong_text",
                                              //     fontSize: textSizeMedium,
                                              //     maxLine: 2,
                                              //     textColor: kMainTextColor),
                                            ],
                                          ),
                                        ),
                                        // Container(
                                        //   width: 4,
                                        //   height: 35,
                                        //   margin: EdgeInsets.only(top: 16),
                                        //   color: pos % 2 == 0 ? t1TextColorPrimary : t1_colorPrimary,
                                        // )
                                      ],
                                    ),
                                  )),



                          // child: Card(
                          //   elevation: 5,
                          //   child:  Column(
                          //   children: <Widget>[
                          //     Row(
                          //       children: <Widget>[
                          //         Padding(
                          //           padding: const EdgeInsets.only(left: 16.3),
                          //           child: Image.asset(
                          //             'images/logo_user.png',
                          //             height: 42.3,
                          //             width: 33.7,
                          //           ),
                          //         ),
                          //         Expanded(
                          //           child: ListTile(
                          //             title: Text(
                          //               list[i].cname!=null? list[i].cname:"Food",
                          //               style: orderMapAppBarTextStyle.copyWith(
                          //                   letterSpacing: 0.07),
                          //             ),
                          //             subtitle: Text(
                          //               list[i].time!=null?list[i].time:"20 June 2020",
                          //               style: Theme.of(context)
                          //                   .textTheme
                          //                   .headline5
                          //                   .copyWith(
                          //                       fontSize: 11.7,
                          //                       letterSpacing: 0.06,
                          //                       color: Color(0xffc1c1c1)),
                          //             ),
                          //             trailing: Column(
                          //               mainAxisAlignment:
                          //                   MainAxisAlignment.center,
                          //               children: <Widget>[
                          //                 Text(
                          //                   'Pickup Arranged',
                          //                   style: orderMapAppBarTextStyle
                          //                       .copyWith(color: kMainColor),
                          //                 ),
                          //                 SizedBox(height: 7.0),
                          //                 Text(
                          //                  "Items "+list[i].pcount!=null?list[i].pcount:"2" +
                          //                       ' | ' +
                          //                      list[i].price!=null?list[i].price:" 100"+" Rs",
                          //                   style: Theme.of(context)
                          //                       .textTheme
                          //                       .headline6
                          //                       .copyWith(
                          //                           fontSize: 11.7,
                          //                           letterSpacing: 0.06,
                          //                           color: Color(0xffc1c1c1)),
                          //                 )
                          //               ],
                          //             ),
                          //           ),
                          //         )
                          //       ],
                          //     ),
                          //     Divider(
                          //       color: kCardBackgroundColor,
                          //       thickness: 1.0,
                          //     ),
                          //     // Coloumn(
                          //     //   child : text('OrderNo : ${order_item[index].order_key}',
                          //     //     fontSize: textSizeSMedium,
                          //     //     fontFamily: fontRegular,
                          //     //     maxLine: 2,
                          //     //     textColor: t1_textColorSecondary),),
                          //     Row(
                          //       children: <Widget>[
                          //         Padding(
                          //           padding: EdgeInsets.only(
                          //               left: 36.0,
                          //               bottom: 6.0,
                          //               top: 12.0,
                          //               right: 12.0),
                          //           child: ImageIcon(
                          //             AssetImage(
                          //                 'images/custom/ic_pickup_pointact.png'),
                          //             size: 13.3,
                          //             color: kMainColor,
                          //           ),
                          //         ),
                          //
                          //         Flexible(child:
                          //         Text(
                          //           list[i].vname!=null &&list[i].vadd!=null? list[i].vname+" ("+list[i].vadd+")":"Name",
                          //           style: Theme.of(context)
                          //               .textTheme
                          //               .caption
                          //               .copyWith(
                          //                   fontSize: 12.0,
                          //                   letterSpacing: 0.05),
                          //         ),),
                          //       ],
                          //     ),
                          //     Row(
                          //       children: <Widget>[
                          //         Padding(
                          //           padding: EdgeInsets.only(
                          //               left: 36.0,
                          //               bottom: 12.0,
                          //               top: 12.0,
                          //               right: 12.0),
                          //           child: ImageIcon(
                          //             AssetImage(
                          //                 'images/custom/ic_droppointact.png'),
                          //             size: 13.3,
                          //             color: kMainColor,
                          //           ),
                          //         ),
                          //         Flexible(child:
                          //         Text(
                          //           list[i].dadd!=null?  list[i].dadd:"Address",
                          //           style: Theme.of(context)
                          //               .textTheme
                          //               .caption
                          //               .copyWith(
                          //                   fontSize: 12.0,
                          //                   letterSpacing: 0.05),
                          //         ),),
                          //       ],
                          //     ),
                          //     Divider(
                          //       color: kCardBackgroundColor,
                          //       thickness:13.3,
                          //     ),
                          //   ],
                          // ),),


                          );
                        },
                      ),
              ),
            ),


          ],
        ),

    );
  }
}


