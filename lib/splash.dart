
import 'package:flutter/material.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  bool drop = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(
          "images/logo_user.png", //Go Bunty logo
          height: 130.0,
          width: 93.7,

        ),
      ),
    );
  }
}
/*

import 'dart:async';

import 'package:gobunty/Auth/MobileNumber/UI/mobile_input.dart';
import 'package:gobunty/main.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: SplashScreen(),
    ),
  );
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SplashState();
  }
}

class SplashState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: new Color(0xaad53899),
                gradient: LinearGradient(
                  colors: [new Color(0x33c10533), new Color(0xaad53899),new Color(0xabd3421a),],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                )),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:<Widget> [
              CircleAvatar(
                backgroundColor: Colors.white,
                radius:75.0,
                child: Icon(
                  Icons.beach_access,
                  color: Colors.deepPurple,
                  size: 50.0,
                ),
              ),
              Padding(padding: EdgeInsets.only(top:10.0),),
              Text("Umbrella Academy",
                  style:TextStyle(
                    color: Colors.deepPurple,
                    fontSize: 24.0,

                  )
              ),

            ],
          )
        ],
      ),
    );
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 5), onLoading);
  }

  onLoading() async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => MyApp()),
    );
  }
}
*/
