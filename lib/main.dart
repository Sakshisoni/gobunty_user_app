import 'package:gobunty/Themes/style.dart';
import 'package:gobunty/Auth/login_navigator.dart';
import 'package:gobunty/Locale/locales.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:gobunty/Themes/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Auth/login_navigator.dart';
import 'HomeOrderAccount/home_order_account.dart';
import 'Locale/locales.dart';


void main() {
  runApp(Phoenix(child: gobunty()));
}
class gobunty extends StatefulWidget {
  @override
  _gobuntyState createState() => _gobuntyState();
}

class _gobuntyState extends State<gobunty> {
  bool value=false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      getdata();
    });

  }
  getdata() async {
    SharedPreferences sharedPreferencesq1 = await SharedPreferences.getInstance();
    if(sharedPreferencesq1.getString('uid')!=null)
    {
      setState(() {
        value=true;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('hi'),
      ],
      theme: appTheme,
      home:LoginNavigator(),
      routes: PageRoutes().routes(),
    );
  }
}


