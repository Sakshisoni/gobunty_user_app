import 'dart:convert';

import 'package:gobunty/Auth/Verification/UI/verification_page.dart';
import 'package:gobunty/Components/bottom_bar.dart';
import 'package:gobunty/Components/entry_field.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import '../../login_navigator.dart';
import 'package:http/http.dart' as http;
//register page for registration of a new user
// ignore: must_be_immutable

class RegisterPage extends StatelessWidget {
  String text;
  RegisterPage({Key key, @required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.blue[900],
        title: Text(
          'Register',
          style: TextStyle(fontSize: 16.7,
          color: Colors.white,
          ),
        ),
      ),

      //this column contains 3 textFields and a bottom bar
      body: RegisterForm(text:text),
    );
  }
}

class RegisterForm extends StatefulWidget {
    RegisterForm({this.text});
    String text;

  @override
  _RegisterFormState createState() => _RegisterFormState(text:text);
}

class _RegisterFormState extends State<RegisterForm> {

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  // RegisterBloc _registerBloc;
  String text;
  _RegisterFormState({this.text});
  ProgressDialog pr;
  bool _isLoading;
  @override
  void initState() {
    super.initState();
    // _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _phoneController.text=text;
  }

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true,isDismissible: false);
    pr.style(message: 'Please wait...');
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 12.0),
          child: Column(
            children: <Widget>[
              Divider(
                color: kCardBackgroundColor,
                thickness: 8.0,
              ),
              //name textField
              EntryField(
                textCapitalization: TextCapitalization.words,
                controller: _nameController,
                label: 'FULL NAME',
                image: 'images/icons/ic_name.png',
              ),
              //email textField
              EntryField(
                textCapitalization: TextCapitalization.none,
                controller: _emailController,
                label: 'EMAIL ADDRESS',
                image: 'images/icons/ic_mail.png',
                keyboardType: TextInputType.emailAddress,
              ),

              //phone textField
              EntryField(
                label: 'PHONE NUMBER',
                image: 'images/icons/ic_phone.png',
                controller: _phoneController,
                keyboardType: TextInputType.number,
              ),

              Text(
                "We'll send verification code on above given number.",
                style: Theme.of(context).textTheme.headline6,
              ),
            ],
          ),
        ),

        //continue button bar
        BottomBar(
            text: "Continue",
            onTap: () {
              pr.show();
              final bool isValid = EmailValidator.validate(_emailController.text);
              if(isValid&&_nameController.text.length>2) {
                signIn(_phoneController.text, _emailController.text,
                    _nameController.text);
              }
              else
                {
                  pr.hide();
                  Toast.show("Fill Valid Information",context);
                }

            })
      ],
    );
  }
  signIn(String phone,email,name) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map data = {
      'full_name':name,
      'phone_number': phone,
      'email_id':email,
      'device_token':"3643638476",
    };
    print (data);
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/user/user_register", body: data);

    if(response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        print(jsonResponse);
         sharedPreferences.setString("token", phone);
        if(jsonResponse['msg']=="Success") {
          Navigator.pop(context);
          pr.hide().whenComplete(() =>
              Navigator.push(context,MaterialPageRoute(builder:(context)=> VerificationPage(text:phone))));
        }
      }
    }
    else {
      pr.hide();
      Toast.show("Connection Error", context);
      setState(() {
        _isLoading = false;
      });
      print(response.body);
    }
  }
}
