import 'dart:async';
import 'dart:convert';

import 'package:gobunty/Components/bottom_bar.dart';
import 'package:gobunty/Components/entry_field.dart';
import 'package:gobunty/HomeOrderAccount/home_order_account.dart';
import 'package:gobunty/Maps/UI/location_page.dart';
import 'package:gobunty/Modals/UserModal.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

List<UserModal> _user = [];
//Verification page that sends otp to the phone number entered on phone number page
class VerificationPage extends StatelessWidget {

String text;
  VerificationPage({ this.text});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: kMainColor,
        elevation: 0.0,
        title: Text(
          'Verification',
          style: TextStyle(fontSize: 16.7 ,
          color: Colors.white),
        ),
      ),
      body: OtpVerify(text),
    );
  }
}

//otp verification class
class OtpVerify extends StatefulWidget {

String text;
  OtpVerify(this.text);


  @override
  _OtpVerifyState createState() => _OtpVerifyState(text);
}

class _OtpVerifyState extends State<OtpVerify> {
  final TextEditingController _controller = TextEditingController();
  String phone;
  _OtpVerifyState(this.phone);
  // VerificationBloc _verificationBloc;
  bool isDialogShowing = false;
  int _counter = 60;
  Timer _timer;
  ProgressDialog pr;
  _startTimer() {
    //shows timer
    _counter = 60; //time counter

    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _counter > 0 ? _counter-- : _timer.cancel();
      });
    });
  }

  @override
  void initState() {
    super.initState();
    verifyPhoneNumber();
  }

  getUserDetails(String text) async {
    SharedPreferences pref=await SharedPreferences.getInstance();

    Map data = {
      'otp': text,
      'mobile':phone,
    };
    var response = await http.post("https://gobunty.com/api/user/user_verify_otp",body:data);
    if (response.statusCode == 200) {
      String responeBody = response.body;
      var jsonBody = json.decode(responeBody);
      print(jsonBody);
      if(jsonBody['msg'].toString().contains("Success")) {
        var data = jsonBody['user_data'];
        _user.add(new UserModal(
            data['id'], data['name'], data['email'], data['mobile'],
            data['device_token']),);

        pref.setString("uid", jsonBody['user_data']['id']);
        pref.setString("mobile", jsonBody['user_data']['mobile']);
        Navigator.pop(context);

        pr.hide().whenComplete(() =>
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => LocationPage(),
            )));
        setState(() {});
      }
      else
        {
          pr.hide();
          Toast.show("Wrong Otp",context);
        }

    } else {
      print('Something went wrong');
    }


  }
  void verifyPhoneNumber() {
    //verify phone number method using otp
    _startTimer();

    //int resendToken;
    //_verificationBloc = BlocProvider.of<VerificationBloc>(context);
//    FirebaseAuth.instance.verifyPhoneNumber(
//      phoneNumber: widget.phoneNumber,
//      timeout: Duration(seconds: 5),
//      verificationCompleted: (AuthCredential authCredential) =>
//          _verificationBloc.add(VerificationSuccessEvent(
//              authCredential, widget.name, widget.email)),
//
//      //This callback would gets called when verification is done automatically
//      verificationFailed: (AuthException authException) =>
//          _verificationBloc.add(VerificationFailureEvent(authException)),
//      codeSent: (String verId, [int forceCodeResend]) {
//        resendToken = forceCodeResend;
//        _verificationBloc.add(PhoneCodeSentEvent(verId));
//      },
//      codeAutoRetrievalTimeout: (String verId) =>
//          _verificationBloc.add(CodeAutoRetrievalTimeoutEvent(verId)),
//      forceResendingToken: resendToken,
//    );
  }

  @override
  void dispose() {
    _controller.dispose();
    _timer.cancel();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true,isDismissible: false);
    pr.style(message: 'Please wait...');
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Divider(
          color: kCardBackgroundColor,
          thickness: 8.0,
        ),
        Padding(
          padding: EdgeInsets.all(20.0),
          child: Text(
            "Enter verification code we've sent on given number.",
            style: Theme.of(context)
                .textTheme
                .headline6
                .copyWith(fontSize: 22, color: Colors.black87),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 16.0, right: 20.0),
          child: EntryField(
            controller: _controller,
            readOnly: false,
            label: 'ENTER VERIFICATION CODE',
            maxLength: 6,
            keyboardType: TextInputType.number,
          ),
        ),
        Spacer(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 16.0),
              child: Text(
                '$_counter sec',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            FlatButton(
                shape: RoundedRectangleBorder(side: BorderSide.none),
                padding: EdgeInsets.all(24.0),
                disabledTextColor: kDisabledColor,
                textColor: kMainColor,
                child: Text(
                  "Resend",
                  style: TextStyle(
                    fontSize: 16.7,
                  ),
                ),
                onPressed: _counter < 1
                    ? () {
                        verifyPhoneNumber();
                      }
                    : null),
          ],
        ),

        BottomBar(
            text: "Continue",
            onTap: () {

             /* Navigator.pop(context);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeOrderAccount(),
                  ));*/

              if (isValidPhoneNumber(_controller.text)) {
                pr.show();
                getUserDetails(_controller.text);
              }
              else {
                pr.hide();
                Toast.show("Invalid OTP", context);
              }

                      // getUserDetails(_controller.text);
//                  _verificationBloc.add(SubmittedEvent(
//                  _controller.text, widget.name, widget.email));
            }),
      ],
    );
  }


  bool isValidPhoneNumber(String otptext) {
    // You may need to change this pattern to fit your requirement.
    // I just copied the pattern from here: https://regexr.com/3c53v
    // final pattern = r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
    // final regExp = RegExp(pattern);

    if (otptext == null || otptext.isEmpty || otptext.length != 4) {
      return false;
    }
    // if (!regExp.hasMatch(phoneNumber)) {
    //   return false;
    // }
    return true;
  }

}
