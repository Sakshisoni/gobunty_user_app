import 'dart:convert';

import 'package:gobunty/Auth/Registration/UI/register_page.dart';
import 'package:gobunty/Auth/Verification/UI/verification_page.dart';
import 'package:gobunty/Auth/login_navigator.dart';
import 'package:gobunty/Components/entry_field.dart';
import 'package:gobunty/HomeOrderAccount/Home/UI/home.dart';
import 'package:gobunty/HomeOrderAccount/home_order_account.dart';
import 'package:gobunty/Locale/locales.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:location/location.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class MobileInput extends StatefulWidget {
  @override
  _MobileInputState createState() => _MobileInputState();
}

class _MobileInputState extends State<MobileInput> {
  final TextEditingController _controller = TextEditingController();
  ProgressDialog pr;

  //MobileBloc _mobileBloc;
  String isoCode;
  bool _isLoading=false;
  Location location = new Location();
  PermissionStatus _permission;


  LocationData locationData;


  @override
  void initState() {
    super.initState();
    getdata();

    //_mobileBloc = BlocProvider.of<MobileBloc>(context);
  }

  void _getLocationPermission() async {
    bool enable;
    enable = await location.serviceEnabled();
    if (!enable) {
      enable = await location.requestService();
      if (!enable) {
        return;
      }
    }
    _permission = await location.hasPermission();
    if (_permission == PermissionStatus.DENIED) {
      _permission = await location.requestPermission();
      if (_permission != PermissionStatus.GRANTED) {
        return;
      }
    }
    locationData = await location.getLocation();
    print(locationData.toString());



  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true,isDismissible: false);
    pr.style(message: 'Please wait...');
    return Row(
      children: <Widget>[
       /* CountryCodePicker(
          onChanged: (value) {
            isoCode = value.code;
          },
          builder: (value) => buildButton(value),
          initialSelection: '+91',
          textStyle: Theme.of(context).textTheme.caption,
          showFlag: false,
          showFlagDialog: true,
          favorite: ['+91', 'INDIA'],
        ),*/
        SizedBox(
          width: 10.0,
        ),
        //takes phone number as input
        Expanded(
          child: EntryField(
            controller: _controller,
            keyboardType: TextInputType.number,
            readOnly: false,
            hint: AppLocalizations.of(context).mobileText,
            maxLength: 10,
            border: InputBorder.none,
          ),

        ),

        //if phone number is valid, button gets enabled and takes to register screen
        RaisedButton(
          child: Text(
            AppLocalizations.of(context).continueText,
            style: Theme.of(context).textTheme.button,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          onPressed: () {
            pr.show();
            if(isValidPhoneNumber(_controller.text)) {

              signIn(_controller.text);
            }
            else{
              pr.hide();
              Toast.show("Invalid Mobile No.",context);
            }

           // Navigator.pushNamed(context, LoginRoutes.registration);
//              _mobileBloc.add(SubmittedEvent(
//                  isoCode: isoCode, mobileNumber: _controller.text));
          },
        ),
      ],
    );
  }
  bool isValidPhoneNumber(String phoneNumber) {
    // You may need to change this pattern to fit your requirement.
    // I just copied the pattern from here: https://regexr.com/3c53v
    final pattern = r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
    final regExp = RegExp(pattern);

    if (phoneNumber == null || phoneNumber.isEmpty||phoneNumber.length!=10) {
      return false;
    }

    if (!regExp.hasMatch(phoneNumber)) {
      return false;
    }
    return true;
  }
  void goToNextScreen(
      bool isRegistered, String normalizedPhoneNumber, BuildContext context) {
    if (isRegistered) {
      Navigator.pushNamed(
        context,
        LoginRoutes.verification,
      );
    } else {
      Navigator.pushNamed(
        context,
        LoginRoutes.registration,
      );
    }
  }
   getdata() async {
    SharedPreferences sharedPreferencesq1 = await SharedPreferences.getInstance();
    if(sharedPreferencesq1.getString('uid')!=null)
      {
        Navigator.pop(context);
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => HomeOrderAccount(),
            ));
      }
    else
      {
        _getLocationPermission();
      }
  }
  signIn(String phone) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map data = {
      'phone_number': phone,
    };
    var jsonResponse = null;
    var response = await http.post("https://gobunty.com/api/user/user_login", body: data);

    print(response.statusCode);
    if(response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
        print(jsonResponse);
        sharedPreferences.setString("token", phone);
        if (jsonResponse['msg'] == "Success") {
          pr.hide().whenComplete(() =>
              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                  VerificationPage(text: _controller.text))));
        }
      }
    }
    else  if(response.statusCode == 400) {
      Navigator.pop(context);
      pr.hide().whenComplete(() =>
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => RegisterPage(text: _controller.text),
              )));
    }
    else {
      setState(() {
        _isLoading = false;
      });
      print(response.body);
    }
  }


}


