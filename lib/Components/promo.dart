import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gobunty/Components/FoodColors.dart';
import 'package:gobunty/Components/FoodConstant.dart';
import 'package:gobunty/Components/FoodWidget.dart';
import 'package:gobunty/Modals/promo_code.dart';
import 'package:gobunty/Pages/view_cart.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class FoodCoupon extends StatefulWidget {
  static String tag = '/FoodCoupon';
  String vid;
  FoodCoupon({this.vid});

  @override
  FoodCouponState createState() => FoodCouponState(vid);
}

class FoodCouponState extends State<FoodCoupon> {
  List<Coupons> mList;
  String vid,id,title;
  FoodCouponState(this.vid);
  TextEditingController controller=new TextEditingController();

  @override
  void initState() {
    super.initState();
    getData();
  }


  List<PromoCode> modal = [];
  ScrollController controller1 = ScrollController();
  void getData() async {
    Map data1 = {
      'vendor_id':vid,
    };
    var jsonResponse = null;
    var response = await http.post(
        "https://gobunty.com/api/Product/promocode_list",body: data1
    );

    if (response.statusCode == 200) {
      Map data = json.decode(response.body);
      if(data['status']==true) {
        for (var val in data['vendor_data']) {
          setState(() {
            modal.add(new PromoCode(
                val['id'], val['title'], val['description'], val['promo_code'],
                val['value']));
          });
        }
      }
      else
      {
        Toast.show("No Promo Code Available", context);
      }
      print(response);
    }
    else if (response.statusCode == 404) {
      Toast.show("No Promo Code Available", context);
    }
    else {
      Toast.show("Connection Error", context);
    }

    print(response.body);
  }

  @override
  Widget build(BuildContext context) {

    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: food_app_background,
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: food_textColorPrimary,
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
                child: Container(
              margin: EdgeInsets.only(left: spacing_standard_new, right: spacing_standard_new),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  text("Apply Code", fontFamily: fontBold, fontSize: textSizeLarge),
                  SizedBox(
                    height: spacing_standard_new,
                  ),
                  Container(
                    decoration: boxDecoration(color: food_colorPrimary, showShadow: true, radius: 50),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: TextField(
                            controller: controller,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(14.0),
                              isDense: true,
                              hintText: "Enter PromoCode ",
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () {
                                if(controller.text!=null)
                                  {
                                for(int i=0;i<modal.length;i++)
                                  {
                                    if(controller.text==modal[i].code)
                                      {
                                        id=modal[i].id;
                                        title=modal[i].name;
                                      }
                                  }}
                                if(id!=null) {
                                  Navigator.pop(context);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          ViewCart(add: "Home",
                                              promoid: id,
                                              title: title,
                                              value: true)
                                      ,
                                    ),
                                  );
                                }
                                else
                                  {
                                    Toast.show("Invalid Code", context);
                                  }
                              },
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                decoration: gradientBoxDecoration(radius: 50, gradientColor1: food_color_blue_gradient1, gradientColor2: food_color_blue_gradient2),
                                child: Icon(
                                  Icons.arrow_forward,
                                  color: food_white,
                                ),
                              ),
                            ))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: spacing_standard_new,
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        height: 0.6,
                        color: food_view_color,
                        width: width * 0.1,
                        margin: EdgeInsets.only(right: spacing_standard),
                      ),
                      text("Use Your Mobile and Email", fontSize: textSizeSMedium, textColor: food_textColorSecondary, textAllCaps: true),
                      Container(
                        height: 0.6,
                        color: food_view_color,
                        width: width * 0.1,
                        margin: EdgeInsets.only(left: spacing_standard),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: spacing_standard_new,
                  ),
                  ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: modal.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return Coupon(modal[index], index);
                      }),
                ],
              ),
            )),
          )
        ],
      )),
    );
  }
}
class Coupons {
var couponsName;
var info;
var offer;

Coupons(this.couponsName, this.info, this.offer);
}

class Coupon extends StatelessWidget {
  PromoCode model;

  Coupon(PromoCode model, int pos) {
    this.model = model;
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            //borderRadius: BorderRadius.all(Radius.circular(spacing_middle)),
            child: Container(
              width: width * 0.3,
              color: food_color_light_primary,
              child: text(model.code, textColor: food_colorAccent, isCentered: true),
            ),
          ),
          SizedBox(
            height: spacing_control,
          ),
          text(model.name, fontFamily: fontMedium, fontSize: textSizeLargeMedium),
          text(model.description, textColor: food_textColorSecondary, isLongText: true),
          Container(
            height: 0.5,
            color: food_view_color,
            width: width,
            margin: EdgeInsets.only(top: spacing_standard_new, bottom: spacing_standard_new),
          )
        ],
      ),
    );
  }
}
