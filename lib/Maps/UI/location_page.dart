import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:gobunty/Components/bottom_bar.dart';
import 'package:gobunty/Components/custom_appbar.dart';
import 'package:gobunty/Components/entry_field.dart';
import 'package:gobunty/HomeOrderAccount/home_order_account.dart';
import 'package:gobunty/Maps/Components/address_type_button.dart';
import 'package:gobunty/Routes/routes.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:geocoder/geocoder.dart';
//import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:search_map_place/search_map_place.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:toast/toast.dart';
import 'dart:async';
import 'package:google_maps_webservice/places.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter/material.dart';
import 'dart:math';


const kGoogleApiKey = "AIzaSyC14egQ8aT1JC702KV9VCQdhWYtXBOS2a8";

// to get places detail (lat/lng)
GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class LocationPage extends StatelessWidget {
  bool bool1;
  LocationPage({this.bool1});

  @override
  Widget build(BuildContext context) {
    return SetLocation(bool1);
  }
}

enum AddressType {
  Home,
  Office,
  Other,
}
AddressType selectedAddress = AddressType.Other;
final homeScaffoldKey = GlobalKey<ScaffoldState>();

class SetLocation extends StatefulWidget {
  bool bool1;

  SetLocation(this.bool1);
  @override
  _SetLocationState createState() => _SetLocationState(bool1);
}

class _SetLocationState extends State<SetLocation> {
  bool isCard = false;
  Set<Marker> _markers = HashSet<Marker>();
  Set<Polygon> _polygons = HashSet<Polygon>();
  Set<Polyline> _polylines = HashSet<Polyline>();
  Set<Circle> _circles = HashSet<Circle>();
  bool _showMapStyle = false;
  // String lon = "", lat = "";
  String address = "";
  GoogleMapController _mapController;
  BitmapDescriptor _markerIcon;
  bool  bool1;
  ProgressDialog pr;
  _SetLocationState(this.bool1);
  // Location location = new Location();
  PermissionStatus _permission;

  TextEditingController _addressController = TextEditingController();

   String Address;
   double lat,lng;
   LocationData locationData;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {


      // _getLocationPermission();
    });

  }
  //API CALLING
String value="";
  send() async {
    SharedPreferences pref=await SharedPreferences.getInstance();

    print(value);
    if(selectedAddress==AddressType.Home)
      {
        value="Home";
      }
    if(selectedAddress==AddressType.Office)
      {
        value="Office";
      }
    if(selectedAddress==AddressType.Other)
      {
        value="Other";
      }
    Map data = {
      'address':Address,
      'latitude':lat.toString(),
      'longitude':lng.toString(),
      'type':value,
      'user_id': pref.getString('uid'),
    };
    print(data);
    var response;

    response = await http.post("https://gobunty.com/api/user_Delivery/add_address",
          body: data);

    if (response.statusCode == 200) {
      String responeBody = response.body;
      var jsonBody = json.decode(responeBody);

      var data = jsonBody['msg'];
      print(data);
      if(data.toString().contains("Success")) {
        Navigator.pop(context);
        pr.hide().whenComplete(() =>

        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => HomeOrderAccount(),
            )));
      }
    } else {
      pr.hide().whenComplete(() => Toast.show("Something Went Wrong",context));
      print('Something went wrong');
    }


  }
  // void _getLocationPermission() async {
  //   bool enable;
  //   enable = await location.serviceEnabled();
  //   if (!enable) {
  //     enable = await location.requestService();
  //     if (!enable) {
  //       return;
  //     }
  //   }
  //   _permission = await location.hasPermission();
  //   if (_permission == PermissionStatus.DENIED) {
  //     _permission = await location.requestPermission();
  //     if (_permission != PermissionStatus.GRANTED) {
  //       return;
  //     }
  //   }
  //   locationData = await location.getLocation();
  //   print(locationData.toString());
  //   setState(() {
  //     lat=locationData.latitude.toString();
  //     lon= locationData.longitude.toString();
  //     getstreet(
  //         locationData.latitude.toString(), locationData.longitude.toString());
  //   });
  //
  //
  // }
  // void getstreet(String lat, String lon) async {
  //   _setMarkerIcon();
  //   _setPolygons();
  //   _setPolylines();
  //   _setCircles();
  //   final coordinates = new Coordinates(
  //       double.parse(lat.toString()), double.parse(lon.toString()));
  //   var addresses =
  //   await Geocoder.local.findAddressesFromCoordinates(coordinates);
  //   var first = addresses.first;
  //
  //   setState(() {
  //     address = "${first.addressLine}";
  //   });
  // }

/*
  void _getlocation() async {
    final geopos = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if(mounted) {
      setState(() {
        lat = '${geopos.latitude}';
        lon = '${geopos.longitude}';
      });
    }
    double fat = double.parse(lat);
    double fon = double.parse(lon);
    final coordinates = new Coordinates(fat, fon);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    pr.hide();
    if(mounted) {
      setState(() {
        address = "${first.addressLine}";
      });
    }
    // print("${first.featureName} : ${first.addressLine}");
  }*/

  void _setMarkerIcon() async {
    _markerIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'assets/noodle_icon.png');
  }

  void _toggleMapStyle() async {
    String style = await DefaultAssetBundle.of(context)
        .loadString('assets/map_style.json');

    if (_showMapStyle) {
      _mapController.setMapStyle(style);
    } else {
      _mapController.setMapStyle(null);
    }
  }

  void _setPolygons() {
    List<LatLng> polygonLatLongs = List<LatLng>();
    polygonLatLongs.add(LatLng(37.78493, -122.42932));
    polygonLatLongs.add(LatLng(37.78693, -122.41942));
    polygonLatLongs.add(LatLng(37.78923, -122.41542));
    polygonLatLongs.add(LatLng(37.78923, -122.42582));

    _polygons.add(
      Polygon(
        polygonId: PolygonId("0"),
        points: polygonLatLongs,
        fillColor: Colors.white,
        strokeWidth: 1,
      ),
    );
  }

  void _setPolylines() {
    List<LatLng> polylineLatLongs = List<LatLng>();
    polylineLatLongs.add(LatLng(37.74493, -122.42932));
    polylineLatLongs.add(LatLng(37.74693, -122.41942));
    polylineLatLongs.add(LatLng(37.74923, -122.41542));
    polylineLatLongs.add(LatLng(37.74923, -122.42582));

    _polylines.add(
      Polyline(
        polylineId: PolylineId("0"),
        points: polylineLatLongs,
        color: Colors.purple,
        width: 1,
      ),
    );
  }

  void _setCircles() {
    _circles.add(
      Circle(
          circleId: CircleId("0"),
          center: LatLng(37.76493, -122.42432),
          radius: 1000,
          strokeWidth: 2,
          fillColor: Color.fromRGBO(102, 51, 153, .5)),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;

    setState(() {
      _markers.add(
        Marker(
            markerId: MarkerId("0"),
            position: LatLng(37.77483, -122.41942),
            infoWindow: InfoWindow(
              title: "San Francsico",
              snippet: "An Interesting city",
            ),
            icon: _markerIcon),
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    // Mode _mode = Mode.overlay;
    pr = new ProgressDialog(context, showLogs: true,isDismissible: false);
    pr.style(message: 'Please wait...');

    return Scaffold(
//          extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
//              boxShadow: BoxShadow(
//                color: Colors.grey.shade600,
//                offset: Offset(0.0, 2.0),
//                blurRadius: 5.0,
//              ),
//              color: Colors.white,
          title: Text(
            'Set delivery location',
            style: TextStyle(fontSize: 16.7, color: Colors.black),
          ),
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              color: kMainColor,
             onPressed: () => Navigator.of(context).pop()
    ),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            height: 8.0,
          ),
          Expanded(
            child: Stack(
              children: <Widget>[

                Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: SizedBox(
                    height: 600.0,
                    child: GoogleMap(
                        onMapCreated: _onMapCreated,
                        myLocationEnabled: true,
                        myLocationButtonEnabled: true,
                        initialCameraPosition: CameraPosition(
                          target: LatLng(
                            22.7560008,
                            75.9012473,
                          ),
                          zoom: 15,
                        )),
                  ),
                ),
              ],
            ),
          ),
          Container(
            color: kCardBackgroundColor,
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            child: Row(
              children: <Widget>[
                Image.asset(
                  'images/map_pin.png',
                  scale: 2.5,
                ),
                SizedBox(
                  width: 16.0,
                ),
                Expanded(
                  child: Text(
                    address,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.caption,
                  ),
                ),
              ],
            ),
          ),
          isCard ?   Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: EntryField(
                  controller: _addressController,
                  hint: 'ADDRESS',
                  readOnly: true,
                  onTap: _handlePressButton,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: Text(
                  'SAVE ADDRESS AS',
                  style: Theme
                      .of(context)
                      .textTheme
                      .subtitle2,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    AddressTypeButton(
                      label: 'Home',
                      image: 'images/address/ic_homeblk.png',
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            selectedAddress = AddressType.Home;
                          });
                        }
                      },
                      isSelected: selectedAddress == AddressType.Home,
                    ),
                    AddressTypeButton(
                      label: 'Office',
                      image: 'images/address/ic_officeblk.png',
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            selectedAddress = AddressType.Office;
                          });
                        }
                      },
                      isSelected: selectedAddress == AddressType.Office,
                    ),
                    AddressTypeButton(
                      label: 'Other',
                      image: 'images/address/ic_otherblk.png',
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            selectedAddress = AddressType.Other;
                          });
                        }
                      },
                      isSelected: selectedAddress == AddressType.Other,
                    ),
                  ],
                ),
              )
            ],
          ) : Container(),
          BottomBar(
              text: "Continue",
              onTap: () {

                if (isCard == false) {
                  if(mounted) {
                    setState(() {
                      send();
                      // _addressController.text = address;
                      // getstreet();
                      isCard = true;
                    });
                  }
                } else {
                  pr.show();
                  send();
                }
              }),
        ],
      ),
    );
  }

  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      onError: onError,
      mode: Mode.overlay,
      language: "en",
      // language: "en",
      components: [Component(Component.country, "in")],
    );

    displayPrediction(p, homeScaffoldKey.currentState);
  }

  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);

      lat = detail.result.geometry.location.lat ;
      lng = detail.result.geometry.location.lng ;

      print(lat);
      print(lng);
      // print(detail.result.geometry.location);
      print(p.description);
      Address = p.description;

        setState(() {
          _addressController.text = p.description;


          print(_addressController.text);
        });

      // scaffold.showSnackBar(
      //   SnackBar(content: Text("${p.description} - $lat/$lng")),
      //
      // );
    }
  }

  void onError(PlacesAutocompleteResponse response) {
    Toast.show("errrorrrrrrrrrr", context);
    // homeScaffoldKey.currentState.showSnackBar(
    //   SnackBar(content: Text(response.errorMessage)),
    // );
  }


}
//
// enum AddressType {
//   Home,
//   Office,
//   Other,
// }
// AddressType selectedAddress = AddressType.Other;
// final homeScaffoldKey = GlobalKey<ScaffoldState>();
//
// class SaveAddressCard extends StatefulWidget {
//   @override
//   _SaveAddressCardState createState() => _SaveAddressCardState();
// }
//
// class _SaveAddressCardState extends State<SaveAddressCard> {
//   Mode _mode = Mode.overlay;
//   @override
//   Widget build(BuildContext context) {
//     return
//       Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: <Widget>[
//         Padding(
//           padding: EdgeInsets.symmetric(horizontal: 8.0),
//           child: EntryField(
//             controller: _addressController,
//             label: 'FLAT NUM, LANDMARK, APARTMENT, ETC.',
//             readOnly: true,
//             onTap: _handlePressButton,
//           ),
//         ),
//         Padding(
//           padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
//           child: Text(
//             'SAVE ADDRESS AS',
//             style: Theme
//                 .of(context)
//                 .textTheme
//                 .subtitle2,
//           ),
//         ),
//         Padding(
//           padding: EdgeInsets.only(bottom: 16.0),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: <Widget>[
//               AddressTypeButton(
//                 label: 'Home',
//                 image: 'images/address/ic_homeblk.png',
//                 onPressed: () {
//                   if (mounted) {
//                     setState(() {
//                       selectedAddress = AddressType.Home;
//                     });
//                   }
//                 },
//                 isSelected: selectedAddress == AddressType.Home,
//               ),
//               AddressTypeButton(
//                 label: 'Office',
//                 image: 'images/address/ic_officeblk.png',
//                 onPressed: () {
//                   if (mounted) {
//                     setState(() {
//                       selectedAddress = AddressType.Office;
//                     });
//                   }
//                 },
//                 isSelected: selectedAddress == AddressType.Office,
//               ),
//               AddressTypeButton(
//                 label: 'Other',
//                 image: 'images/address/ic_otherblk.png',
//                 onPressed: () {
//                   if (mounted) {
//                     setState(() {
//                       selectedAddress = AddressType.Other;
//                     });
//                   }
//                 },
//                 isSelected: selectedAddress == AddressType.Other,
//               ),
//             ],
//           ),
//         )
//       ],
//     );
//   }
//
//
// }
