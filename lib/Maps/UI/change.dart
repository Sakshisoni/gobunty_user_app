
import 'package:gobunty/Components/bottom_bar.dart';
import 'package:gobunty/Maps/Components/address_type_button.dart';
import 'package:gobunty/Pages/view_cart.dart';
import 'package:gobunty/Themes/colors.dart';
import 'package:flutter/material.dart';

enum AddressType {
  Home,
  Office,
  Other,
}
AddressType selectedAddress = AddressType.Other;

class change extends StatefulWidget {
  @override
  _SaveAddressCardState createState() => _SaveAddressCardState();
}

class _SaveAddressCardState extends State<change> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
        Text('Select Address', style: Theme.of(context).textTheme.bodyText1),
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            color: kMainColor,
            onPressed: () => Navigator.of(context).pop()),
      ),
      body:
      Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          child: Text(
            'SAVE ADDRESS AS',
            style: Theme.of(context).textTheme.subtitle2,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              AddressTypeButton(
                label: 'Home',
                image: 'images/address/ic_homeblk.png',
                onPressed: () {
                  if(mounted) {
                    setState(() {
                      selectedAddress = AddressType.Home;
                    });
                  }
                },
                isSelected: selectedAddress == AddressType.Home,
              ),
              AddressTypeButton(
                label: 'Office',
                image: 'images/address/ic_officeblk.png',
                onPressed: () {
                  if(mounted) {
                    setState(() {
                      selectedAddress = AddressType.Office;
                    });
                  }
                },
                isSelected: selectedAddress == AddressType.Office,
              ),
              AddressTypeButton(
                label: 'Other',
                image: 'images/address/ic_otherblk.png',
                onPressed: () {
                  if(mounted) {
                    setState(() {
                      selectedAddress = AddressType.Other;
                    });
                  }
                },
                isSelected: selectedAddress == AddressType.Other,
              ),
            ],
          ),
        ),
        BottomBar(
            text: "Continue",
            onTap: () {
              String addr;
              if(selectedAddress==AddressType.Home)
                {
                  addr="Home";
                }
              if(selectedAddress==AddressType.Office)
              {
                addr="Office";
              }
              if(selectedAddress==AddressType.Other)
              {
                addr="Other";
              }
              Navigator.pop(context);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ViewCart(add:addr)));
            }),
      ],
    ));
  }
}
