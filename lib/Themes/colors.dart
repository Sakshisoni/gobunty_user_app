import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
// import 'dart:ui';
//Color kMainColor = Color(0xff1c437e);
Color kMainColor =Colors.blue;
Color kDisabledColor = Color(0xff616161);
Color kWhiteColor = Colors.white;
Color kLightTextColor = Colors.black38;
Color kCardBackgroundColor = Color(0xfff8f9fd);
Color kTransparentColor = Colors.transparent;
Color kMainTextColor = Color(0xff000000);
Color kIconColor = Color(0xffc4c8c1);
Color kHintColor = Color(0xff999e93);
Color kBarColor = Color(0xff5104d7);
Color t5ColorPrimaryDark = Color(0xFF325BF0);
const textPrimaryColor = Color(0xFF2E3033);
const appWhite = Color(0xFFFFFFFF);
const appColorPrimary = Color(0xFF0A79DF);
const Color white = const Color(0xFFFFFFFF);
const textSecondaryColor = Color(0xFF757575);
const t1_textColorSecondary = Color(0XFF747474);
const t1_colorPrimaryDark = Color(0XFFff8080);
const t1_colorAccent = Color(0XFFff8080);
const t1_textColorPrimary = Color(0XFF333333);

const shadow_color = Color(0X95E9EBF0);

// const textSizeSmall = 12.0;
// const textSizeSMedium = 14.0;
// const textSizeMedium = 16.0;
// const textSizeLargeMedium = 18.0;
// const textSizeNormal = 20.0;
// const textSizeLarge = 24.0;
// const textSizeXLarge = 30.0;
// const textVerySmallSize = 10.0;
// const profileImage = 'assets/images/profile.png';
// const t1_colorPrimary = Color(0XFFff8080);
// const shadow_color = Color(0X95E9EBF0);
// const t1_white = Color(0XFFffffff);

//
//
// const fontRegular = 'Regular';
// const fontMedium = 'Medium';
// const fontSemibold = 'Semibold';
// const fontBold = 'Bold';
// const fontAntina = 'Andina';
